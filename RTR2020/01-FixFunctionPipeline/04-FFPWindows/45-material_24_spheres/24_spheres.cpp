#include <windows.h>
#include <gl/GL.h>
#include<gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

static int angleLightX = 0;
static int angleLightY = 0;
static int angleLightZ = 0;

int rotationX = 0;
int rotationY = 0;
int rotationZ = 0;

bool gbLighting = false;
GLUquadric* quadric = NULL;


GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 1.0f, 0.0f };

GLfloat lightModelAmbient[] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat lightModelLocalViewer[] = { 0.0f, 0.0f, 0.0f };

GLfloat sOnecOnematerial_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat sOnecOnematerial_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat sOnecOnematerial_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat sOnecOnematerial_shininess[] = { 0.6f * 128.0f };

GLfloat sTwocOnematerial_ambient[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat sTwocOnematerial_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat sTwocOnematerial_specular[] = { 0.316228, 0.316228, 0.316228, 1.0f };
GLfloat sTwocOnematerial_shininess[] = { 0.1f * 128.0f };

GLfloat sThreecOnematerial_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat sThreecOnematerial_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat sThreecOnematerial_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat sThreecOnematerial_shininess[] = { 0.3f * 128.0f };


GLfloat sFourcOnematerial_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat sFourcOnematerial_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat sFourcOnematerial_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat sFourcOnematerial_shininess[] = { 0.088f * 128.0f };

GLfloat sFivecOnematerial_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat sFivecOnematerial_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat sFivecOnematerial_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat sFivecOnematerial_shininess[] = { 0.6f * 128.0f };

GLfloat sSixcOnematerial_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat sSixcOnematerial_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat sSixcOnematerial_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat sSixcOnematerial_shininess[] = { 0.1f * 128.0f };

GLfloat sOnecTwomaterial_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat sOnecTwomaterial_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat sOnecTwomaterial_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat sOnecTwomaterial_shininess[] = { 0.21794872f * 128.0f };

GLfloat sTwocTwomaterial_ambient[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat sTwocTwomaterial_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat sTwocTwomaterial_specular[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat sTwocTwomaterial_shininess[] = { 0.2f * 128.0f };

GLfloat sThreecTwomaterial_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat sThreecTwomaterial_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat sThreecTwomaterial_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat sThreecTwomaterial_shininess[] = { 0.6f * 128.0f };

GLfloat sFourcTwomaterial_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
GLfloat sFourcTwomaterial_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat sFourcTwomaterial_specular[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
GLfloat sFourcTwomaterial_shininess[] = { 0.1f * 128.0f };

GLfloat sFivecTwomaterial_ambient[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
GLfloat sFivecTwomaterial_diffuse[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat sFivecTwomaterial_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat sFivecTwomaterial_shininess[] = { 0.4f * 128.0f };

GLfloat sSixcTwomaterial_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat sSixcTwomaterial_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat sSixcTwomaterial_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat sSixcTwomaterial_shininess[] = { 0.4f * 128.0f };

GLfloat sOnecThreematerial_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat sOnecThreematerial_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat sOnecThreematerial_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
GLfloat sOnecThreematerial_shininess[] = { 0.25f * 128.0f };

GLfloat sTwocThreematerial_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat sTwocThreematerial_diffuse[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat sTwocThreematerial_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat sTwocThreematerial_shininess[] = { 0.25f * 128.0f };

GLfloat sThreecThreematerial_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat sThreecThreematerial_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat sThreecThreematerial_specular[] = { 0.45f, 0.55f, 0.45f, 1.0f };
GLfloat sThreecThreematerial_shininess[] = { 0.25f * 128.0f };

GLfloat sFourcThreematerial_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat sFourcThreematerial_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat sFourcThreematerial_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
GLfloat sFourcThreematerial_shininess[] = { 0.25f * 128.0f };

GLfloat sFivecThreematerial_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat sFivecThreematerial_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat sFivecThreematerial_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
GLfloat sFivecThreematerial_shininess[] = { 0.25f * 128.0f };

GLfloat sSixcThreematerial_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat sSixcThreematerial_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat sSixcThreematerial_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
GLfloat sSixcThreematerial_shininess[] = { 0.25f * 128.0f };

GLfloat sOnecFourmaterial_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat sOnecFourmaterial_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat sOnecFourmaterial_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat sOnecFourmaterial_shininess[] = { 0.078125f * 128.0f };

GLfloat sTwocFourmaterial_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat sTwocFourmaterial_diffuse[] = { 0.4f, 0.5f, 0.5f, 1.0f };
GLfloat sTwocFourmaterial_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat sTwocFourmaterial_shininess[] = { 0.078125f * 128.0f };

GLfloat sThreecFourmaterial_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat sThreecFourmaterial_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat sThreecFourmaterial_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat sThreecFourmaterial_shininess[] = { 0.078125f * 128.0f };

GLfloat sFourcFourmaterial_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat sFourcFourmaterial_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat sFourcFourmaterial_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat sFourcFourmaterial_shininess[] = { 0.078125f * 128.0f };

GLfloat sFivecFourmaterial_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat sFivecFourmaterial_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat sFivecFourmaterial_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat sFivecFourmaterial_shininess[] = { 0.078125f * 128.0f };

GLfloat sSixcFourmaterial_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat sSixcFourmaterial_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat sSixcFourmaterial_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat sSixcFourmaterial_shininess[] = { 0.078125f * 128.0f };

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	int xPos, yPos;
	MONITORINFO mi;
	mi = { sizeof(MONITORINFO) };

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Materials"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi);
	xPos = (mi.rcMonitor.right - mi.rcMonitor.left) / 2;
	yPos = (mi.rcMonitor.bottom - mi.rcMonitor.top) / 2;
	SetWindowPos(hwnd, HWND_TOP, xPos - 400, yPos - 300, 800, 600, SWP_NOZORDER | SWP_FRAMECHANGED);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:	//For 'l' or 'L'
			if (gbLighting == false)
			{
				gbLighting = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				gbLighting = false;
				glDisable(GL_LIGHTING);
			}
			break;
		case 0x58:
			rotationX = 1;
			rotationY = 0;
			rotationZ = 0;
			break;
		case 0x59:
			rotationX = 0;
			rotationY = 1;
			rotationZ = 0;
			break;
		case 0x5A:
			rotationX = 0;
			rotationY = 0;
			rotationZ = 1;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.50f, 0.50f, 0.50f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glEnable(GL_LIGHT0);
	quadric = gluNewQuadric();
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	if (rotationX == 1)
	{
		glPushMatrix();
		glRotatef((GLfloat)angleLightX, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = angleLightX;
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
		glPopMatrix();
	}
	else if (rotationY == 1)
	{
		glPushMatrix();
		glRotatef((GLfloat)angleLightY, 0.0f, 1.0f, 0.0f);
		lightPosition[0] = angleLightY;
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
		glPopMatrix();
	}
	else if (rotationZ == 1)
	{
		glPushMatrix();
		glRotatef((GLfloat)angleLightZ, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = angleLightZ;
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
		glPopMatrix();
	}

	glMaterialfv(GL_FRONT, GL_AMBIENT, sOnecOnematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sOnecOnematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sOnecOnematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sOnecOnematerial_shininess);

	glPushMatrix();
	glTranslatef(-4.0f, 6.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sTwocOnematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sTwocOnematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sTwocOnematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sTwocOnematerial_shininess);

	glPushMatrix();
	glTranslatef(-4.0f, 4.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sThreecOnematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sThreecOnematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sThreecOnematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sThreecOnematerial_shininess);

	glPushMatrix();
	glTranslatef(-4.0f, 1.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFourcOnematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFourcOnematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFourcOnematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFourcOnematerial_shininess);

	glPushMatrix();
	glTranslatef(-4.0f, -1.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFivecOnematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFivecOnematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFivecOnematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFivecOnematerial_shininess);

	glPushMatrix();
	glTranslatef(-4.0f, -3.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sSixcOnematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sSixcOnematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sSixcOnematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sSixcOnematerial_shininess);

	glPushMatrix();
	glTranslatef(-4.0f, -6.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sOnecTwomaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sOnecTwomaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sOnecTwomaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sOnecTwomaterial_shininess);

	glPushMatrix();
	glTranslatef(-1.5f, 6.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sTwocTwomaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sTwocTwomaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sTwocTwomaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sTwocTwomaterial_shininess);

	glPushMatrix();
	glTranslatef(-1.5f, 4.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sThreecTwomaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sThreecTwomaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sThreecTwomaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sThreecTwomaterial_shininess);

	glPushMatrix();
	glTranslatef(-1.5f, 1.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFourcTwomaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFourcTwomaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFourcTwomaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFourcTwomaterial_shininess);

	glPushMatrix();
	glTranslatef(-1.5f, -1.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFivecTwomaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFivecTwomaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFivecTwomaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFivecTwomaterial_shininess);

	glPushMatrix();
	glTranslatef(-1.5f, -3.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sSixcTwomaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sSixcTwomaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sSixcTwomaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sSixcTwomaterial_shininess);

	glPushMatrix();
	glTranslatef(-1.5f, -6.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sOnecThreematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sOnecThreematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sOnecThreematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sOnecThreematerial_shininess);

	glPushMatrix();
	glTranslatef(1.0f, 6.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sTwocThreematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sTwocThreematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sTwocThreematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sTwocThreematerial_shininess);

	glPushMatrix();
	glTranslatef(1.0f, 4.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sThreecThreematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sThreecThreematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sThreecThreematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sThreecThreematerial_shininess);

	glPushMatrix();
	glTranslatef(1.0f, 1.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFourcThreematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFourcThreematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFourcThreematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFourcThreematerial_shininess);

	glPushMatrix();
	glTranslatef(1.0f, -1.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFivecThreematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFivecThreematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFivecThreematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFivecThreematerial_shininess);

	glPushMatrix();
	glTranslatef(1.0f, -3.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sSixcThreematerial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sSixcThreematerial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sSixcThreematerial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sSixcThreematerial_shininess);

	glPushMatrix();
	glTranslatef(1.0f, -6.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sOnecFourmaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sOnecFourmaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sOnecFourmaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sOnecFourmaterial_shininess);

	glPushMatrix();
	glTranslatef(3.5f, 6.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sTwocFourmaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sTwocFourmaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sTwocFourmaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sTwocFourmaterial_shininess);

	glPushMatrix();
	glTranslatef(3.5f, 4.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sThreecFourmaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sThreecFourmaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sThreecFourmaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sThreecFourmaterial_shininess);

	glPushMatrix();
	glTranslatef(3.5f, 1.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFourcFourmaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFourcFourmaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFourcFourmaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFourcFourmaterial_shininess);

	glPushMatrix();
	glTranslatef(3.5f, -1.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sFivecFourmaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sFivecFourmaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sFivecFourmaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sFivecFourmaterial_shininess);

	glPushMatrix();
	glTranslatef(3.5f, -3.5f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();

	glMaterialfv(GL_FRONT, GL_AMBIENT, sSixcFourmaterial_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, sSixcFourmaterial_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, sSixcFourmaterial_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, sSixcFourmaterial_shininess);

	glPushMatrix();
	glTranslatef(3.5f, -6.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.0f, 100, 100);
	glPopMatrix();
	SwapBuffers(ghdc);
}
void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	//Set angle = 0.0f to view the triangle withour translation
}

void uninitialize(void)
{

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	gluDeleteQuadric(quadric);
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
	DestroyWindow(ghwnd);
	ghwnd = NULL;
}


void update(void)
{
	angleLightX = (angleLightX - 1) % 360;
	angleLightY = (angleLightY - 1) % 360;
	angleLightZ = (angleLightZ - 1) % 360;
}
