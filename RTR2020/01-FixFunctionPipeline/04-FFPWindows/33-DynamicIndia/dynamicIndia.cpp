#include<windows.h>
#include<stdio.h>
#include<gl\gl.h>
#include<gl/glu.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

const float PI = 3.1415f;

GLfloat i1_anime = -2.0f;
GLfloat n_anime = 2.0f;
GLfloat d_anime = -2.0f;
GLfloat i2_anime = -2.0f;
GLfloat a_anime = 2.0f;

GLfloat plane1_bottom_trans_x = -1.0f;
GLfloat plane1_bottom_trans_y = -0.5f;
GLfloat plane1_bottom_rotate_z = 80;

GLfloat plane2_top_trans_x = -1.0f;
GLfloat plane2_top_trans_y = 0.5f;
GLfloat plane2_top_rotate_z = -80;

GLfloat plane3_middle_trans_x = -1.5f;
GLfloat plane3_middle_trans_y = 0.0f;
 
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "winmm.lib")

bool gbActiveWindow = false;
FILE* gpFile;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void initialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Jithin Window");
	bool bDone = false;
	int WindowX = 0;
	int WindowY = 0;

	//code
	if (fopen_s(&gpFile, "myLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot create desired file!!"), TEXT("Error!!"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, NULL);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, NULL);

	RegisterClassEx(&wndclass);
	WindowX = ((GetSystemMetrics(SM_CXSCREEN)) / 2) - (800 / 2);
	WindowY = ((GetSystemMetrics(SM_CYSCREEN)) / 2) - (400 / 2);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Jithin Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		WindowX,
		WindowY,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;
	initialize();
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				//UpdateWindow(hwnd);
			}
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		PlaySound(TEXT("NA.wav"), NULL, SND_FILENAME | SND_ASYNC);
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void initialize(void)
{
	//function declaration
	void resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() failed\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() failed\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd);
	}

	//set Clear color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	void I1(void);
	void N(void);
	void D(void);
	void I2(void);
	void A(void);

	void iaf_plane(void);
	void plane_animation(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(i1_anime, 0.0f, -2.5f);
	I1();
	if (i1_anime >= 0.2f)
	{
		glTranslatef(1.06f, 0.0f, 0.0f);
		glTranslatef(a_anime, 0.0f, 0.0f);
		A();
	}

	if (a_anime <= 0.2f)
	{
		glTranslatef(-1.06f, 0.0f, 0.0f);
		glTranslatef(0.0f, n_anime, 0.0f);
		N();
	}

	if (n_anime <= 0.0f)
	{
		glTranslatef(0.86f, 0.0f, 0.0f);
		glTranslatef(0.0f, i2_anime, 0.0f);
		I2();
	}

	if (i2_anime >= 0.0f)
	{
		glTranslatef(-0.56f, 0.0f, 0.0f);
		glTranslatef(0.0f, 0.0f, d_anime);
		D();
	}


	if (i1_anime <= 0.2f)
		i1_anime = i1_anime + 0.0028f;

	if (i1_anime >= 0.2f && a_anime >= 0.2f)
		a_anime = a_anime - 0.0028f;

	if (a_anime <= 0.2f && n_anime >= 0.0f)
		n_anime = n_anime - 0.0028f;

	if (n_anime <= 0.0f && i2_anime <= 0.0f)
		i2_anime = i2_anime + 0.0028f;

	if (i2_anime >= 0.0f && d_anime <= 0)
		d_anime = d_anime + 0.0028f;

	if (d_anime >= 0)
	{
		glTranslatef(0.0f, 0.0f, 7.0f);
		plane_animation();
	}
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//code

	if (gbFullScreen == true)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Logfile Closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void I1(void)
{
	//I
	glLineWidth(30.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();
}

void N(void)
{
	//N
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.8f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, -0.6f, 0.0f);
	glEnd();
}

void D(void)
{
	//D
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.9f, 0.57f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, -0.57f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.65f, 0.57f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.65f, 0.0f, 0.0f);
	glVertex3f(-0.65f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.65f, -0.57f, 0.0f);
	glEnd();

	glLineWidth(20.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.95f, 0.57f, 0.0f);
	glVertex3f(-0.65f, 0.57f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.95f, -0.57f, 0.0f);
	glVertex3f(-0.65f, -0.57f, 0.0f);
	glEnd();
}

void I2(void)
{
	//I
	glLineWidth(30.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();
}

void A(void)
{
	if (plane3_middle_trans_x >= 0.5f)
	{
		//A
		glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.85f, 0.0f, 0.0f);
		glVertex3f(-0.55f, 0.0f, 0.0f);
		glEnd();
	}
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.7f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.85f, 0.0f, 0.0f);
	glVertex3f(-0.85f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.7f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.55f, 0.0f, 0.0f);
	glVertex3f(-0.55f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();
}

void iaf_plane(void)
{

	//ammunition
	glBegin(GL_QUADS);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f / 4.0f, 0.36f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.42f / 4.0f, 0.0f);
	glVertex3f(-0.3f / 4.0f, 0.42f / 4.0f, 0.0f);
	glVertex3f(-0.3f / 4.0f, 0.36f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.4f / 4.0f, 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.3f / 4.0f, (0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, (0.36f / 4.0f), 0.0f);
	glVertex3f(-0.2f / 4.0f, (0.39f / 4.0f), 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f / 4.0f, -(0.36f / 4.0f), 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, -(0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, -(0.36f / 4.0f), 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.4f / 4.0f), 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.3f / 4.0f, -(0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, -(0.36f / 4.0f), 0.0f);
	glVertex3f(-0.2f / 4.0f, -(0.39f / 4.0f), 0.0f);
	glEnd();


	//end of ammunition
	//plane body

	//top side flip
	glBegin(GL_TRIANGLES); 
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f/4.0f, 0.4f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.4f / 4.0f), 0.0f);
	glVertex3f(0.05f / 4.0f, 0.0f, 0.0f);
	glEnd();

	//bottom side flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, 0.4f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.1f / 4.0f, 0.0f);
	glVertex3f(0.05f / 4.0f, 0.01f / 4.0f, 0.0f);
	glEnd();

	//back flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f / 4.0f, 0.1f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.1f / 4.0f), 0.0f);
	glVertex3f(-0.7f / 4.0f, 0.0f, 0.0f);
	glEnd();

	//back top flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, 0.1f / 4.0f, 0.0f);
	glVertex3f(-0.62f / 4.0f, 0.036f / 4.0f, 0.0f);
	glVertex3f(-0.60f / 4.0f, 0.15f / 4.0f, 0.0f);
	glEnd();

	//back bottom flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, -(0.1f / 4.0f), 0.0f);
	glVertex3f(-0.62f / 4.0f, -(0.036f / 4.0f), 0.0f);
	glVertex3f(-0.60f / 4.0f, -(0.15f / 4.0f), 0.0f);
	glEnd();



	//tail flag

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.6f / 4.0f, 0.009f, 0.0f);
	glVertex3f(-0.8f / 4.0f, 0.009f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f / 4.0f, -0.009f, 0.0f);
	glVertex3f(-0.8f / 4.0f, -0.009f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, -(0.4f / 4.0f), 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.1f / 4.0f), 0.0f);
	glVertex3f(0.05f / 4.0f, -(0.01f / 4.0f), 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.1f / 4.0f, 0.2f / 4.0f, 0.0f);
	glVertex3f(-0.1f / 4.0f, -(0.2f / 4.0f), 0.0f);
	glVertex3f(0.5f / 4.0f, 0.0f, 0.0f);
	glEnd();

	//windscreen
	glTranslatef(0.04f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f / 8.0f, 0.2f / 8.0f, 0.0f);
	glVertex3f(-0.1f / 8.0f, -(0.2f / 8.0f), 0.0f);
	glVertex3f(0.2f / 8.0f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f / 8.0f, 0.0f, 0.0f);
	glVertex3f(0.2f / 8.0f, 0.0f, 0.0f);
	glEnd();
	//End of plane body

	//green logo
	glTranslatef(-0.14f, 0.048f, 0.0f);
	glPointSize(5);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 1.0f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.02 / 4.0f) * cos(angle), (0.02 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//while logo
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.04 / 4.0f) * cos(angle), (0.04 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//orange logo
	glPointSize(3);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.5f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.06 / 4.0f )* cos(angle), (0.06 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();


	glTranslatef(0.0f, -0.096f, 0.0f);
	//green logo
	glPointSize(5);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 1.0f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.02 / 4.0f) * cos(angle), (0.02 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//while logo
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.04 / 4.0f) * cos(angle), (0.04 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//orange logo
	glPointSize(3);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.5f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.06 / 4.0f) * cos(angle), (0.06 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();


	glTranslatef(0.1f, 0.045f, 0.0f);
	//I
	glColor3f(1.0f, 1.0f, 1.0f);
	glScalef(0.18f, 0.18f, 0.18f);
	glTranslatef(-0.5f, 0.0f, 0.0f);
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glVertex3f(-0.5f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -0.3f / 4.0f, 0.0f);
	glEnd();

	//A
	glTranslatef(0.03f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.425f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.275f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.35f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -0.3f / 4.0f, 0.0f);
	glVertex3f(-0.35f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.2f / 4.0f, -0.3f / 4.0f, 0.0f);
	glEnd();

	//F
	glTranslatef(0.1f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.5f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.25f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.05f / 4.0f, 0.0f);
	glVertex3f(-0.3f / 4.0f, 0.05f / 4.0f, 0.0f);
	glEnd();
}

void plane_animation(void)
{
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glTranslatef(plane1_bottom_trans_x, plane1_bottom_trans_y, -1.5f);
glRotatef(plane1_bottom_rotate_z, 0.0f, 0.0f, 1.0f);
iaf_plane();

glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glTranslatef(plane2_top_trans_x, plane2_top_trans_y, -1.5f);
glRotatef(plane2_top_rotate_z, 0.0f, 0.0f, 1.0f);
iaf_plane();

glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glTranslatef(plane3_middle_trans_x, plane3_middle_trans_y, -1.5f);
iaf_plane();


if (plane1_bottom_trans_x <= -0.7f && plane3_middle_trans_x <= 0.7f)
plane1_bottom_trans_x = plane1_bottom_trans_x + 0.0025f;
if (plane1_bottom_trans_y <= 0.0f && plane3_middle_trans_x <= 0.7f)
plane1_bottom_trans_y = plane1_bottom_trans_y + 0.0042f;
if (plane1_bottom_rotate_z >= 0.0f && plane3_middle_trans_x <= 0.7f)
plane1_bottom_rotate_z = plane1_bottom_rotate_z - 0.69f;

if (plane2_top_trans_x <= -0.7f && plane3_middle_trans_x <= 0.7f)
plane2_top_trans_x = plane2_top_trans_x + 0.0025f;
if (plane2_top_trans_y >= 0.0f && plane3_middle_trans_x <= 0.7f)
plane2_top_trans_y = plane2_top_trans_y - 0.0042f;
if (plane2_top_rotate_z <= 0.0f && plane3_middle_trans_x <= 0.7f)
plane2_top_rotate_z = plane2_top_rotate_z + 0.69f;

if (plane3_middle_trans_x <= -0.7f)
plane3_middle_trans_x = plane3_middle_trans_x + 0.0068f;


if (plane3_middle_trans_x >= -0.7f && plane2_top_trans_x >= -0.7 && plane1_bottom_trans_x >= -0.7 && plane3_middle_trans_x < 0.7f)
{
	plane3_middle_trans_x = plane2_top_trans_x;
	plane1_bottom_trans_x = plane2_top_trans_x;
	plane3_middle_trans_y = plane2_top_trans_y;
	plane1_bottom_trans_y = plane2_top_trans_y;
	glTranslatef(plane3_middle_trans_x, 0.0f, -1.5f);
	glTranslatef(plane2_top_trans_x, 0.0f, -1.5f);
	glTranslatef(plane1_bottom_trans_x, 0.0f, -1.5f);
	plane3_middle_trans_x = plane3_middle_trans_x + 0.0128f;
	plane2_top_trans_x = plane2_top_trans_x + 0.0128f;
	plane1_bottom_trans_x = plane1_bottom_trans_x + 0.0128f;
}

if (plane3_middle_trans_x >= 0.7f)
{
		plane1_bottom_trans_x = plane1_bottom_trans_x + 0.0125f;
		plane1_bottom_trans_y = plane1_bottom_trans_y - 0.0142f;
		plane1_bottom_rotate_z = plane1_bottom_rotate_z - 0.89f;

		plane2_top_trans_x = plane2_top_trans_x + 0.0125f;
		plane2_top_trans_y = plane2_top_trans_y + 0.0142f;
		plane2_top_rotate_z = plane2_top_rotate_z + 0.89f;

		plane3_middle_trans_x = plane3_middle_trans_x + 0.0128f;
}
}