#include<stdio.h>

int main(void)
{
	int AddIntegers(int, int);
	int SubtractIntegers(int, int);
	float AddFloat(float, float);

	typedef int (*AddIntFnPtr)(float, float);
	AddIntFnPtr jgn_ptrAddtwoIntegers = NULL;
	AddIntFnPtr jgn_ptrFunc = NULL;

	typedef float (*AddFloatFnPtr)(float, float);
	AddFloatFnPtr jgn_ptrAddTwoFloats = NULL;

	int	jgn_iAnswer = 0;
	float jgn_fAnswer = 0.0f;

	jgn_ptrAddtwoIntegers = AddIntegers;
	jgn_iAnswer = jgn_ptrAddtwoIntegers(9, 10);
	printf("\n\n");
	printf("Sum of integers := %d", jgn_iAnswer);

	jgn_ptrFunc = SubtractIntegers;
	jgn_iAnswer = jgn_ptrFunc(9, 10);
	printf("\n\n");
	printf("Subtraction of integers := %d", jgn_iAnswer);


	jgn_ptrAddTwoFloats = AddFloat;
	jgn_fAnswer = jgn_ptrAddTwoFloats(2.5f, 6.52f);
	printf("\n\n");
	printf("Subtraction of integers := %f", jgn_fAnswer);

	return(0);
}

int AddIntegers(int a, int b)
{
	int jgn_c;
	jgn_c = a + b;
	return(jgn_c);
}

int SubtractIntegers(int a, int b)
{
	int jgn_c;
	if (a > b)
		jgn_c = a - b;
	else
		jgn_c = b - a;

	return(jgn_c);
}

float AddFloat(float fNum1, float fNum2)
{
	float jgn_ans;
	jgn_ans = fNum1 + fNum2;

	return(jgn_ans);
}