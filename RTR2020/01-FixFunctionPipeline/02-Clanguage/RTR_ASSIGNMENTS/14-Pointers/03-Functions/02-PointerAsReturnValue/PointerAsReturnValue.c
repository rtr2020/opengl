#include<stdio.h>
#include<stdlib.h>

#define	MAX_STRING_LENGTH 512

int main(void)
{
	char* ReplaceVowelsWithHashSymbol(char*);

	char jgn_string[MAX_STRING_LENGTH];
	char* jgn_replacedString = NULL;

	printf("\n\n");
	printf("Enter string: ");
	gets_s(jgn_string, MAX_STRING_LENGTH);

	jgn_replacedString = ReplaceVowelsWithHashSymbol(jgn_string);

	if (jgn_replacedString == NULL)
	{
		printf("Replaced function hqas failed ...Exiting\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Replaced string is: \n\n");
	printf("%s\n\n", jgn_replacedString);

	if (jgn_replacedString == NULL)
	{
		free(jgn_replacedString);
		jgn_replacedString = NULL;
	}

	return(0);
}

char* ReplaceVowelsWithHashSymbol(char* s)
{
	void MyStrcpy(char*, char*);
	int MyStrlen(char*);

	char* jgn_new_string = NULL;
	int jgn_i;

	jgn_new_string = (char*)malloc(MyStrlen(s) * sizeof(char));
	if (jgn_new_string == NULL)
	{
		printf("Could not allocate memory for new string!!\n\n");
		return(NULL);
	}
	
	MyStrcpy(jgn_new_string, s);

	for (jgn_i = 0; jgn_i < MyStrlen(jgn_new_string); jgn_i++)
	{
		switch (jgn_new_string[jgn_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'u':
		case 'U':
			jgn_new_string[jgn_i] = '#';
			break;
		default:
			break;
		}
	}

	return(jgn_new_string);
}

void MyStrcpy(char* str_destination, char* str_src)
{
	int MyStrlen(char*);
	int jgn_iStrLength = 0;
	int jgn_j;

	jgn_iStrLength = MyStrlen(str_src);
	for (jgn_j = 0; jgn_j < jgn_iStrLength; jgn_j++)
	{
		*(str_destination + jgn_j) = *(str_src + jgn_j);
	}
	*(str_destination + jgn_j) = '\0';
}

int MyStrlen(char* str)
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if (*(str + jgn_j) == '\0')
			break;
		else
			jgn_string_length++;
	}
	return(jgn_string_length);
}