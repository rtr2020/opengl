#include<stdio.h>
#include<stdlib.h>

int main(void)
{
	void MultiplyArrayElementsByNumber(int*, int, int);

	int* jgn_iArray = NULL;
	int jgn_num_elements;
	int jgn_i, jgn_num;

	printf("\n\n");
	printf("How many elements you want in integer array: ");
	scanf("%d", &jgn_num_elements);

	jgn_iArray = (int*)malloc(jgn_num_elements * sizeof(int));

	if (jgn_iArray == NULL)
	{
		printf("Memoery allocation for arrray failed!!\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Enter integer elements of the array: \n\n");
	for (jgn_i = 0; jgn_i < jgn_num_elements; jgn_i++)
	{
		scanf("%d", &jgn_iArray[jgn_i]);
	}

	printf("Array before passing to MultiplyArrayElementsByNumber function: \n\n");

	for (jgn_i = 0; jgn_i < jgn_num_elements; jgn_i++)
	{
		printf("jgn_iArray[%d] = %d\n", jgn_i, jgn_iArray[jgn_i]);
	}

	printf("\n\n");
	printf("Enter number you want to multiply with each elements of the array:");
	scanf("%d", &jgn_num);

	MultiplyArrayElementsByNumber(jgn_iArray, jgn_num_elements, jgn_num);

	printf("\n\n");
	printf("Array returned by function MultiplyArrayElementsByNumber is :\n");

	for (jgn_i = 0; jgn_i < jgn_num_elements; jgn_i++)
	{
		printf("jgn_iArray[%d] = %d\n", jgn_i, jgn_iArray[jgn_i]);
	}

	if (jgn_iArray)
	{
		free(jgn_iArray);
		jgn_iArray = NULL;
		printf("\n\n");
		printf("Memory allocation of array freed successfully\n\n");
	}

	return(0);
}

void MultiplyArrayElementsByNumber(int* arr, int iNumElements, int n)
{
	int jgn_i;

	for (jgn_i = 0; jgn_i < iNumElements; jgn_i++)
		arr[jgn_i] = arr[jgn_i] * n;
}