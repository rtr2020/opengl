#include<stdio.h>

int main(void)
{
	void SwapNumbers(int, int);

	int jgn_a, jgn_b;
	printf("\n\n");
	printf("Enter value for a: ");
	scanf("%d", &jgn_a);

	printf("\n\n");
	printf("Enter value for b: ");
	scanf("%d", &jgn_b);

	printf("\n\n");
	printf("Before Swapping\n\n");
	printf("Value of a = %d\n", jgn_a);
	printf("Value of b = %d\n", jgn_b);

	SwapNumbers(jgn_a, jgn_b);

	printf("\n\n");
	printf("After Swapping\n\n");
	printf("Value of a = %d\n", jgn_a);
	printf("Value of b = %d\n", jgn_b);

	return(0);
}

void SwapNumbers(int x, int y)
{
	int jgn_temp;

	printf("\n\n");
	printf("Before Swapping\n");
	printf("Value of x = %d\n", x);
	printf("Value of y = %d\n", y);

	jgn_temp = x;
	x = y;
	y = jgn_temp;

	printf("\n\n");
	printf("After Swapping\n");
	printf("Value of x = %d\n", x);
	printf("Value of y = %d\n", y);

}