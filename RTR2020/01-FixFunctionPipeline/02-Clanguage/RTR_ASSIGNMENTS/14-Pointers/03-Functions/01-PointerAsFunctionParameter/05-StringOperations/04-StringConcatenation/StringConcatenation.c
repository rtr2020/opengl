#include<stdio.h>
#include<stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrcat(char*, char*);
	int MyStrlen(char*);

	char* jgn_chArray_original = NULL, * jgn_chConcat = NULL;
	int jgn_origninal_iStringLength = 0;

	printf("\n\n");
	jgn_chArray_original = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (jgn_chArray_original == NULL)
	{
		printf("Memory allocation to character array failed!!!!...Exiting\n\n");
		exit(0);
	}

	printf("Enter A string : \n\n");
	gets_s(jgn_chArray_original, MAX_STRING_LENGTH);

	printf("\n\n");
	jgn_origninal_iStringLength = MyStrlen(jgn_chArray_original);
	jgn_chConcat = (char*)malloc(jgn_origninal_iStringLength * sizeof(char));
	if (jgn_chConcat == NULL)
	{
		printf("Memory allocation for concat string failed\n\n");
		exit(0);
	}
	printf("Enter Second string : \n\n");
	gets_s(jgn_chConcat, MAX_STRING_LENGTH);

	MyStrcat(jgn_chConcat, jgn_chArray_original);

	printf("\n\n");
	printf("The original string entered by you is:\n\n");
	printf("%s\n", jgn_chArray_original);

	printf("\n\n");
	printf("The concatenated string entered by you is:\n\n");
	printf("%s\n", jgn_chConcat);

	if (jgn_chConcat)
	{
		free(jgn_chConcat);
		jgn_chConcat = NULL;
		printf("\n\n");
		printf("Memory freed for concat string allocation\n\n");
	}

	if (jgn_chArray_original)
	{
		free(jgn_chArray_original);
		jgn_chArray_original = NULL;
		printf("\n\n");
		printf("Memory freed for original string allocation\n\n");
	}

	return(0);
}

void MyStrcat(char* str_destination, char* str_src)
{
	int MyStrlen(char*);
	int jgn_iStrLength_src = 0;
	int jgn_iStrLength_dest = 0;
	int jgn_i, jgn_j, jgn_len;

	jgn_iStrLength_src = MyStrlen(str_src);
	jgn_iStrLength_dest = MyStrlen(str_destination);
	for (jgn_i = jgn_iStrLength_dest, jgn_j = 0; jgn_j < jgn_iStrLength_src; jgn_i++, jgn_j++)
	{
		*(str_destination + jgn_i) = *(str_src + jgn_j);
	}
	*(str_destination + jgn_i) = '\0';
}

int MyStrlen(char* str)
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if (*(str + jgn_j) == '\0')
			break;
		else
			jgn_string_length++;
	}
	return(jgn_string_length);
}