#include<stdio.h>
#include<stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrlen(char*);

	char* jgn_chArray = NULL;
	int jgn_iStringLength = 0;

	printf("\n\n");
	jgn_chArray = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (jgn_chArray == NULL)
	{
		printf("Memory allocation to character array failed!!!!...Exiting\n\n");
		exit(0);
	}

	printf("Enter A string : \n\n");
	gets_s(jgn_chArray, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", jgn_chArray);

	printf("\n\n");
	jgn_iStringLength = MyStrlen(jgn_chArray);
	printf("Length of string is = %d characters\n\n", jgn_iStringLength);

	if (jgn_chArray)
	{
		free(jgn_chArray);
		jgn_chArray = NULL;
	}

	return(0);
}

int MyStrlen(char* str)
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if (*(str + jgn_j) == '\0')
			break;
		else
			jgn_string_length++;
	}
	return(jgn_string_length);
}