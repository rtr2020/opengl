#include<stdio.h>
#include<stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrrev(char*, char*);
	int MyStrlen(char*);

	char* jgn_chArray_original = NULL, * jgn_chReversed = NULL;
	int jgn_origninal_iStringLength = 0;

	printf("\n\n");
	jgn_chArray_original = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (jgn_chArray_original == NULL)
	{
		printf("Memory allocation to character array failed!!!!...Exiting\n\n");
		exit(0);
	}

	printf("Enter A string : \n\n");
	gets_s(jgn_chArray_original, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", jgn_chArray_original);

	printf("\n\n");
	jgn_origninal_iStringLength = MyStrlen(jgn_chArray_original);
	jgn_chReversed = (char*)malloc(jgn_origninal_iStringLength * sizeof(char));

	if (jgn_chReversed == NULL)
	{
		printf("Memory allocation for copy string failed\n\n");
		exit(0);
	}

	MyStrrev(jgn_chReversed, jgn_chArray_original);

	printf("\n\n");
	printf("The original string entered by you is:\n\n");
	printf("%s\n", jgn_chArray_original);

	printf("\n\n");
	printf("The copied string entered by you is:\n\n");
	printf("%s\n", jgn_chReversed);

	if (jgn_chReversed)
	{
		free(jgn_chReversed);
		jgn_chReversed = NULL;
		printf("\n\n");
		printf("Memory freed for reverse string allocation\n\n");
	}

	if (jgn_chArray_original)
	{
		free(jgn_chArray_original);
		jgn_chArray_original = NULL;
		printf("\n\n");
		printf("Memory freed for original string allocation\n\n");
	}

	return(0);
}

void MyStrrev(char* str_destination, char* str_src)
{
	int MyStrlen(char*);
	int jgn_iStrLength = 0;
	int jgn_i, jgn_j, jgn_len;

	jgn_iStrLength = MyStrlen(str_src);
	jgn_len = jgn_iStrLength - 1;
	for (jgn_i = 0, jgn_j = jgn_len; jgn_i < jgn_iStrLength, jgn_j >= 0; jgn_i++, jgn_j--)
	{
		*(str_destination + jgn_i) = *(str_src + jgn_j);
	}
	*(str_destination + jgn_i) = '\0';
}

int MyStrlen(char* str)
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if (*(str + jgn_j) == '\0')
			break;
		else
			jgn_string_length++;
	}
	return(jgn_string_length);
}