#include<stdio.h>

enum
{
	NEGATIVE = -1,
	ZERO,
	POSITIVE
};

int main(void)
{
	int Difference(int, int, int*);

	int jgn_a;
	int jgn_b;
	int jgn_answer, jgn_ret;

	printf("\n\n");
	printf("Enter value of A: ");
	scanf("%d", &jgn_a);

	printf("\n\n");
	printf("Enter value of B: ");
	scanf("%d", &jgn_b);

	jgn_ret = Difference(jgn_a, jgn_b, &jgn_answer);

	printf("\n\n");
	printf("Difference of %d and %d = %d\n\n", jgn_a, jgn_b, jgn_answer);

	if (jgn_ret == POSITIVE)
	{
		printf("The differnce of %d and %d is POSITIVE !!!\n\n", jgn_a, jgn_b);
	}
	else if (jgn_ret == NEGATIVE)
	{
		printf("The differnce of %d and %d is NEGATIVE !!!\n\n", jgn_a, jgn_b);
	}
	else	
	{
		printf("The differnce of %d and %d is ZERO !!!\n\n", jgn_a, jgn_b);
	}

	return(0);
}

int Difference(int x, int y, int* diff)
{
	*diff = x - y;
	if (*diff > 0)
		return(POSITIVE);
	else if(*diff < 0)
		return(NEGATIVE);
	else
		return(ZERO);
}