#include<stdio.h>

int main(void)
{
	void MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	int jgn_a;
	int jgn_b;
	int jgn_answer_sum;
	int jgn_answer_difference;
	int	jgn_answer_product;
	int jgn_answer_quotient;
	int jgn_answer_remainder;

	printf("\n\n");
	printf("Enter value of A: ");
	scanf("%d", &jgn_a);

	printf("\n\n");
	printf("Enter value of B: ");
	scanf("%d", &jgn_b);

	MathematicalOperations(jgn_a, jgn_b, &jgn_answer_sum, &jgn_answer_difference, &jgn_answer_product, &jgn_answer_quotient, &jgn_answer_remainder);

	printf("\n\n");
	printf("*******Results*******\n\n");
	printf("Sum = %d\n\n", jgn_answer_sum);
	printf("Difference = %d\n\n", jgn_answer_difference);
	printf("Product = %d\n\n", jgn_answer_product);
	printf("Quotient = %d\n\n", jgn_answer_quotient);
	printf("Remainder = %d\n\n", jgn_answer_remainder);
	return(0);
}

void MathematicalOperations(int x, int y, int* sum, int* difference, int* product, int* quotient, int* remainder)
{
	*sum = x + y;
	*difference = x - y;
	*product = x * y;
	*quotient = x / y;
	*remainder = x % y;
}