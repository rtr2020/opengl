#include<stdio.h>

int main(void)
{
	void MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	int jgn_a;
	int jgn_b;
	int *jgn_sum;
	int *jgn_difference;
	int *jgn_product;
	int *jgn_quotient;
	int *jgn_remainder;

	printf("\n\n");
	printf("Enter value of 'A': ");
	scanf("%d", &jgn_a);

	printf("\n\n");
	printf("Enter value of 'B': ");
	scanf("%d", &jgn_b);

	jgn_sum = (int*)malloc(1 * sizeof(int));
	if (jgn_sum == NULL)
	{
		printf("Could not allcoate memory for jgn_sum....Exiting\n");
		exit(0);
	}

	jgn_difference = (int*)malloc(1 * sizeof(int));
	if (jgn_difference == NULL)
	{
		printf("Could not allcoate memory for jgn_difference....Exiting\n");
		exit(0);
	}

	jgn_product = (int*)malloc(1 * sizeof(int));
	if (jgn_product == NULL)
	{
		printf("Could not allcoate memory for jgn_product....Exiting\n");
		exit(0);
	}

	jgn_quotient = (int*)malloc(1 * sizeof(int));
	if (jgn_quotient == NULL)
	{
		printf("Could not allcoate memory for jgn_quotient....Exiting\n");
		exit(0);
	}

	jgn_remainder = (int*)malloc(1 * sizeof(int));
	if (jgn_remainder == NULL)
	{
		printf("Could not allcoate memory for jgn_remainder....Exiting\n");
		exit(0);
	}

	MathematicalOperations(jgn_a, jgn_b, jgn_sum, jgn_difference, jgn_product, jgn_quotient, jgn_remainder);

	printf("\n\n");
	printf("********RESULTS*********\n\n");
	printf("Sum : = %d\n", *jgn_sum);
	printf("Difference := %d\n", *jgn_difference);
	printf("Product := %d\n", *jgn_product);
	printf("Quotient := %d\n", *jgn_quotient);
	printf("Remainder := %d\n\n", *jgn_remainder);
	return(0);
}

void MathematicalOperations(int x, int y, int* sum, int* difference, int* product, int* quotient, int* remainder)
{
	*sum = x + y;
	*difference = x - y;
	*product = x * y;
	*quotient = x / y;
	*remainder = x % y;
}