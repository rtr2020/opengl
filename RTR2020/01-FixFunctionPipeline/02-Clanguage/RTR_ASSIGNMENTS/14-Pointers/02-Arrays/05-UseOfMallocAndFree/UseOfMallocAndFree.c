#include<stdio.h>
#include<stdlib.h>

int main(void)
{
	int *jgn_ptr_iarray = NULL;

	unsigned int intArrayLength = 0;

	int jgn_i;

	printf("\n\n");
	printf("Enter the number of elements you want in your array: ");
	scanf("%d", &intArrayLength);
	printf("\n\n");
	jgn_ptr_iarray = (int*)malloc(intArrayLength * sizeof(int));
	if (jgn_ptr_iarray == NULL)
	{
		printf("Allocation of Integer array has failed...EXiting now..\n\n");
		exit(0);
	}
	else
	{
		printf("Memory allocation for integer array has succeded\n\n");
		printf("Memoery address from %p to %p has been allocated \n\n", jgn_ptr_iarray, jgn_ptr_iarray + (intArrayLength - 1));
	}

	printf("\n\n");
	printf("Enter Integer array of %d elements:\n\n", intArrayLength);

	for (jgn_i = 0; jgn_i < intArrayLength; jgn_i++)
	{
		scanf("%d", (jgn_ptr_iarray + jgn_i));
	}

	printf("\n\n");
	printf("The integer array entered by you is: \n\n");

	for (jgn_i = 0; jgn_i < intArrayLength; jgn_i++)
	{
		printf("jgn_ptr_iarray[%d] = %d \t\t At address &jgn_ptr_iArray[%d] : %p\n", jgn_i, jgn_ptr_iarray[jgn_i], jgn_i, &jgn_ptr_iarray[jgn_i]);
	}

	for (jgn_i = 0; jgn_i < intArrayLength; jgn_i++)
	{
		printf("jgn_ptr_iarray + %d = %d \t\t At address &jgn_ptr_iArray + %d : %p\n", jgn_i, *(jgn_ptr_iarray + jgn_i), jgn_i, (jgn_ptr_iarray + jgn_i));
	}


	if (jgn_ptr_iarray)
	{
		free(jgn_ptr_iarray);
		jgn_ptr_iarray = NULL;

		printf("\n\n");
		printf("Memory allocated has been freed\n\n");
	}

	return(0);
}