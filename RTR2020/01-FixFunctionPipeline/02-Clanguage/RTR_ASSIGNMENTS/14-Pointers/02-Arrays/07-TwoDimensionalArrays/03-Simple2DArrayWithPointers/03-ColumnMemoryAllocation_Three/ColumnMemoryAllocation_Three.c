#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 5

int main(void)
{
	int jgn_i, jgn_j;

	int* jgn_iArray[NUM_ROWS];

	printf("\n\n");
	printf("***********Memory allocation to 2D integer array**************\n\n");
	for (jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{


		jgn_iArray[jgn_i] = (int*)malloc((NUM_COLUMNS - jgn_i) * sizeof(int));

		if (jgn_iArray[jgn_i] == NULL)
		{
			printf("Memory allocation of column row %d failed\n\n", jgn_i);
			exit(0);
		}
		else
		{
			printf("Memoery allocation of column row %d successfull\n\n", jgn_i);
		}
	}



	for (jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		for (jgn_j = 0; jgn_j < (NUM_COLUMNS - jgn_i); jgn_j++)
			jgn_iArray[jgn_i][jgn_j] = (jgn_i + 1) * (jgn_j + 1);
	}

	printf("\n\n");
	printf("2D Integer array along with address: \n\n");

	for (jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		for (jgn_j = 0; jgn_j < (NUM_COLUMNS - jgn_i); jgn_j++)
		{
			printf("jgn_iArray[%d][%d] = %d \t\t At address : %p\n", jgn_i, jgn_j, jgn_iArray[jgn_i][jgn_j], &jgn_iArray[jgn_i][jgn_j]);
		}

		printf("\n\n");
	}


	for (jgn_i = (NUM_ROWS - 1); jgn_i >= 0; jgn_i--)
	{
		free(jgn_iArray[jgn_i]);
		jgn_iArray[jgn_i] = NULL;
		printf("Memory allocated to row %d has been successfully freed\n\n", jgn_i);
	}

	return(0);
}