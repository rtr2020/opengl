#include<stdio.h>
#include<stdlib.h>

int main(void)
{
	int jgn_i, jgn_j;

	int** jgn_ptr_iArray = NULL;
	
	int jgn_num_rows, jgn_num_columns;

	printf("\n\n");
	printf("Enter number of rows:");
	scanf("%d", &jgn_num_rows);

	printf("\n\n");
	printf("Enter number of columns:");
	scanf("%d", &jgn_num_columns);

	jgn_ptr_iArray = (int**)malloc(jgn_num_rows * sizeof(int*));

	if (jgn_ptr_iArray == NULL)
	{
		printf("Memoery allocation failed ...EXiting...!\n\n");
	}
	else
	{
		printf("Memory allocation of %d rows successfull\n\n", jgn_num_rows);
	}

	for (jgn_i = 0; jgn_i < jgn_num_rows; jgn_i++)
	{
		jgn_ptr_iArray[jgn_i] = (int*)malloc(jgn_num_columns * sizeof(int));

		if (jgn_ptr_iArray == NULL)
		{
			printf("Memoery allocation of column row %d failed\n\n", jgn_i);
			exit(0);
		}
		else
		{
			printf("Memoery allocation of column row %d successfull\n\n", jgn_i);
		}
	}

	for (jgn_i = 0; jgn_i < jgn_num_rows; jgn_i++)
	{
		for (jgn_j = 0; jgn_j < jgn_num_columns; jgn_j++)
			*(*(jgn_ptr_iArray + jgn_i) + jgn_j) = (jgn_i + 1) * (jgn_j + 1);
	}


	for (jgn_i = 0; jgn_i < jgn_num_rows; jgn_i++)
	{
		printf("Base address of row %d : jgn_ptr_iArray[%d] = %p \t At Address : %p \n", jgn_i, jgn_i, jgn_ptr_iArray[jgn_i], &jgn_ptr_iArray[jgn_i]);
	}

	printf("\n\n");
	for (jgn_i = 0; jgn_i < jgn_num_rows; jgn_i++)
	{
		for (jgn_j = 0; jgn_j < jgn_num_columns; jgn_j++)
		{
			printf("jgn_pyt_iArray[%d][%d] = %d \t\t At Address : %p \n", jgn_i, jgn_j, jgn_ptr_iArray[jgn_i][jgn_j], &jgn_ptr_iArray[jgn_i][jgn_j]);
		}

		printf("\n\n");
	}

	//freeing memory

	for (jgn_i = (jgn_num_rows - 1); jgn_i >= 0; jgn_i--)
	{
		if (jgn_ptr_iArray[jgn_i])
		{
			free((jgn_ptr_iArray[jgn_i]));
			jgn_ptr_iArray[jgn_i] = NULL;
			printf("Memoery allocated to row %d has been successfully freed!!!\n\n", jgn_i);
		}
	}

	if (jgn_ptr_iArray)
	{
		free(jgn_ptr_iArray);
		jgn_ptr_iArray = NULL;
		printf("Memory allocated to jgn_ptr_iArray has been successfully freed");
	}
	return(0);
}