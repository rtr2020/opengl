#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	int jgn_iArray[NUM_ROWS][NUM_COLUMNS];
	int jgn_i, jgn_j;

	int *jgn_ptr_iArray_Row = NULL;

	for (jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		jgn_ptr_iArray_Row = jgn_iArray[jgn_i];
		for (jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
			*(jgn_ptr_iArray_Row + jgn_j) = (jgn_i + 1) * (jgn_j + 1);
	}

	printf("\n\n");
	printf("2D Integer array along with address: \n\n");

	for (jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		jgn_ptr_iArray_Row = jgn_iArray[jgn_i];
		for (jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
		{
			printf("*(jgn_ptr_iArray_Row + %d) = %d \t\t At address (jgn_ptr_iArray_Row + jgn_j) : %p\n", jgn_j, *(jgn_ptr_iArray_Row + jgn_j), (jgn_ptr_iArray_Row + jgn_j));
		}

		printf("\n\n");
	}

	return(0);
}