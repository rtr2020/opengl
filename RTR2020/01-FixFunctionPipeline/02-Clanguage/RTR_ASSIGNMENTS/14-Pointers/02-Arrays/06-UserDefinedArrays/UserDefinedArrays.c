#include<stdio.h>
#include<stdlib.h>

#define INT_SIZE sizeof(int)
#define FLOAT_SIZE sizeof(float)
#define DOUBLE_SIZE sizeof(double)
#define CHAR_SIZE sizeof(char)

int main()
{
	int *jgn_ptr_iArray = NULL;
	unsigned int jgn_intArrayLength = 0;

	float *jgn_ptr_fArray = NULL;
	unsigned int jgn_floatArrayLength = 0;

	double *jgn_ptr_dArray = NULL;
	unsigned int jgn_doubleArrayLength = 0;

	char *jgn_ptr_cArray = NULL;
	unsigned int jgn_charArrayLength = 0;

	int jgn_i;

	printf("\n\n");
	printf("Enter the number of elements you want in integer array: ");
	scanf("%u", &jgn_intArrayLength);

	jgn_ptr_iArray = (int*)malloc(jgn_intArrayLength * sizeof(int));
	if (jgn_ptr_iArray == NULL)
	{
		printf("\n\n");
		printf("Memory allocation for integer array failed....Exiting now....\n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory allocation of integer array succeded!!!\n\n");
	}

	printf("\n\n");
	printf("Enter %d integer elements to fill up the array\n\n", jgn_intArrayLength);
	for (jgn_i = 0; jgn_i < jgn_intArrayLength; jgn_i++)
		scanf("%d", (jgn_ptr_iArray + jgn_i));




	printf("\n\n");
	printf("Enter the number of elements you want in float array: ");
	scanf("%u", &jgn_floatArrayLength);

	jgn_ptr_fArray = (float *)malloc(jgn_floatArrayLength * sizeof(float));
	if (jgn_ptr_fArray == NULL)
	{
		printf("\n\n");
		printf("Memory allocation for floating array failed....Exiting now....\n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory allocation of floating array succeded!!!\n\n");
	}

	printf("\n\n");
	printf("Enter %d floating elements to fill up the array\n\n", jgn_floatArrayLength);
	for (jgn_i = 0; jgn_i < jgn_floatArrayLength; jgn_i++)
		scanf("%f", (jgn_ptr_fArray + jgn_i));



	printf("\n\n");
	printf("Enter the number of elements you want in double array: ");
	scanf("%u", &jgn_doubleArrayLength);

	jgn_ptr_dArray = (double *)malloc(jgn_doubleArrayLength * sizeof(double));
	if (jgn_ptr_dArray == NULL)
	{
		printf("\n\n");
		printf("Memory allocation for double array failed....Exiting now....\n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory allocation of double array succeded!!!\n\n");
	}

	printf("\n\n");
	printf("Enter %d double elements to fill up the array\n\n", jgn_doubleArrayLength);
	for (jgn_i = 0; jgn_i < jgn_doubleArrayLength; jgn_i++)
		scanf("%lf", (jgn_ptr_dArray + jgn_i));


	printf("\n\n");
	printf("Enter the number of elements you want in character array: ");
	scanf("%u", &jgn_charArrayLength);

	jgn_ptr_cArray = (char *)malloc(jgn_charArrayLength * sizeof(char));
	if (jgn_ptr_cArray == NULL)
	{
		printf("\n\n");
		printf("Memory allocation for char array failed....Exiting now....\n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory allocation of char array succeded!!!\n\n");
	}

	printf("\n\n");
	printf("Enter %d char elements to fill up the array\n\n", jgn_charArrayLength);
	for (jgn_i = 0; jgn_i < jgn_charArrayLength; jgn_i++)
	{
		*(jgn_ptr_cArray + jgn_i) = getch();
		printf("%c\n", *(jgn_ptr_cArray + jgn_i));
	}


	//Displaying The Array

	printf("\n\n");
	printf("The integer array entered by you consisting of %d elements:\n\n", jgn_intArrayLength);
	for (jgn_i = 0; jgn_i < jgn_intArrayLength; jgn_i++)
	{
		printf("%d \t\t At address : %p\n", *(jgn_ptr_iArray + jgn_i), (jgn_ptr_iArray + jgn_i));
	}

	printf("\n\n");
	printf("The floating array entered by you consisting of %d elements:\n\n", jgn_floatArrayLength);
	for (jgn_i = 0; jgn_i < jgn_floatArrayLength; jgn_i++)
	{
		printf("%f \t\t At address : %p\n", *(jgn_ptr_fArray + jgn_i), (jgn_ptr_fArray + jgn_i));
	}

	printf("\n\n");
	printf("The double array entered by you consisting of %d elements:\n\n", jgn_doubleArrayLength);
	for (jgn_i = 0; jgn_i < jgn_doubleArrayLength; jgn_i++)
	{
		printf("%lf \t\t At address : %p\n", *(jgn_ptr_dArray + jgn_i), (jgn_ptr_dArray + jgn_i));
	}

	printf("\n\n");
	printf("The char array entered by you consisting of %d elements:\n\n", jgn_charArrayLength);
	for (jgn_i = 0; jgn_i < jgn_charArrayLength; jgn_i++)
	{
		printf("%c \t\t At address : %p\n", *(jgn_ptr_cArray + jgn_i), (jgn_ptr_cArray + jgn_i));
	}


	//Freeing memory

	if (jgn_ptr_cArray)
	{
		free(jgn_ptr_cArray);
		jgn_ptr_cArray = NULL;

		printf("\n\n");
		printf("Memory occupied by character array is freed\n\n");
	}

	if (jgn_ptr_dArray)
	{
		free(jgn_ptr_dArray);
		jgn_ptr_dArray = NULL;

		printf("\n\n");
		printf("Memory occupied by double array is freed\n\n");
	}

	if (jgn_ptr_fArray)
	{
		free(jgn_ptr_fArray);
		jgn_ptr_cArray = NULL;

		printf("\n\n");
		printf("Memory occupied by floating array is freed\n\n");
	}

	if (jgn_ptr_iArray)
	{
		free(jgn_ptr_iArray);
		jgn_ptr_iArray = NULL;

		printf("\n\n");
		printf("Memory occupied by integer array is freed\n\n");
	}

	return(0);
}