#include<stdio.h>

int main(void)
{
	int jgn_iArray[] = { 12, 24, 45, 52, 23, 14, 25, 45, 13, 78 };
	int* ptr_iArray = NULL;
	printf("\n\n");
	printf("Using Array name as array i.e : value of xth element of iArray :  iArray[x] AND Address Of xth Element Of iArray : &iArray[x]\n\n");
	printf("Tnteger array elements are: \n\n");
	printf("iArray[0] = %d \t At address : %p\n", jgn_iArray[0], &jgn_iArray[0]);
	printf("iArray[1] = %d \t At address : %p\n", jgn_iArray[1], &jgn_iArray[1]);
	printf("iArray[2] = %d \t At address : %p\n", jgn_iArray[2], &jgn_iArray[2]);
	printf("iArray[3] = %d \t At address : %p\n", jgn_iArray[3], &jgn_iArray[3]);
	printf("iArray[4] = %d \t At address : %p\n", jgn_iArray[4], &jgn_iArray[4]);
	printf("iArray[5] = %d \t At address : %p\n", jgn_iArray[5], &jgn_iArray[5]);
	printf("iArray[6] = %d \t At address : %p\n", jgn_iArray[6], &jgn_iArray[6]);
	printf("iArray[7] = %d \t At address : %p\n", jgn_iArray[7], &jgn_iArray[7]);
	printf("iArray[8] = %d \t At address : %p\n", jgn_iArray[8], &jgn_iArray[8]);
	printf("iArray[9] = %d \t At address : %p\n", jgn_iArray[9], &jgn_iArray[9]);

	ptr_iArray = jgn_iArray;
	printf("\n\n");
	printf("Using pointer as pointer i.e : value of xth element of iArray : * (ptr_iArray + x) AND Address Of xth Element Of iArray : (ptr_iArray + x)\n\n");
	printf("Tnteger array elements are: \n\n");
	printf("*(ptr_iArray + 0) = %d \t At address : %p\n", *(ptr_iArray + 0), (ptr_iArray + 0));
	printf("*(ptr_iArray + 1) = %d \t At address : %p\n", *(ptr_iArray + 1), (ptr_iArray + 1));
	printf("*(ptr_iArray + 2) = %d \t At address : %p\n", *(ptr_iArray + 2), (ptr_iArray + 2));
	printf("*(ptr_iArray + 3) = %d \t At address : %p\n", *(ptr_iArray + 3), (ptr_iArray + 3));
	printf("*(ptr_iArray + 4) = %d \t At address : %p\n", *(ptr_iArray + 4), (ptr_iArray + 4));
	printf("*(ptr_iArray + 5) = %d \t At address : %p\n", *(ptr_iArray + 5), (ptr_iArray + 5));
	printf("*(ptr_iArray + 6) = %d \t At address : %p\n", *(ptr_iArray + 6), (ptr_iArray + 6));
	printf("*(ptr_iArray + 7) = %d \t At address : %p\n", *(ptr_iArray + 7), (ptr_iArray + 7));
	printf("*(ptr_iArray + 8) = %d \t At address : %p\n", *(ptr_iArray + 8), (ptr_iArray + 8));
	printf("*(ptr_iArray + 9) = %d \t At address : %p\n", *(ptr_iArray + 9), (ptr_iArray + 9));

	return(0);
}