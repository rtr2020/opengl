#include<stdio.h>

int main(void)
{
	int jgn_iArray[] = { 12, 24, 45, 52, 23, 14, 25, 45, 13, 78 };
	float jgn_fArray[] = { 8.1f, 5.5f, 4.2f, 4.3f, 5.3f };
	double jgn_dArray[] = { 4.256, 6.2448, 6.3258 };
	char jgn_cArray[] = { 'J', 'I', 'T', 'H', 'I', 'N', '\0' };

	printf("\n\n");
	printf("Tnteger array elements are: \n\n");
	printf("iArray[0] = %d \t At address : %p\n", *(jgn_iArray + 0), (jgn_iArray + 0));
	printf("iArray[1] = %d \t At address : %p\n", *(jgn_iArray + 1), (jgn_iArray + 1));
	printf("iArray[2] = %d \t At address : %p\n", *(jgn_iArray + 2), (jgn_iArray + 2));
	printf("iArray[3] = %d \t At address : %p\n", *(jgn_iArray + 3), (jgn_iArray + 3));
	printf("iArray[4] = %d \t At address : %p\n", *(jgn_iArray + 4), (jgn_iArray + 4));
	printf("iArray[5] = %d \t At address : %p\n", *(jgn_iArray + 5), (jgn_iArray + 5));
	printf("iArray[6] = %d \t At address : %p\n", *(jgn_iArray + 6), (jgn_iArray + 6));
	printf("iArray[7] = %d \t At address : %p\n", *(jgn_iArray + 7), (jgn_iArray + 7));
	printf("iArray[8] = %d \t At address : %p\n", *(jgn_iArray + 8), (jgn_iArray + 8));
	printf("iArray[9] = %d \t At address : %p\n", *(jgn_iArray + 9), (jgn_iArray + 9));

	printf("\n\n");
	printf("Floating array elements are: \n\n");
	printf("fArray[0] = %f \t At address : %p\n", *(jgn_fArray + 0), (jgn_fArray + 0));
	printf("fArray[1] = %f \t At address : %p\n", *(jgn_fArray + 1), (jgn_fArray + 1));
	printf("fArray[2] = %f \t At address : %p\n", *(jgn_fArray + 2), (jgn_fArray + 2));
	printf("fArray[3] = %f \t At address : %p\n", *(jgn_fArray + 3), (jgn_fArray + 3));
	printf("fArray[4] = %f \t At address : %p\n", *(jgn_fArray + 4), (jgn_fArray + 4));

	printf("\n\n");
	printf("Double array elements are: \n\n");
	printf("dArray[0] = %lf \t At address : %p\n", *(jgn_dArray + 0), (jgn_dArray + 0));
	printf("dArray[1] = %lf \t At address : %p\n", *(jgn_dArray + 1), (jgn_dArray + 1));
	printf("dArray[2] = %lf \t At address : %p\n", *(jgn_dArray + 2), (jgn_dArray + 2));

	printf("\n\n");
	printf("Character array elements are: \n\n");
	printf("cArray[0] = %c \t At address : %p\n", *(jgn_cArray + 0), (jgn_cArray + 0));
	printf("cArray[1] = %c \t At address : %p\n", *(jgn_cArray + 1), (jgn_cArray + 1));
	printf("cArray[2] = %c \t At address : %p\n", *(jgn_cArray + 2), (jgn_cArray + 2));
	printf("cArray[3] = %c \t At address : %p\n", *(jgn_cArray + 3), (jgn_cArray + 3));
	printf("cArray[4] = %c \t At address : %p\n", *(jgn_cArray + 4), (jgn_cArray + 4));
	printf("cArray[5] = %c \t At address : %p\n", *(jgn_cArray + 5), (jgn_cArray + 5));
	return(0);
}