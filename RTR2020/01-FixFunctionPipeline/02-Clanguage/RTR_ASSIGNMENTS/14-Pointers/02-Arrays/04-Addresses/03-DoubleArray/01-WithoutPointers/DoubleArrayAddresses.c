#include<stdio.h>

int main()
{
	double jgn_dArray[10];
	int jgn_i;

	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		jgn_dArray[jgn_i] = (double)(jgn_i + 1) * 3.3568;

	printf("\n\n");
	printf("Elements of the double array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_dArray[%d] = %lf\n", jgn_i, jgn_dArray[jgn_i]);

	printf("\n\n");
	printf("Elements of the double array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_dArray[%d] = %lf \t\t Address = %p\n", jgn_i, jgn_dArray[jgn_i], &jgn_dArray[jgn_i]);
	return(0);
}