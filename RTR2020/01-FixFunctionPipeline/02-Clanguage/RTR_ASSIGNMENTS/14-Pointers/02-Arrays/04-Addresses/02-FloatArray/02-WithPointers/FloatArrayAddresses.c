#include<stdio.h>

int main()
{
	float jgn_fArray[10];
	float* ptr_fArray = NULL;
	int jgn_i;

	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		jgn_fArray[jgn_i] = (float)(jgn_i + 1) * 1.5f;

	ptr_fArray = jgn_fArray;

	printf("\n\n");
	printf("Elements of the float array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_fArray[%d] = %f\n", jgn_i, *(ptr_fArray + jgn_i));

	printf("\n\n");
	printf("Elements of the float array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_fArray[%d] = %f \t\t Address = %p\n", jgn_i, *(ptr_fArray + jgn_i), (ptr_fArray + jgn_i));
	return(0);
}