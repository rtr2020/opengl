#include<stdio.h>

int main()
{
	int jgn_iArray[10];
	int jgn_i;

	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		jgn_iArray[jgn_i] = (jgn_i + 1) * 3;
	printf("\n\n");

	printf("Elements of the integer array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_iArray[%d] = %d\n", jgn_i, jgn_iArray[jgn_i]);

	printf("\n\n");
	printf("Elements of the integer array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_iArray[%d] = %d \t\t Address = %p\n", jgn_i, jgn_iArray[jgn_i], &jgn_iArray[jgn_i]);
	return(0);
}