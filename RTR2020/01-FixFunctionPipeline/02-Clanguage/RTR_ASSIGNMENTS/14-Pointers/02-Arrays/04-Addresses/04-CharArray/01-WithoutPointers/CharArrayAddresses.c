#include<stdio.h>

int main()
{
	char jgn_cArray[10];
	int jgn_i;

	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		jgn_cArray[jgn_i] = (char)(jgn_i + 65);
	printf("\n\n");

	printf("Elements of the char array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_cArray[%d] = %c\n", jgn_i, jgn_cArray[jgn_i]);

	printf("\n\n");
	printf("Elements of the char array: \n\n");
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("jgn_cArray[%d] = %c \t\t Address = %p\n", jgn_i, jgn_cArray[jgn_i], &jgn_cArray[jgn_i]);
	return(0);
}