#include<stdio.h>
#include<stdlib.h>

int main(void)
{
	void MyAlloc(int** ptr, unsigned int numberOfElements);

	int* jgn_piArray = NULL;
	unsigned int jgn_numElements;
	int jgn_i;

	printf("\n\n");
	printf("How many elements you want in your integer array\n");
	scanf("%d", &jgn_numElements);

	printf("\n\n");
	MyAlloc(&jgn_piArray, jgn_numElements);

	printf("\n\n");
	printf("Enter %d number into the array: ", jgn_numElements);
	for (jgn_i = 0; jgn_i < jgn_numElements; jgn_i++)
		scanf("%d", &jgn_piArray[jgn_i]);

	printf("\n\n");
	printf("Elements entered by you are: \n\n");

	for (jgn_i = 0; jgn_i < jgn_numElements; jgn_i++)
		printf("%u\n", jgn_piArray[jgn_i]);

	printf("\n\n");
	if (jgn_piArray)
	{
		free(jgn_piArray);
		jgn_piArray = NULL;
		printf("Memory allocation has been successfully freed\n\n");

	}
	return(0);
}

void MyAlloc(int** ptr, unsigned int numberOfElements)
{
	*ptr = (int*)malloc(numberOfElements * sizeof(int));
	if (*ptr == NULL)
	{
		printf("Could not allocate memory....Exiting\n\n");
		exit(0);
	}

	printf("Memoery of %lu allocated succesfully\n\n", numberOfElements * sizeof(int));
}