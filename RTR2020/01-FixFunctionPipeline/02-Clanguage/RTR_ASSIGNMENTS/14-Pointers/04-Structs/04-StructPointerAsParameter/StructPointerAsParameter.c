#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
};

int main(void)
{
	void ChangeValues(struct MyData *);
	struct MyData* pData = NULL;

	printf("\n\n");
	pData = (struct MyData*)malloc(sizeof(struct MyData));
	if (pData == NULL)
	{
		printf("Failed to allocate memory to struct\n\n");
		exit(0);
	}
	else
	{
		printf("Successfully allocated the memory\n\n");
	}

	pData->jgn_i = 30;
	pData->jgn_f = 11.45f;
	pData->jgn_d = 3.2834;

	printf("\n\n");


	printf("\n\n");
	printf("Printing the data memebrs of the struct MyData Before Changing Values\n\n");
	printf("i = %d\n", pData->jgn_i);
	printf("f = %f\n", pData->jgn_f);
	printf("d = %lf\n", pData->jgn_d);



	ChangeValues(pData);

	printf("\n\n");
	printf("Printing the data memebrs of the struct MyData After changing Values\n\n");
	printf("i = %d\n", pData->jgn_i);
	printf("f = %f\n", pData->jgn_f);
	printf("d = %lf\n", pData->jgn_d);

	if (pData)
	{
		free(pData);
		pData = NULL;
		printf("Memory allocated to struct MyData is successfully freed\n\n");
	}


	return(0);
}

void ChangeValues(struct MyData* param_data)
{
	param_data->jgn_i = 9;
	param_data->jgn_f = 9.7f;
	param_data->jgn_d = 56.7777;

}