#include<stdio.h>

struct MyData
{
	int* jgn_ptri;
	int jgn_i;

	float* jgn_ptrf;
	float jgn_f;

	double *jgn_ptrd;
	double jgn_d;
};

int main(void)
{
	struct MyData data;

	data.jgn_i = 8;
	data.jgn_ptri = &data.jgn_i;

	data.jgn_f = 8.6f;
	data.jgn_ptrf = &data.jgn_f;

	data.jgn_d = 7.8686;
	data.jgn_ptrd = &data.jgn_d;

	printf("\n\n");
	printf("i = %d\n", *(data.jgn_ptri));
	printf("Address of 'i' = %p\n", data.jgn_ptri);

	printf("\n\n");
	printf("i = %f\n", *(data.jgn_ptrf));
	printf("Address of 'f' = %p\n", data.jgn_ptrf);

	printf("\n\n");
	printf("i = %lf\n", *(data.jgn_ptrd));
	printf("Address of 'd' = %p\n", data.jgn_ptrd);
}