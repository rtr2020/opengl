#include<stdio.h>

struct MyData
{
	int* jgn_ptri;
	int jgn_i;

	float* jgn_ptrf;
	float jgn_f;

	double* jgn_ptrd;
	double jgn_d;
};

int main(void)
{
	struct MyData* pdata;

	printf("\n\n");
	pdata = (struct MyData*)malloc(sizeof(struct MyData));
	if (pdata == NULL)
	{
		printf("Allocation of memory for struct MyData failed\n\n");
		exit(0);
	}
	else
	{
		printf("Successfully allocated memory for struct MyData\n\n");
	}

	pdata->jgn_i = 8;
	(*pdata).jgn_ptri = &(pdata->jgn_i);

	pdata->jgn_f = 8.6f;
	pdata->jgn_ptrf = &(pdata->jgn_f);

	pdata->jgn_d = 7.8686;
	pdata->jgn_ptrd = &(pdata->jgn_d);

	printf("\n\n");
	printf("i = %d\n", *(pdata->jgn_ptri));
	printf("Address of 'i' = %p\n", pdata->jgn_ptri);

	printf("\n\n");
	printf("i = %f\n", *(pdata->jgn_ptrf));
	printf("Address of 'f' = %p\n", pdata->jgn_ptrf);

	printf("\n\n");
	printf("i = %lf\n", *(pdata->jgn_ptrd));
	printf("Address of 'd' = %p\n", pdata->jgn_ptrd);

	return(0);
}