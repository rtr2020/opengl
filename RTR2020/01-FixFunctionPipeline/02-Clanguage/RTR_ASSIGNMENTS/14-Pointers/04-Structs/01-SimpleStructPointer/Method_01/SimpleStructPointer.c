#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
};

int main(void)
{
	int jgn_iSize;
	int jgn_fSize;
	int jgn_dSize;
	int structMydataSize;
	int	pointerToStructMydataSize;

	struct MyData* pData = NULL;

	printf("\n\n");
	pData = (struct MyData*)malloc(sizeof(struct MyData));
	if (pData == NULL)
	{
		printf("Failed to allocate memory to struct\n\n");
		exit(0);
	}
	else
	{
		printf("Successfully allocated the memory\n\n");
	}

	(*pData).jgn_i = 30;
	(*pData).jgn_f = 11.45f;
	(*pData).jgn_d = 3.2834;

	printf("\n\n");
	printf("Data members of	struct MyData are \n\n");
	printf("i = %d\n", (*pData).jgn_i);
	printf("f = %f\n", (*pData).jgn_f);
	printf("d = %lf\n", (*pData).jgn_d);

	jgn_iSize = sizeof((*pData).jgn_i);
	jgn_fSize = sizeof((*pData).jgn_f);
	jgn_dSize = sizeof((*pData).jgn_d);

	printf("\n\n");
	printf("Printing the size of data memebrs of the struct MyData\n\n");
	printf("Size of i = %d bytes\n", jgn_iSize);
	printf("Size of f = %d bytes\n", jgn_fSize);
	printf("Size of d = %d bytes\n", jgn_dSize);

	structMydataSize = sizeof(struct MyData);
	pointerToStructMydataSize = sizeof(struct MyData*);

	printf("\n\n");
	printf("Size of 'struct MyData' = %d bytes \n\n", structMydataSize);
	printf("Size of pointer of struct MyData = %d bytes\n\n", pointerToStructMydataSize);

	if (pData)
	{
		free(pData);
		pData = NULL;
		printf("Memory allocated to struct MyData is successfully freed\n\n");
	}

	return(0);
}