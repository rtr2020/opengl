#include<stdio.h>
#include<ctype.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char jgn_name[NAME_LENGTH];
	int jgn_age;
	char jgn_sex;
	float jgn_salary;
	char jgn_marital_status;
};

int main(void)
{
	void MyGetString(char[], int);

	struct Employee* pEmployeeRecord = NULL;
	int jgn_numEmployees, jgn_i;

	printf("\n\n");
	printf("Enter number of employees details you want to record: ");
	scanf("%d", &jgn_numEmployees);

	printf("\n\n");
	pEmployeeRecord = (struct Employee*)malloc(sizeof(struct Employee) * jgn_numEmployees);

	if (pEmployeeRecord == NULL)
	{
		printf("Failed to allocate memory for employess\n\n");
		exit(0);
	}
	else
	{
		printf("Memory allocation successfull");
	}

	for (jgn_i = 0; jgn_i < jgn_numEmployees; jgn_i++)
	{
		printf("\n\n");
		printf("***************Data Entry for Employee %d ****************\n", jgn_i + 1);
		
		printf("\n\n");
		printf("Enter Employee Name: ");
		MyGetString(pEmployeeRecord[jgn_i].jgn_name, NAME_LENGTH);
		
		printf("\n\n");
		printf("Enter employee age in years: ");
		scanf("%d", &pEmployeeRecord[jgn_i].jgn_age);

		printf("\n\n");
		printf("Enter employee sex (M/F): ");
		printf("%c", pEmployeeRecord[jgn_i].jgn_sex);
		pEmployeeRecord[jgn_i].jgn_sex = toupper(pEmployeeRecord[jgn_i].jgn_sex);

		printf("\n\n");
		printf("Enter Employee Salary: ");
		scanf("%f", &pEmployeeRecord[jgn_i].jgn_salary);

		printf("\n\n");
		printf("Is the employee married? Enter (Y/y) for yes ...(N/n) for No: ");
		pEmployeeRecord[jgn_i].jgn_marital_status = getch();
		pEmployeeRecord[jgn_i].jgn_marital_status = toupper(pEmployeeRecord[jgn_i].jgn_marital_status);


		printf("\n\n\n");

	}

	printf("****************Displayiong Employee Records**************\n\n");
	for (jgn_i = 0; jgn_i < jgn_numEmployees; jgn_i++)
	{
		printf("**************Employee Number %d ******************\n\n", jgn_i + 1);
		printf("Employee Name = %s\n", pEmployeeRecord[jgn_i].jgn_name);
		printf("Employee Age = %d\n", pEmployeeRecord[jgn_i].jgn_age);

		if (pEmployeeRecord[jgn_i].jgn_sex == 'M')
			printf("Employee Sex = Male\n\n");
		else if (pEmployeeRecord[jgn_i].jgn_sex == 'F')
			printf("Employee Sex = Female\n\n");
		else
			printf("Employement sex = Invalid data entered\n\n");

		printf("Employee Salary = %f\n\n", pEmployeeRecord[jgn_i].jgn_salary);
		
		printf("\n\n");
		if (pEmployeeRecord[jgn_i].jgn_marital_status == 'Y')
			printf("Employee Maritial Status = Married\n\n");
		else if (pEmployeeRecord[jgn_i].jgn_marital_status == 'N')
			printf("Employee Maritial Status = UnMarried\n\n");
		else
			printf("Employee Maritial Status = Invalid data entered\n\n");
	
		printf("\n\n");
	}

	if (pEmployeeRecord)
	{
		free(pEmployeeRecord);
		pEmployeeRecord = NULL;
		printf("Memory allocated to the Employee is successfully freed\n\n");
	}

	return(0);
}


void MyGetString(char str[], int str_size)
{
	int jgn_i;
	char jgn_ch = '\0';

	jgn_i = 0;

	do {
		jgn_ch = getch();
		str[jgn_i] = jgn_ch;

		printf("%c", str[jgn_i]);
	} while ((jgn_ch != '\r') && (jgn_i < str_size));

	if (jgn_i == str_size)
		str[jgn_i - 1] = '\0';
	else
		str[jgn_i] = '\0';
}