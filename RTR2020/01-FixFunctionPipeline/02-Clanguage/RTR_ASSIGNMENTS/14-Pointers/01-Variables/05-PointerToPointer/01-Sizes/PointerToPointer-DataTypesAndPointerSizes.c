#include<stdio.h>

struct Employee
{
	char jgn_name[100];
	int jgn_age;
	float jgn_salary;
	char jgn_sex;
	char jgn_maritial_status;
};

int main(void)
{
	printf("\n\n");
	printf("Size of data types and pointers to those respective data types:\n\n");
	printf("Size of (int) : %d	\t\t Size of pointer to int (int *) : %d \t\t Size of pointer to pointer to int (int **) : %d\n", sizeof(int), sizeof(int*), sizeof(int**));
	printf("Size of (float) : %d	\t\t Size of pointer to float (float *) : %d \t\t Size of pointer to pointer to float (float **) : %d\n", sizeof(float), sizeof(float*), sizeof(float**));
	printf("Size of (double) : %d	\t\t Size of pointer to double (double *) : %d \t\t Size of pointer to pointer to double (double **) : %d\n", sizeof(double), sizeof(double*), sizeof(double**));
	printf("Size of (char) : %d	\t\t Size of pointer to char (char *) : %d \t\t Size of pointer to pointer to char (char **) : %d\n", sizeof(char), sizeof(char*), sizeof(char**));
	printf("Size of (struct Employee) : %d	\t\t Size of pointer to struct Employee (struct Employee *) : %d \t\t Size of pointer to pointer to struct Employee (struct Employee **) : %d\n", sizeof(struct Employee), sizeof(struct Employee*), sizeof(struct Employee**));
	return(0);
}