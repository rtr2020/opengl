#include<stdio.h>

int main(void)
{
	float jgn_num;
	float *ptr = NULL;
	jgn_num = 5.8f;

	printf("\n\n");

	printf("Before ptr = &jgn_num\n\n");
	printf("Value of jgn_num  = %f\n\n", jgn_num);
	printf("Address of jgn_num  = %p\n\n", &jgn_num);
	printf("Value at address of jgn_num = %f\n\n", *(&jgn_num));

	ptr = &jgn_num;

	printf("After ptr = &jgn_num\n\n");
	printf("Value of jgn_num  = %f\n\n", jgn_num);
	printf("Address of jgn_num  = %p\n\n", ptr);
	printf("Value at address of jgn_num = %f\n\n", *ptr);

	return(0);
}