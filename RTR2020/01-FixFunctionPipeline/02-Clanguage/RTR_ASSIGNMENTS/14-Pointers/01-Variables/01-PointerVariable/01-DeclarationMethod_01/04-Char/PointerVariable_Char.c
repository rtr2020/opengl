#include<stdio.h>

int main(void)
{
	char jgn_ch;
	char *ptr = NULL;
	jgn_ch = 'J';

	printf("\n\n");

	printf("Before ptr = &jgn_ch\n\n");
	printf("Value of jgn_ch  = %c\n\n", jgn_ch);
	printf("Address of jgn_ch  = %p\n\n", &jgn_ch);
	printf("Value at address of jgn_ch = %c\n\n", *(&jgn_ch));

	ptr = &jgn_ch;

	printf("After ptr = &jgn_ch\n\n");
	printf("Value of jgn_ch  = %c\n\n", jgn_ch);
	printf("Address of jgn_ch  = %p\n\n", ptr);
	printf("Value at address of jgn_ch = %c\n\n", *ptr);

	return(0);
}