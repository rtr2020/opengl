#include<stdio.h>

int main(void)
{
	int jgn_num;
	int *ptr = NULL;
	jgn_num = 10;

	printf("\n\n");

	printf("Before ptr = &jgn_num\n\n");
	printf("Value of jgn_num  = %d\n\n", jgn_num);
	printf("Address of jgn_num  = %p\n\n", &jgn_num);
	printf("Value at address of jgn_num = %d\n\n", *(&jgn_num));

	ptr = &jgn_num;

	printf("After ptr = &jgn_num\n\n");
	printf("Value of jgn_num  = %d\n\n", jgn_num);
	printf("Address of jgn_num  = %p\n\n", ptr);
	printf("Value at address of jgn_num = %d\n\n", *ptr);

	return(0);
}