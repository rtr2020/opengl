#include<stdio.h>

int main(void)
{
	double jgn_num;
	double* ptr = NULL;
	jgn_num = 5.858584;

	printf("\n\n");

	printf("Before ptr = &jgn_num\n\n");
	printf("Value of jgn_num  = %lf\n\n", jgn_num);
	printf("Address of jgn_num  = %p\n\n", &jgn_num);
	printf("Value at address of jgn_num = %lf\n\n", *(&jgn_num));

	ptr = &jgn_num;

	printf("After ptr = &jgn_num\n\n");
	printf("Value of jgn_num  = %lf\n\n", jgn_num);
	printf("Address of jgn_num  = %p\n\n", ptr);
	printf("Value at address of jgn_num = %lf\n\n", *ptr);

	return(0);
}