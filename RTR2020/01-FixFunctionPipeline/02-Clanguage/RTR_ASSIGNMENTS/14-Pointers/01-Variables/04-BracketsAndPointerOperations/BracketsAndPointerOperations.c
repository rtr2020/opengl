#include<stdio.h>

int main(void)
{
	int jgn_num;
	int *ptr = NULL;
	int jgn_ans;

	jgn_num = 5;
	ptr = &jgn_num;

	printf("\n\n");
	printf(" num		= %d\n", jgn_num);
	printf(" &num		= %p\n", &jgn_num);
	printf(" *(&num)		= %d\n", *(&jgn_num));
	printf(" ptr		= %p\n", ptr);
	printf(" *ptr		= %d\n", *ptr);

	printf("\n\n");
	printf("Answer of (ptr + 10) = %p\n\n", (ptr + 10));

	printf("Answer of *(ptr + 10) = %d\n\n", *(ptr + 10));

	printf("Answer of (*ptr + 10) = %d\n\n", (*ptr + 10));

	++* ptr;
	printf("Answer of ++*ptr :	%d\n\n", *ptr);

	*ptr++;
	printf("Answer of *ptr++ :	%d\n\n", *ptr);

	ptr = &jgn_num;
	(*ptr)++;

	printf("Answer of (*ptr)++ :	%d\n\n", *ptr);
	
	return(0);
}