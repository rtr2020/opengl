#include<stdio.h>

struct Employee
{
	char jgn_name[100];
	int jgn_age;
	float jgn_salary;
	char jgn_sex;
	char jgn_marital_status;
};

int main(void)
{
	printf("\n\n");

	printf("Size of data types and pointers to the respective data types are: \n\n");

	printf("Size of (int)		:%d \t\t\t Size of int (int*)		:%d\n\n", sizeof(int), sizeof(int*));
	printf("Size of (float)		:%d \t\t\t Size of int (float*)		:%d\n\n", sizeof(float), sizeof(float*));
	printf("Size of (double)		:%d \t\t\t Size of int (int*)		:%d\n\n", sizeof(double), sizeof(double*));
	printf("Size of (char)		:%d \t\t\t Size of int (int*)		:%d\n\n", sizeof(char), sizeof(char*));
	printf("Size of (struct Employee)		:%d \t\t\t Size of struct Employee (struct Employee*)		:%d\n\n", sizeof(struct Employee), sizeof(struct Employee*));

	return(0);
}