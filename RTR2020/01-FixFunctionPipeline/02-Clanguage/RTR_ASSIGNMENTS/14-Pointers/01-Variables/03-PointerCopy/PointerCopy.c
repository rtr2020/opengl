
#include<stdio.h>

int main(void)
{
	int jgn_num;
	int *ptr = NULL;
	int *copy_ptr = NULL;

	jgn_num = 5;
	ptr = &jgn_num;

	printf("\n\n");
	printf("Before Copy\n\n");
	printf(" num		= %d\n", jgn_num);
	printf(" &num		= %p\n", &jgn_num);
	printf(" *(&num)		= %d\n", *(&jgn_num));
	printf(" ptr		= %p\n", ptr);
	printf(" *ptr		= %d\n", *ptr);

	copy_ptr = ptr;
	printf("\n\n");
	printf("After Copy\n\n");
	printf(" num		= %d\n", jgn_num);
	printf(" &num		= %p\n", &jgn_num);
	printf(" *(&num)		= %d\n", *(&jgn_num));
	printf(" ptr		= %p\n", ptr);
	printf(" *ptr		= %d\n", *ptr);
	printf(" copy_ptr		= %p\n", copy_ptr);
	printf(" *copy_ptr		= %d\n", *copy_ptr);
	return(0);
}