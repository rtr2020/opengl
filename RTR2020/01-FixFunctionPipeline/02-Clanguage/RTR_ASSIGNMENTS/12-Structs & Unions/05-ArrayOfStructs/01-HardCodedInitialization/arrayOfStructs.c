#include<stdio.h>

#define NAME_LENGTH 100
#define MARITIAL_STATUS 10

struct Employee
{
	char jgn_name[NAME_LENGTH];
	int jgn_age;
	float jgn_salary;
	char jgn_sex;
	char jgn_maritial_status[MARITIAL_STATUS];
};

int main(void)
{
	struct Employee EmployeeReocord[5];

	char employee_rajesh[] = "Rajesh";
	char employee_sameer[] = "Sameer";
	char employee_kalyani[] = "Kalyani";
	char employee_sonal[] = "Sonal";
	char employee_raj[] = "Raj";

	int jgn_i;

	strcpy(EmployeeReocord[0].jgn_name, employee_rajesh);
	EmployeeReocord[0].jgn_age = 30;
	EmployeeReocord[0].jgn_salary = 50000.0f;
	EmployeeReocord[0].jgn_sex = 'M';
	strcpy(EmployeeReocord[0].jgn_maritial_status, "Unmarried");

	strcpy(EmployeeReocord[1].jgn_name, employee_sameer);
	EmployeeReocord[1].jgn_age = 60;
	EmployeeReocord[1].jgn_salary = 60000.0f;
	EmployeeReocord[1].jgn_sex = 'M';
	strcpy(EmployeeReocord[1].jgn_maritial_status, "Married");

	strcpy(EmployeeReocord[2].jgn_name, employee_kalyani);
	EmployeeReocord[2].jgn_age = 40;
	EmployeeReocord[2].jgn_salary = 80000.0f;
	EmployeeReocord[2].jgn_sex = 'F';
	strcpy(EmployeeReocord[2].jgn_maritial_status, "Unmarried");

	strcpy(EmployeeReocord[3].jgn_name, employee_sonal);
	EmployeeReocord[3].jgn_age = 36;
	EmployeeReocord[3].jgn_salary = 90000.0f;
	EmployeeReocord[3].jgn_sex = 'F';
	strcpy(EmployeeReocord[3].jgn_maritial_status, "Unmarried");

	strcpy(EmployeeReocord[4].jgn_name, employee_raj);
	EmployeeReocord[4].jgn_age = 50;
	EmployeeReocord[4].jgn_salary = 70000.0f;
	EmployeeReocord[4].jgn_sex = 'M';
	strcpy(EmployeeReocord[4].jgn_maritial_status, "Unmarried");
	
	printf("\n\n");
	printf("Displaying employee records:\n\n");

	for (jgn_i = 0; jgn_i < 5; jgn_i++)
	{
		printf("*******Employee Number %d*******\n\n", (jgn_i + 1));
		printf("Name			:	%s\n", EmployeeReocord[jgn_i].jgn_name);
		printf("Age			:	%d\n", EmployeeReocord[jgn_i].jgn_age);

		if (EmployeeReocord[0].jgn_sex == 'M' || EmployeeReocord[0].jgn_sex == 'm')
			printf("Sex			:	Male\n");
		else
			printf("Sex			:	Female\n");

		printf("Salary			:	Rs. %f\n", EmployeeReocord[0].jgn_salary);
		printf("Maritial Status		:	%s\n", EmployeeReocord[0].jgn_maritial_status);
		printf("\n\n");
	}

	return(0);
}