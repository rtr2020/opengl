#include<stdio.h>
#include<ctype.h>
#include<string.h>

#define MAX_STRING_LENGTH 1024

struct CharacterCount
{
	char jgn_ch;
	int jgn_chCount;
}character_and_count[] = {  {'A', 0},
						    {'B', 0},
							{'C', 0},
							{'D', 0}, 
							{'E', 0}, 
							{'F', 0}, 
							{'G', 0}, 
							{'H', 0}, 
							{'I', 0}, 
							{'J', 0}, 
							{'K', 0}, 
							{'L', 0}, 
							{'M', 0}, 
							{'N', 0}, 
							{'O', 0}, 
							{'P', 0}, 
							{'Q', 0}, 
							{'R', 0}, 
							{'S', 0}, 
							{'T', 0}, 
							{'U', 0}, 
							{'V', 0},
							{'W', 0}, 
							{'X', 0}, 
							{'Y', 0}, 
							{'Z', 0}};

#define SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS sizeof(character_and_count)
#define SIZE_OF_ONE_STRUCT_FROM_ARRAY_OF_STRUCTS sizeof(character_and_count[0])
#define NUM_ELEMENTS_IN_ARRAY (SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS / SIZE_OF_ONE_STRUCT_FROM_ARRAY_OF_STRUCTS)

int main(void)
{
	char jgn_str[MAX_STRING_LENGTH];
	int jgn_i, jgn_j, jgn_actualStringLength;

	printf("\n\n");
	printf("Enter a string: \n\n");
	gets_s(jgn_str, MAX_STRING_LENGTH);

	jgn_actualStringLength = strlen(jgn_str);

	printf("\n\n");
	printf("The string you have entered is :\n\n");
	printf("%s\n\n", jgn_str);

	for (jgn_i = 0; jgn_i < jgn_actualStringLength; jgn_i++)
	{
		for (jgn_j = 0; jgn_j < NUM_ELEMENTS_IN_ARRAY; jgn_j++)
		{
			jgn_str[jgn_i] = toupper(jgn_str[jgn_i]);

			if (jgn_str[jgn_i] == character_and_count[jgn_j].jgn_ch)
				character_and_count[jgn_j].jgn_chCount++;
		}
	}

	printf("\n\n");
	printf("The number of occurences of all characters from alphabets are as follows:\n\n");
	for (jgn_i = 0; jgn_i < NUM_ELEMENTS_IN_ARRAY; jgn_i++)
	{
		printf("Character %c = %d\n", character_and_count[jgn_i].jgn_ch, character_and_count[jgn_i].jgn_chCount);
	}
	printf("\n\n");

	return(0);
}