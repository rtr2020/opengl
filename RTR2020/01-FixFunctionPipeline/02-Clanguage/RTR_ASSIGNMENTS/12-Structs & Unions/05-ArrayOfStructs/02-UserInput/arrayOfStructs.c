#include<stdio.h>
#include <ctype.h>

#define NUM_EMPLOYEES 5
#define NAME_LENGTH 100
#define MARITIAL_STATUS 10

struct Employee
{
	char jgn_name[NAME_LENGTH];
	int jgn_age;
	float jgn_salary;
	char jgn_sex;
	char jgn_maritial_status;
};

int main(void)
{
	void MyGetString(char[], int);

	struct Employee EmployeeReocord[NUM_EMPLOYEES];

	int jgn_i;


	printf("\n\n");
	printf("Displaying employee records:\n\n");

	for (jgn_i = 0; jgn_i < 5; jgn_i++)
	{
		printf("*******Data entry for Employee Number %d*******\n\n", (jgn_i + 1));
		printf("Enter Employee name: ");
		MyGetString(EmployeeReocord[jgn_i].jgn_name, NAME_LENGTH);

		printf("\n\n");
		printf("Enter employee age in years: ");
		scanf("%d", &EmployeeReocord[jgn_i].jgn_age);

		printf("\n\n");
		printf("Enter employee sex (M/F): ");
		EmployeeReocord[jgn_i].jgn_sex = getch();
		EmployeeReocord[jgn_i].jgn_sex = toupper(EmployeeReocord[jgn_i].jgn_sex);

		printf("\n\n");
		printf("Enter employee salary in rupees: ");
		scanf("%f", &EmployeeReocord[jgn_i].jgn_salary);

		printf("\n\n");
		printf("Is employee married (Y/N): ");
		EmployeeReocord[jgn_i].jgn_maritial_status = getch();
		EmployeeReocord[jgn_i].jgn_maritial_status = toupper(EmployeeReocord[jgn_i].jgn_maritial_status);
	}

	for (jgn_i = 0; jgn_i < 5; jgn_i++)
	{
		printf("*******Employee Number %d*******\n\n", (jgn_i + 1));
		printf("Name			:	%s\n", EmployeeReocord[jgn_i].jgn_name);
		printf("Age			:	%d\n", EmployeeReocord[jgn_i].jgn_age);

		if (EmployeeReocord[0].jgn_sex == 'M' || EmployeeReocord[0].jgn_sex == 'm')
			printf("Sex			:	Male\n");
		else
			printf("Sex			:	Female\n");

		printf("Salary			:	Rs. %f\n", EmployeeReocord[0].jgn_salary);
		if (EmployeeReocord[jgn_i].jgn_maritial_status == 'Y')
			printf("Maritial Status		:	Married\n");
		else if (EmployeeReocord[jgn_i].jgn_maritial_status == 'N')
			printf("Maritial Status		:	Unmarried\n");
		else
			printf("Invalid Value entered\n");
		printf("\n\n");
	}

	return(0);
}

void MyGetString(char str[], int str_size)
{
	int jgn_i;
	char jgn_ch = '\0';

	jgn_i = 0;
	do {
		jgn_ch = getch();
		str[jgn_i] = jgn_ch;
		printf("%c", str[jgn_i]);
		jgn_i++;
	} while ((jgn_ch != '\r') && (jgn_i < str_size));

	if (jgn_i == str_size)
		str[jgn_i - 1] = '\0';
	else
		str[jgn_i] = '\0';
}