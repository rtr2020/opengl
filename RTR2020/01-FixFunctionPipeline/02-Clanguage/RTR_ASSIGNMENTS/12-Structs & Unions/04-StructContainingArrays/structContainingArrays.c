#include<stdio.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20

#define ALPHABET_BEGINNING 65

struct MyDataOne
{
	int jgn_iArray[INT_ARRAY_SIZE];
	float jgn_fArray[FLOAT_ARRAY_SIZE];
};

struct MyDataTwo
{
	char jgn_cArray[CHAR_ARRAY_SIZE];
	char jgn_strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main(void)
{
	struct MyDataOne jgn_data_one;
	struct MyDataTwo jgn_data_two;
	int jgn_i;

	jgn_data_one.jgn_fArray[0] = 0.1f;
	jgn_data_one.jgn_fArray[1] = 1.1f;
	jgn_data_one.jgn_fArray[2] = 2.1f;
	jgn_data_one.jgn_fArray[3] = 3.1f;
	jgn_data_one.jgn_fArray[4] = 4.1f;

	printf("\n\n");
	printf("Enter %d Integers:\n", INT_ARRAY_SIZE);

	for (jgn_i = 0; jgn_i < INT_ARRAY_SIZE; jgn_i++)
		scanf("%d", &jgn_data_one.jgn_iArray[jgn_i]);
	for (jgn_i = 0; jgn_i < CHAR_ARRAY_SIZE; jgn_i++)
		jgn_data_two.jgn_cArray[jgn_i] = (char)(jgn_i + ALPHABET_BEGINNING);

	strcpy(jgn_data_two.jgn_strArray[0], "Welcome!!!");
	strcpy(jgn_data_two.jgn_strArray[1], "to");
	strcpy(jgn_data_two.jgn_strArray[2], "rtr");
	strcpy(jgn_data_two.jgn_strArray[4], "batch");
	strcpy(jgn_data_two.jgn_strArray[5], "of");
	strcpy(jgn_data_two.jgn_strArray[6], "2020");
	strcpy(jgn_data_two.jgn_strArray[7], "We");
	strcpy(jgn_data_two.jgn_strArray[8], "welcome");
	strcpy(jgn_data_two.jgn_strArray[9], "you");

	printf("\n\n");
	printf("Members Of 'struct DataOne' alongwith their assigned values are : \n\n");
	printf("\n\n");
	printf("Integer array jgn_data_one.iArray[] :\n\n");
	for(jgn_i = 0; jgn_i < INT_ARRAY_SIZE; jgn_i++)
		printf("jgn_data_one.jgn_iArray[%d] = %d\n", jgn_i, jgn_data_one.jgn_iArray[jgn_i]);

	printf("\n\n");

	printf("Floating array jgn_data_one.jgn_fArray[] :\n\n");
	for (jgn_i = 0; jgn_i < FLOAT_ARRAY_SIZE; jgn_i++)
		printf("jgn_data_one.jgn_fArray[%d] = %f\n", jgn_i, jgn_data_one.jgn_fArray[jgn_i]);

	printf("\n\n");

	printf("Members Of 'struct DataTwo' alongwith their assigned values are : \n\n");
	printf("\n\n");
	printf("character array jgn_data_two.jgn_cArray[] :\n\n");
	for (jgn_i = 0; jgn_i < CHAR_ARRAY_SIZE; jgn_i++)
		printf("jgn_data_two.cArray[%d] = %c\n", jgn_i, jgn_data_two.jgn_cArray[jgn_i]);

	printf("\n\n");

	printf("String array jgn_data_two.jgn_strArray[] :\n\n");
	for (jgn_i = 0; jgn_i < NUM_STRINGS; jgn_i++)
		printf("%s ", jgn_data_two.jgn_strArray[jgn_i]);
	printf("\n\n");

	return(0);
}