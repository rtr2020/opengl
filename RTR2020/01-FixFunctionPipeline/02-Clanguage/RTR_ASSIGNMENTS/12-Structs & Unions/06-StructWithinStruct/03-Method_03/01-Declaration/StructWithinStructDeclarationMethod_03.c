#include<stdio.h>

int main(void)
{


	struct Rectangle
	{
		struct MyPoint
		{
			int x;
			int y;
		} point_1, point_2;
	} rect;

	int jgn_length, jgn_breadth, jgn_area;

	printf("\n\n");
	printf("Enter left most X cordinate of rectangle: ");
	scanf("%d", &rect.point_1.x);

	printf("\n\n");
	printf("Enter bottom Y most cordinate of rectangle: ");
	scanf("%d", &rect.point_1.y);

	printf("\n\n");
	printf("Enter Right most X cordinate of rectangle: ");
	scanf("%d", &rect.point_2.x);

	printf("\n\n");
	printf("Enter top most Y cordinate of rectangle: ");
	scanf("%d", &rect.point_2.y);

	jgn_length = rect.point_2.y - rect.point_1.y;
	if (jgn_length < 0)
		jgn_length = jgn_length * -1;
	jgn_breadth = rect.point_2.x - rect.point_1.x;
	if (jgn_breadth < 0)
		jgn_breadth = jgn_breadth * -1;

	jgn_area = jgn_length * jgn_breadth;

	printf("\n\n");
	printf("Length of Rectangle = %d\n\n", jgn_length);
	printf("Breadth of Rectangle = %d\n\n", jgn_breadth);
	printf("Area of Rectangle = %d\n\n", jgn_area);

	return(0);
}