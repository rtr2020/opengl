#include<stdio.h>

struct MyNumber
{
	int jgn_num;
	int jgn_num_table[10];
};

struct NumTables
{
	struct MyNumber n;
};

int main(void)
{
	struct NumTables tables[10];
	int jgn_i, jgn_j;

	for (jgn_i = 0; jgn_i < 10; jgn_i++)
	{
		tables[jgn_i].n.jgn_num = (jgn_i + 1);
	}

	for (jgn_i = 0; jgn_i < 10; jgn_i++)
	{
		printf("\n\n");
		printf("Table of %d: \n\n", tables[jgn_i].n.jgn_num);
		for (jgn_j = 0; jgn_j < 10; jgn_j++)
		{
			tables[jgn_i].n.jgn_num_table[jgn_j] = tables[jgn_i].n.jgn_num * (jgn_j + 1);
			printf("%d * %d = %d\n", tables[jgn_i].n.jgn_num, (jgn_j + 1), tables[jgn_i].n.jgn_num_table[jgn_j]);
		}
	}

	return(0);
}