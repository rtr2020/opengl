#include<stdio.h>

int main(void)
{
	struct MyPoint
	{
		int x;
		int y;
	};
	struct Rectangle
	{
		struct MyPoint point_1;
		struct MyPoint point_2;
	};

	struct Rectangle rect = { {2, 3}, {5, 7} };

	int jgn_length, jgn_breadth, jgn_area;

	jgn_length = rect.point_2.y - rect.point_1.y;
	if (jgn_length < 0)
		jgn_length = jgn_length * -1;
	jgn_breadth = rect.point_2.x - rect.point_1.x;
	if (jgn_breadth < 0)
		jgn_breadth = jgn_breadth * -1;

	jgn_area = jgn_length * jgn_breadth;

	printf("\n\n");
	printf("Length of Rectangle = %d\n\n", jgn_length);
	printf("Breadth of Rectangle = %d\n\n", jgn_breadth);
	printf("Area of Rectangle = %d\n\n", jgn_area);

	return(0);
}