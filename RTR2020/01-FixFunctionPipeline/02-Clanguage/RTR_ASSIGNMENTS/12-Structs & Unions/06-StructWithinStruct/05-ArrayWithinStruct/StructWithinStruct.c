#include<stdio.h>

struct MyNumber
{
	int jgn_num;
	int jgn_num_table[10];
};

struct NumTables
{
	struct MyNumber	a;
	struct MyNumber	b;
	struct MyNumber	c;
};

int main(void)
{
	struct NumTables tables;
	int jgn_i;

	tables.a.jgn_num = 2;
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		tables.a.jgn_num_table[jgn_i] = tables.a.jgn_num * (jgn_i + 1);
	printf("\n\n");	
	printf("Table of %d : \n\n", tables.a.jgn_num);
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("%d * %d =%d\n", tables.a.jgn_num, (jgn_i + 1), tables.a.jgn_num_table[jgn_i]);

	tables.b.jgn_num = 3;
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		tables.b.jgn_num_table[jgn_i] = tables.b.jgn_num * (jgn_i + 1);
	printf("\n\n");
	printf("Table of %d : \n\n", tables.b.jgn_num);
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("%d * %d =%d\n", tables.b.jgn_num, (jgn_i + 1), tables.b.jgn_num_table[jgn_i]);

	tables.c.jgn_num = 4;
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		tables.c.jgn_num_table[jgn_i] = tables.c.jgn_num * (jgn_i + 1);
	printf("\n\n");
	printf("Table of %d : \n\n", tables.c.jgn_num);
	for (jgn_i = 0; jgn_i < 10; jgn_i++)
		printf("%d * %d =%d\n", tables.c.jgn_num, (jgn_i + 1), tables.c.jgn_num_table[jgn_i]);
	
	return(0);
}