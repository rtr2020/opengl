#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

int main(void)
{
	struct MyData data;
	data.jgn_i = 39;
	data.jgn_f = 39.5f;
	data.jgn_d = 332545.8786;
	data.jgn_c = 'J';
	
	printf("\n\n");
	printf("Data of sruct MyData are:\n\n");
	printf("i = %d\n", data.jgn_i);
	printf("f = %f\n", data.jgn_f);
	printf("d = %lf\n", data.jgn_d);
	printf("c = %c\n", data.jgn_c);

	printf("\n\n");
	printf("Address of sruct MyData are:\n\n");
	printf("i Address = %p\n", &data.jgn_i);
	printf("f Address = %p\n", &data.jgn_f);
	printf("d Address = %p\n", &data.jgn_d);
	printf("c Address = %p\n", &data.jgn_c);

	printf("Starting address of MyData is =%p\n\n", &data);

	return(0);
}