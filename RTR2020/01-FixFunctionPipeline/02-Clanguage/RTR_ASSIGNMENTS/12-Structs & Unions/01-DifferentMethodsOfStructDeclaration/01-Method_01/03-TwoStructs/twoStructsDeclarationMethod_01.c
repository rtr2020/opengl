#include<stdio.h>
#include<string.h>
struct	MyPoint
{
	int jgn_x;
	int jgn_y;
}point;


struct MyPointProperties
{
	int jgn_quadrant;
	char jgn_axisLocation[10];
}point_properties;

int main(void)
{
	printf("\n\n");
	printf("Enter X cordinate for A point: ");
	scanf("%d", &point.jgn_x);

	printf("Enter Y cordinate for A point: ");
        scanf("%d", &point.jgn_y);

	printf("\n\n");
	printf("Point cordinates (x, y) are : (%d, %d) !!!\n\n", point.jgn_x, point.jgn_y);

	if (point.jgn_x == 0 && point.jgn_y == 0)
		printf("The point is orogon (%d, %d)\n\n", point.jgn_x, point.jgn_y);
	else
	{
		if(point.jgn_x == 0)
		{
			if(point.jgn_y < 0)
				strcpy(point_properties.jgn_axisLocation, "Negative Y");

			if(point.jgn_y > 0)
                                strcpy(point_properties.jgn_axisLocation, "Positive Y");

			point_properties.jgn_quadrant = 0;

			printf("The point lies on the %s axis !!\n\n", point_properties.jgn_axisLocation);

		}

		else if(point.jgn_y == 0)
		{
			if(point.jgn_x <0)
				strcpy(point_properties.jgn_axisLocation, "Negative X");

			if(point.jgn_x > 0)
                                strcpy(point_properties.jgn_axisLocation, "Positive X");

			point_properties.jgn_quadrant = 0;

                        printf("The point lies on the %s axis !!\n\n", point_properties.jgn_axisLocation);


		}

		else
		{
			point_properties.jgn_axisLocation[0] = '\0';

			if(point.jgn_x > 0 && point.jgn_y > 0)
				point_properties.jgn_quadrant = 1;

			else if(point.jgn_x < 0 && point.jgn_y > 0)
                                point_properties.jgn_quadrant = 2;
			
			else if(point.jgn_x < 0 && point.jgn_y < 0)
                                point_properties.jgn_quadrant = 3;
			
			else
                                point_properties.jgn_quadrant = 3;

			printf("The point lies in quadrant number %d !!!\n\n", point_properties.jgn_quadrant);
		}
	}

	return(0);
}


