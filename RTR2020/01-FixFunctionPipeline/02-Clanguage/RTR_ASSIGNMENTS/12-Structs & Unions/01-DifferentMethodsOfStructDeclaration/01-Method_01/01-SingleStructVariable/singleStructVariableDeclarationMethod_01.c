#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
}data;

int main(void)
{
	int jgn_iSize;
	int jgn_fSize;
	int jgn_dSize;
	int jgn_struct_MyData_size;

	data.jgn_i = 20;
	data.jgn_f = 8.9f;
	data.jgn_d = 5.7890;
	data.jgn_c = 'a';

	printf("\n\n");
	printf("Data members of struct are:\n\n");
	printf("i = %d\n\n",data.jgn_i);
	printf("f = %f\n\n",data.jgn_f);
	printf("d = %lf\n\n",data.jgn_d);
	printf("c = %c\n\n",data.jgn_c);

	jgn_iSize = sizeof(data.jgn_i);
	jgn_fSize = sizeof(data.jgn_f);
	jgn_dSize = sizeof(data.jgn_d);

	printf("\n\n");
	printf("\n\nSize of data members:");
	printf("Size of i = %d\n\n", jgn_iSize);
	printf("Size of f = %d\n\n", jgn_fSize);
	printf("Size of d = %d\n\n", jgn_dSize);

	jgn_struct_MyData_size = sizeof(struct MyData);

	printf("\n\n");
	printf("Size of entire structure = %d", jgn_struct_MyData_size);

	return(0);

}
