#include<stdio.h>

struct MyPoint
{
	int jgn_x;
	int jgn_y;
} point_A, point_B, point_C, point_D, point_E;

int main(void)
{
	point_A.jgn_x = 3;
	point_A.jgn_y = 0;

        point_B.jgn_x = 4;
        point_B.jgn_y = 5;

        point_C.jgn_x = 3;
        point_C.jgn_y = 7;

	point_D.jgn_x = 3;
        point_D.jgn_y = 7;

	point_E.jgn_x = 8;
        point_E.jgn_y = 3;

	printf("\n\n");

	printf("Co-ordinates (x,y) of A = %d, %d\n\n", point_A.jgn_x, point_A.jgn_y);
	printf("Co-ordinates (x,y) of B = %d, %d\n\n", point_A.jgn_x, point_B.jgn_y);
	printf("Co-ordinates (x,y) of C = %d, %d\n\n", point_A.jgn_x, point_C.jgn_y);
	printf("Co-ordinates (x,y) of D = %d, %d\n\n", point_A.jgn_x, point_D.jgn_y);
	printf("Co-ordinates (x,y) of E = %d, %d\n\n", point_A.jgn_x, point_E.jgn_y);

	printf("\n\n");

	return(0);
}
