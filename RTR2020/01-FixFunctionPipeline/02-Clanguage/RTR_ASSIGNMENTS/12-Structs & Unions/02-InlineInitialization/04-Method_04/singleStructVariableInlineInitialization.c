#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

struct MyData data = {30, 5.6f, 23.5678, 'J'};

int main(void)
{
	struct MyData data1 = { 35, 3.9f, 1.23765, 'A'};

	struct MyData data2 = { 'P', 6.9f, 6.23765, 68};

	struct MyData data3 = { 38, 'A'};

	struct MyData data4 = { 37 };

	printf("\n\n");
	printf("Data members of data1 are:\n\n");
	printf("i = %d\n\n",data1.jgn_i);
	printf("f = %f\n\n",data1.jgn_f);
	printf("d = %lf\n\n",data1.jgn_d);
	printf("c = %c\n\n",data1.jgn_c);

	printf("\n\n");
        printf("Data members of data1 are:\n\n");
        printf("i = %d\n\n",data2.jgn_i);
        printf("f = %f\n\n",data2.jgn_f);
        printf("d = %lf\n\n",data2.jgn_d);
        printf("c = %c\n\n",data2.jgn_c);

	printf("\n\n");
        printf("Data members of data1 are:\n\n");
        printf("i = %d\n\n",data3.jgn_i);
        printf("f = %f\n\n",data3.jgn_f);
        printf("d = %lf\n\n",data3.jgn_d);
        printf("c = %c\n\n",data3.jgn_c);

	printf("\n\n");
        printf("Data members of data1 are:\n\n");
        printf("i = %d\n\n",data4.jgn_i);
        printf("f = %f\n\n",data4.jgn_f);
        printf("d = %lf\n\n",data4.jgn_d);
        printf("c = %c\n\n",data4.jgn_c);

	printf("\n\n");

	return(0);

}
