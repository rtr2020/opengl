#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
}data = {30, 5.6f, 23.5678, 'J'};

int main(void)
{

	printf("\n\n");
	printf("Data members of struct are:\n\n");
	printf("i = %d\n\n",data.jgn_i);
	printf("f = %f\n\n",data.jgn_f);
	printf("d = %lf\n\n",data.jgn_d);
	printf("c = %c\n\n",data.jgn_c);

	printf("\n\n");

	return(0);

}
