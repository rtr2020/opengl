#include<stdio.h>

union MyUnion
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

int main(void)
{
	union MyUnion u1, u2;
	printf("\n\n");
	printf("Members of union u1 are: \n\n");

	u1.jgn_i = 6;
	u1.jgn_f = 6.6f;
	u1.jgn_d = 6.57867;
	u1.jgn_c = 'C';

	printf("u1.jgn_i = %d\n", u1.jgn_i);
	printf("u1.jgn_f = %f\n", u1.jgn_f);
	printf("u1.jgn_d = %lf\n", u1.jgn_d);
	printf("u1.jgn_c = %c\n", u1.jgn_c);

	printf("Address of members of u1: \n\n");

	printf("u1.jgn_i = %p\n", &u1.jgn_i);
	printf("u1.jgn_f = %p\n", &u1.jgn_f);
	printf("u1.jgn_d = %p\n", &u1.jgn_d);
	printf("u1.jgn_c = %p\n", &u1.jgn_c);

	printf("MyUninon u1 = %p\n\n", &u1);

	printf("Members of union u2 are: \n\n");
	u2.jgn_i = 8;
	printf("u2.jgn_i = %d\n", u2.jgn_i);
	u2.jgn_f = 7.9f;
	printf("u2.jgn_f = %f\n", u2.jgn_f);
	u2.jgn_d = 4.56867;
	printf("u2.jgn_d = %lf\n", u2.jgn_d);
	u2.jgn_c = 'J';
	printf("u2.jgn_c = %c\n", u2.jgn_c);

	printf("Address of members of u2: \n\n");

	printf("u2.jgn_i = %p\n", &u2.jgn_i);
	printf("u2.jgn_f = %p\n", &u2.jgn_f);
	printf("u2.jgn_d = %p\n", &u2.jgn_d);
	printf("u2.jgn_c = %p\n", &u2.jgn_c);

	printf("MyUninon u2 = %p\n\n", &u2);

	return(0);
}