#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

union MyUnion
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

int main(void)
{
	union MyUnion u2;
	struct MyData data;

	data.jgn_i = 39;
	data.jgn_f = 39.5f;
	data.jgn_d = 332545.8786;
	data.jgn_c = 'J';

	printf("\n\n");
	printf("Data of sruct MyData are:\n\n");
	printf("i = %d\n", data.jgn_i);
	printf("f = %f\n", data.jgn_f);
	printf("d = %lf\n", data.jgn_d);
	printf("c = %c\n", data.jgn_c);

	printf("\n\n");
	printf("Address of sruct MyData are:\n\n");
	printf("i Address = %p\n", &data.jgn_i);
	printf("f Address = %p\n", &data.jgn_f);
	printf("d Address = %p\n", &data.jgn_d);
	printf("c Address = %p\n", &data.jgn_c);

	printf("Starting address of MyData is =%p\n\n", &data);

	printf("Members of union u2 are: \n\n");
	u2.jgn_i = 8;
	printf("u2.jgn_i = %d\n", u2.jgn_i);
	u2.jgn_f = 7.9f;
	printf("u2.jgn_f = %f\n", u2.jgn_f);
	u2.jgn_d = 4.56867;
	printf("u2.jgn_d = %lf\n", u2.jgn_d);
	u2.jgn_c = 'J';
	printf("u2.jgn_c = %c\n", u2.jgn_c);

	printf("Address of members of u2: \n\n");

	printf("u2.jgn_i = %p\n", &u2.jgn_i);
	printf("u2.jgn_f = %p\n", &u2.jgn_f);
	printf("u2.jgn_d = %p\n", &u2.jgn_d);
	printf("u2.jgn_c = %p\n", &u2.jgn_c);

	printf("MyUninon u2 = %p\n\n", &u2);

	return(0);
}