#include<stdio.h>

struct MyStruct
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

union MyUnion
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

int main(void)
{
	struct MyStruct s;
	union MyUnion u;

	printf("\n\n");
	printf("Size of MyStruct = %lu\n", sizeof(s));
	printf("\n\n");
	printf("Size of MyUninon = %lu\n", sizeof(u));

	return(0);
}
