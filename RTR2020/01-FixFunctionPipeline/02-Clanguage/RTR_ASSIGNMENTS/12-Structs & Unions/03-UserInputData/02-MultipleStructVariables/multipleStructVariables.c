#include<stdio.h>

struct MyPoint
{
	int jgn_x;
	int jgn_y;
};

int main(void)
{
	struct MyPoint point_A, point_B, point_C, point_D, point_E;

	printf("\n\n");
	printf("Enter X-coordinate for point A : ");
	scanf("%d", &point_A.jgn_x);
        printf("Enter Y-coordinate for point A : ");
        scanf("%d", &point_A.jgn_y);

	printf("\n\n");
        printf("Enter X-coordinate for point B : ");
        scanf("%d", &point_B.jgn_x);
        printf("Enter Y-coordinate for point B : ");
        scanf("%d", &point_B.jgn_y);

	printf("\n\n");
        printf("Enter X-coordinate for point C : ");
        scanf("%d", &point_C.jgn_x);
        printf("Enter Y-coordinate for point C : ");
        scanf("%d", &point_C.jgn_y);

	printf("\n\n");
        printf("Enter X-coordinate for point D : ");
        scanf("%d", &point_D.jgn_x);
        printf("Enter Y-coordinate for point D : ");
        scanf("%d", &point_D.jgn_y);

	printf("\n\n");
        printf("Enter X-coordinate for point E : ");
        scanf("%d", &point_E.jgn_x);
        printf("Enter Y-coordinate for point E : ");
        scanf("%d", &point_E.jgn_y);


	printf("\n\n");

	printf("Co-ordinates (x,y) of A = %d, %d\n\n", point_A.jgn_x, point_A.jgn_y);
	printf("Co-ordinates (x,y) of B = %d, %d\n\n", point_B.jgn_x, point_B.jgn_y);
	printf("Co-ordinates (x,y) of C = %d, %d\n\n", point_C.jgn_x, point_C.jgn_y);
	printf("Co-ordinates (x,y) of D = %d, %d\n\n", point_D.jgn_x, point_D.jgn_y);
	printf("Co-ordinates (x,y) of E = %d, %d\n\n", point_E.jgn_x, point_E.jgn_y);

	printf("\n\n");

	return(0);
}
