#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_ch;
};

int main(void)
{	
	struct MyData data;

	printf("\n\n");

	printf("Enter the Integer value for data member 'i' of 'struct MyData': \n");
	scanf("%d", &data.jgn_i);

	printf("Enter the Integer value for data member 'f' of 'struct MyData': \n");
        scanf("%f", &data.jgn_f);

	printf("Enter the Integer value for data member 'd' of 'struct MyData': \n");
        scanf("%lf", &data.jgn_d);

	printf("Enter the Integer value for data member 'ch' of 'struct MyData': \n");
        scanf(" %c", &data.jgn_ch);

	printf("\n\n");
	printf("Data members of struct are:\n\n");
	printf("i = %d\n\n",data.jgn_i);
	printf("f = %f\n\n",data.jgn_f);
	printf("d = %lf\n\n",data.jgn_d);
	printf("c = %c\n\n",data.jgn_ch);

	printf("\n\n");

	return(0);
}
