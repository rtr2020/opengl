#include<stdio.h>

struct MyData
{
	int jgn_i;
	float jgn_f;
	double jgn_d;
	char jgn_c;
};

int main(void)
{
	struct MyData AddStructMembers(struct MyData, struct MyData, struct MyData);
	struct MyData data1, data2, data3, answer_data;

	printf("\n\n"); 
	printf("**** Data 1 ****\n\n"); 
	printf("Enter value for i: "); 
	scanf("%d", &data1.jgn_i);

	printf("\n\n");
	printf("Enter value for f: ");
	scanf("%f", &data1.jgn_f);

	printf("\n\n");
	printf("Enter value for d: ");
	scanf("%lf", &data1.jgn_d);

	printf("\n\n");
	printf("Enter value for c: ");
	data1.jgn_c = getch();

	printf("\n\n");
	printf("**** Data 2 ****\n\n");
	printf("Enter value for i: ");
	scanf("%d", &data2.jgn_i);

	printf("\n\n");
	printf("Enter value for f: ");
	scanf("%f", &data2.jgn_f);

	printf("\n\n");
	printf("Enter value for d: ");
	scanf("%lf", &data2.jgn_d);

	printf("\n\n");
	printf("Enter value for c: ");
	data2.jgn_c = getch();

	printf("\n\n");
	printf("**** Data 3 ****\n\n");
	printf("Enter value for i: ");
	scanf("%d", &data3.jgn_i);

	printf("\n\n");
	printf("Enter value for f: ");
	scanf("%f", &data3.jgn_f);

	printf("\n\n");
	printf("Enter value for d: ");
	scanf("%lf", &data3.jgn_d);

	printf("\n\n");
	printf("Enter value for c: ");
	data3.jgn_c = getch();

	answer_data = AddStructMembers(data1, data2, data3);

	printf("\n\n");
	printf("*** Answer ***\n\n");
	printf("answer_data.i = %d\n", answer_data.jgn_i);
	printf("answer_data.f = %f\n", answer_data.jgn_f);
	printf("answer_data.d = %lf\n", answer_data.jgn_d);
	printf("answer_data.c = %c\n", answer_data.jgn_c);

	answer_data.jgn_c = data1.jgn_c;
	printf("answer_data.c (from data1) = %c\n", answer_data.jgn_c);

	answer_data.jgn_c = data2.jgn_c;
	printf("answer_data.c (from data2) = %c\n", answer_data.jgn_c);

	answer_data.jgn_c = data3.jgn_c;
	printf("answer_data.c (from data3) = %c\n", answer_data.jgn_c);

	return(0);

}


struct MyData AddStructMembers(struct MyData md_one, struct MyData md_two, struct MyData md_three)
{
	struct MyData answer;
	answer.jgn_i = md_one.jgn_i + md_two.jgn_i + md_three.jgn_i;
	answer.jgn_f = md_one.jgn_f + md_two.jgn_f + md_three.jgn_f;
	answer.jgn_d = md_one.jgn_d + md_two.jgn_d + md_three.jgn_d;
	return(answer); 
}