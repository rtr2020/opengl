#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main(void)
{
	int jgn_iArray[NUM_ROWS][NUM_COLUMNS][DEPTH] = { { {4, 7}, {4, 6}, {7, 0} },
				    { {3, 5}, {1, 6}, {5, 0} },
				    { {2, 3}, {6, 6}, {8, 7} },
				    { {4, 9}, {4, 6}, {7, 0} },
				    { {8, 71}, {45, 67}, {2, 4} } };
	int jgn_i, jgn_j, jgn_k;

	int jgn_iArray_1D[NUM_ROWS * NUM_COLUMNS * DEPTH];



	printf("\n\n");

	printf("Elements in integer 3D array: \n\n");

	for(jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		printf("*** ROW %d ***\n", (jgn_i + 1));

		for (jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
		{
			printf("*** COLUMN %d ***\n", (jgn_j + 1));
			for (jgn_k = 0; jgn_k < DEPTH; jgn_k++)
			{
				printf("jgn_iArray[%d][%d][%d] = %d\n", jgn_i, jgn_j, jgn_k, jgn_iArray[jgn_i][jgn_j][jgn_k]);
			}
		printf("\n");
		}
	printf("\n");
	}

	for(jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		for (jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
		{
			for(jgn_k = 0; jgn_k < DEPTH; jgn_k++)
			{
				jgn_iArray_1D[(jgn_i * NUM_COLUMNS * DEPTH) + (jgn_j * DEPTH) + jgn_k] = jgn_iArray[jgn_i][jgn_j][jgn_k];
			}
		}
	}

	printf("\n\n\n\n");
	printf("Elements in 1D array:\n\n");
	for(jgn_i = 0; jgn_i < (NUM_ROWS * NUM_COLUMNS * DEPTH); jgn_i++)
	{
		printf("jgn_iArray_1D[%d] = %d\n", jgn_i, jgn_iArray_1D[jgn_i]);
	}

	return(0);
}
