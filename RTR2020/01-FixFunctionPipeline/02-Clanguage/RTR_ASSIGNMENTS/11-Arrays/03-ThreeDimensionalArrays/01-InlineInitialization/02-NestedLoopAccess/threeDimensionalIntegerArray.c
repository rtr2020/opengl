#include<stdio.h>

int main(void)
{
	int jgn_iArray[5][3][2] = { { {4, 7}, {4, 6}, {7, 0} },
				    { {3, 5}, {1, 6}, {5, 0} },
				    { {2, 3}, {6, 6}, {8, 7} },
				    { {4, 9}, {4, 6}, {7, 0} },
				    { {8, 71}, {45, 67}, {2, 4} } };

	int jgn_int_size;
	int jgn_iArray_size;
	int jgn_iArray_num_elements, jgn_iArray_width, jgn_iArray_height, jgn_iArray_depth;
	int jgn_i, jgn_j, jgn_k;

	printf("\n\n");

	jgn_int_size = sizeof(int);

	jgn_iArray_size = sizeof(jgn_iArray);

	printf("Size of 3D array is : = %d\n\n", jgn_iArray_size);

	jgn_iArray_width = jgn_iArray_size / sizeof(jgn_iArray[0]);

	printf("Number of Rows in 3D array is : = %d", jgn_iArray_width);

	jgn_iArray_height = sizeof(jgn_iArray[0]) / sizeof(jgn_iArray[0][0]);

	printf("Number of Columns in 3D array is : = %d", jgn_iArray_height);

	jgn_iArray_depth = sizeof(jgn_iArray[0][0]) / jgn_int_size;

	printf("Depth of 3D array is : = %d", jgn_iArray_depth);

	jgn_iArray_num_elements = jgn_iArray_width * jgn_iArray_height * jgn_iArray_depth;

	printf("Number of elements in 3D array: = %d", jgn_iArray_num_elements);

	printf("\n\n");

	printf("Elements in integer 3D array: \n\n");

	for (jgn_i = 0; jgn_i < jgn_iArray_width; jgn_i++)
	{
		printf("*** ROW %d ***\n", (jgn_i + 1));

		for (jgn_j = 0; jgn_j < jgn_iArray_height; jgn_j++)
		{
			printf("*** COLUMN %d ***\n", (jgn_j + 1));
			for (jgn_k = 0; jgn_k < jgn_iArray_depth; jgn_k++)
			{
				printf("jgn_iArray[%d][%d][%d] = %d\n", jgn_i, jgn_j, jgn_k, jgn_iArray[jgn_i][jgn_j][jgn_k]);
			}
		printf("\n");
		}
	printf("\n");
	}

	return(0);

}
