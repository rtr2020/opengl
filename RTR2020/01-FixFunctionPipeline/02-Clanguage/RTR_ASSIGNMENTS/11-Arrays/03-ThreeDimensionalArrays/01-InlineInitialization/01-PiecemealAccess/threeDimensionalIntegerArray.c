#include<stdio.h>

int main(void)
{
	int jgn_iArray[5][3][2] = { { {4, 7}, {4, 6}, {7, 0} },
				    { {3, 5}, {1, 6}, {5, 0} },
				    { {2, 3}, {6, 6}, {8, 7} },
				    { {4, 9}, {4, 6}, {7, 0} },
				    { {8, 71}, {45, 67}, {2, 4} } };

	int jgn_int_size;
	int jgn_iArray_size;
	int jgn_iArray_num_elements, jgn_iArray_width, jgn_iArray_height, jgn_iArray_depth;

	printf("\n\n");

	jgn_int_size = sizeof(int);

	jgn_iArray_size = sizeof(jgn_iArray);

	printf("Size of 3D array is : = %d\n\n", jgn_iArray_size);

	jgn_iArray_width = jgn_iArray_size / sizeof(jgn_iArray[0]);

	printf("Number of Rows in 3D array is : = %d", jgn_iArray_width);

	jgn_iArray_height = sizeof(jgn_iArray[0]) / sizeof(jgn_iArray[0][0]);

	printf("Number of Columns in 3D array is : = %d", jgn_iArray_height);

	jgn_iArray_depth = sizeof(jgn_iArray[0][0]) / jgn_int_size;

	printf("Depth of 3D array is : = %d", jgn_iArray_depth);

	jgn_iArray_num_elements = jgn_iArray_width * jgn_iArray_height * jgn_iArray_depth;

	printf("Number of elements in 3D array: = %d", jgn_iArray_num_elements);

	printf("\n\n");

	printf("Elements in integer 3D array: \n\n");

	printf("*** ROW 1 ***\n");
	printf("*** COLUMN 1 ***\n");

	printf("jgn_iArray[0][0][0] = %d\n", jgn_iArray[0][0][0]);
	printf("jgn_iArray[0][0][1] = %d\n", jgn_iArray[0][0][1]);
	printf("\n");

	printf("*** COLUMN 2 ***\n");
        printf("jgn_iArray[0][1][0] = %d\n", jgn_iArray[0][1][0]);
        printf("jgn_iArray[0][1][1] = %d\n", jgn_iArray[0][1][1]);
        printf("\n");

        printf("*** COLUMN 3 ***\n");
        printf("jgn_iArray[0][2][0] = %d\n", jgn_iArray[0][2][0]);
        printf("jgn_iArray[0][2][1] = %d\n", jgn_iArray[0][2][1]);
        printf("\n");

	printf("*** ROW 2 ***\n");
        printf("*** COLUMN 1 ***\n");
        printf("jgn_iArray[1][0][0] = %d\n", jgn_iArray[1][0][0]);
        printf("jgn_iArray[1][0][1] = %d\n", jgn_iArray[1][0][1]);
        printf("\n");

	printf("*** COLUMN 2 ***\n");
        printf("jgn_iArray[1][1][0] = %d\n", jgn_iArray[1][1][0]);
        printf("jgn_iArray[1][1][1] = %d\n", jgn_iArray[1][1][1]);
        printf("\n");

	printf("*** COLUMN 3 ***\n");
        printf("jgn_iArray[1][2][0] = %d\n", jgn_iArray[1][2][0]);
        printf("jgn_iArray[1][2][1] = %d\n", jgn_iArray[1][2][1]);
        printf("\n");


        printf("*** ROW 3 ***\n");
        printf("*** COLUMN 1 ***\n");
        printf("jgn_iArray[2][0][0] = %d\n", jgn_iArray[2][0][0]);
        printf("jgn_iArray[2][0][1] = %d\n", jgn_iArray[2][0][1]);
        printf("\n");

        printf("*** COLUMN 2 ***\n");
        printf("jgn_iArray[2][1][0] = %d\n", jgn_iArray[2][1][0]);
        printf("jgn_iArray[2][1][1] = %d\n", jgn_iArray[2][1][1]);
        printf("\n");

        printf("*** COLUMN 3 ***\n");
        printf("jgn_iArray[2][2][0] = %d\n", jgn_iArray[2][2][0]);
        printf("jgn_iArray[2][2][1] = %d\n", jgn_iArray[2][2][1]);
        printf("\n");
	
        printf("*** ROW 4 ***\n");
        printf("*** COLUMN 1 ***\n");
        printf("jgn_iArray[3][0][0] = %d\n", jgn_iArray[3][0][0]);
        printf("jgn_iArray[3][0][1] = %d\n", jgn_iArray[3][0][1]);
        printf("\n");

        printf("*** COLUMN 2 ***\n");
        printf("jgn_iArray[3][1][0] = %d\n", jgn_iArray[3][1][0]);
        printf("jgn_iArray[3][1][1] = %d\n", jgn_iArray[3][1][1]);
        printf("\n");

        printf("*** COLUMN 3 ***\n");
        printf("jgn_iArray[3][2][0] = %d\n", jgn_iArray[3][2][0]);
        printf("jgn_iArray[3][2][1] = %d\n", jgn_iArray[3][2][1]);
        printf("\n");

					
        printf("*** ROW 5 ***\n");
        printf("*** COLUMN 1 ***\n");
        printf("jgn_iArray[4][0][0] = %d\n", jgn_iArray[4][0][0]);
        printf("jgn_iArray[4][0][1] = %d\n", jgn_iArray[4][0][1]);
        printf("\n");

        printf("*** COLUMN 2 ***\n");
        printf("jgn_iArray[4][1][0] = %d\n", jgn_iArray[4][1][0]);
        printf("jgn_iArray[4][1][1] = %d\n", jgn_iArray[4][1][1]);
        printf("\n");

        printf("*** COLUMN 3 ***\n");
        printf("jgn_iArray[4][2][0] = %d\n", jgn_iArray[4][2][0]);
        printf("jgn_iArray[4][2][1] = %d\n", jgn_iArray[4][2][1]);
        printf("\n");

	return(0);

}
