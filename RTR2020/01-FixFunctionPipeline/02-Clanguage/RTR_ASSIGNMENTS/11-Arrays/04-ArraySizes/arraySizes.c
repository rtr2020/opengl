#include<stdio.h>

int main(void)
{
	int jgn_iArrayOne[5];
	int jgn_iArrayTwo[5][3];
	int jgn_iArrayThree[100][100][5];

	int numOfRows2D;
	int numOfColumns2D;

	int numOfRows3D;
	int numOfColumns3D;
	int depth3D;

	//code
	printf("\n\n");
	printf("Size of 1-D array = %lu\n\n", sizeof(jgn_iArrayOne));
	printf("Number of elements in 1-D array = %lu\n\n", (sizeof(jgn_iArrayOne) / sizeof(int)));

        printf("\n\n");
        printf("Size of 2-D array = %lu\n\n", sizeof(jgn_iArrayTwo));
	printf("Number of rows in 2-D array = %lu\n\n", (sizeof(jgn_iArrayTwo) / sizeof(jgn_iArrayTwo[0])));
	numOfRows2D = (sizeof(jgn_iArrayTwo) / sizeof(jgn_iArrayTwo[0]));
        printf("Number of columns in 2-D array = %lu\n\n", (sizeof(jgn_iArrayTwo[0]) / sizeof(jgn_iArrayTwo[0][0])));
        numOfColumns2D = (sizeof(jgn_iArrayTwo[0]) / sizeof(jgn_iArrayTwo[0][0]));
	printf("Total number of elements in 2-D array:= %d\n\n", (numOfRows2D * numOfColumns2D));
	printf("\n\n");


	printf("\n\n");
        printf("Size of 3-D array = %lu\n\n", sizeof(jgn_iArrayThree));
	printf("Number of rows in 3-D array = %lu\n\n", (sizeof(jgn_iArrayThree) / sizeof(jgn_iArrayThree[0])));
	numOfRows3D = (sizeof(jgn_iArrayThree) / sizeof(jgn_iArrayThree[0]));
        printf("Number of columns in 3-D array = %lu\n\n", (sizeof(jgn_iArrayThree[0]) / sizeof(jgn_iArrayThree[0][0])));
	numOfColumns3D = (sizeof(jgn_iArrayThree[0]) / sizeof(jgn_iArrayThree[0][0]));
	printf("Number elements(depth) in one column = %lu\n\n", (sizeof(jgn_iArrayThree[0][0]) / sizeof(jgn_iArrayThree[0][0][0])));
	depth3D = (sizeof(jgn_iArrayThree[0][0]) / sizeof(jgn_iArrayThree[0][0][0]));
	printf("Total number of elements in 3D array:= %d\n\n", (numOfRows3D * numOfColumns3D * depth3D));
	return(0);
}
