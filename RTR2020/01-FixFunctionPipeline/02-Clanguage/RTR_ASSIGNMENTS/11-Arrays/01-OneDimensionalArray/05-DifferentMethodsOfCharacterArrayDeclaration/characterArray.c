#include<stdio.h>

int main(void)
{
	char jgn_chArray_01[] = {'J', 'I', 'T', 'H', 'I', 'N', '\0'};
	char jgn_chArray_02[] = {'W', 'E', 'L', 'C', 'O', 'M', 'E', '\0'};
	char jgn_chArray_03[] = {'Y', 'O', 'U', '\0'};
	char jgn_chArray_04[] = "TO";
	char jgn_chArray_05[] = "Real Time Randering";
	
	char jgn_chArrayWithoutNullTerminator[] = {'H', 'E', 'L', 'L', 'O'};

	printf("\n\n");

	printf("Size of chArray_01 : %lu\n\n", sizeof(jgn_chArray_01));
        printf("Size of chArray_02 : %lu\n\n", sizeof(jgn_chArray_02));
        printf("Size of chArray_03 : %lu\n\n", sizeof(jgn_chArray_03));
        printf("Size of chArray_04 : %lu\n\n", sizeof(jgn_chArray_04));
	printf("Size of chArray_05 : %lu\n\n", sizeof(jgn_chArray_05));
	

	printf("\n\n");

	printf("The Strings are : \n\n");

	printf("chArray_01 : %s\n\n", jgn_chArray_01);
        printf("chArray_02 : %s\n\n", jgn_chArray_02);
        printf("chArray_03 : %s\n\n", jgn_chArray_03);
        printf("chArray_04 : %s\n\n", jgn_chArray_04);
        printf("chArray_05 : %s\n\n", jgn_chArray_05);

	printf("\n\n");
	printf("Size Of chArrayWithoutNullTerminator : %lu\n\n", sizeof(jgn_chArrayWithoutNullTerminator));

	printf("chArrayWithoutNullTerminator : %s\n\n", jgn_chArrayWithoutNullTerminator);

	return(0);

}
