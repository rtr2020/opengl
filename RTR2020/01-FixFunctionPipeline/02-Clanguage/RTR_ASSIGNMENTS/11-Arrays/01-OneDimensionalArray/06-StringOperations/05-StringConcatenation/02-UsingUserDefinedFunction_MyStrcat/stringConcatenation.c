#include<stdio.h>
#include<string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrcat(char[], char[]);

	char jgn_chArray_one[MAX_STRING_LENGTH], jgn_chArray_two[MAX_STRING_LENGTH];

	//code
	
	printf("\n\n");
	printf("Enter first string:");
	fgets(jgn_chArray_one, MAX_STRING_LENGTH, stdin);


        printf("\n\n");
        printf("Enter first string:");
        fgets(jgn_chArray_two, MAX_STRING_LENGTH, stdin);

	printf("\n\n");
	printf("**************Before Concatenation***********\n");
	printf("First String entered is: \n\n");
	printf("%s\n\n", jgn_chArray_one);

	printf("Second String entered is: \n\n");
        printf("%s\n\n", jgn_chArray_two);
	
	MyStrcat(jgn_chArray_one, jgn_chArray_two);

	printf("\n\n");
	printf("Concatenated string is = %s\n\n", jgn_chArray_one);
	return(0);
}


void MyStrcat(char str_destination[], char str_source[])
{
	int MyStrlen(char str[]);
	
	int jgn_iStringLengthSource = 0, jgn_iStringLengthDestination = 0;

	int jgn_i, jgn_j;

	jgn_iStringLengthSource = MyStrlen(str_source);

	jgn_iStringLengthDestination = MyStrlen(str_destination);

	for(jgn_i = jgn_iStringLengthDestination, jgn_j = 0; jgn_j < jgn_iStringLengthSource; jgn_i++, jgn_j++)
	{
		str_destination[jgn_i] = str_source[jgn_j];
	}

	str_destination[jgn_i] = '\0';
}

int MyStrlen(char str[])
{
	int jgn_j;
	int jgn_str_length = 0;

	for(jgn_j= 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if(str[jgn_j] == '\0')
			break;
		else

			jgn_str_length++;
	}
	return(jgn_str_length);
}
