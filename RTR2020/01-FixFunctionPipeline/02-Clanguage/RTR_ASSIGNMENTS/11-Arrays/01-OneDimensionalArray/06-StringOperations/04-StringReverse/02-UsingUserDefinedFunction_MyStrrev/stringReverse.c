#include<stdio.h>
#include<string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrrev(char[], char[]);

	char jgn_chArray[MAX_STRING_LENGTH], jgn_chArrayRev[MAX_STRING_LENGTH];

	//code
	
	printf("\n\n");
	printf("Enter the string:");
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);


	MyStrrev(jgn_chArrayRev, jgn_chArray);

	printf("\n\n");
	printf("Original String entered is: \n\n");
	printf("%s", jgn_chArray);
	

	printf("\n\n");
	printf("String reversed is = %s\n\n", jgn_chArrayRev);
	return(0);
}

void MyStrrev(char str_destination[], char str_source[])
{
	int MyStrlen(char[]);

	int jgniStringLength = 0;
	int jgn_i, jgn_j, jgn_len;

	jgniStringLength = MyStrlen(str_source);

	jgn_len = jgniStringLength - 1;

	for(jgn_i = 0, jgn_j = jgn_len; jgn_i < jgniStringLength, jgn_j >= 0; jgn_i++, jgn_j--)
	{
		str_destination[jgn_i] = str_source[jgn_j];
	}

	str_destination[jgn_i] = '\0';
}

int MyStrlen(char str[])
{
	int jgn_j;
	int jgn_str_length = 0;

	for(jgn_j= 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if(str[jgn_j] == '\0')
			break;
		else

			jgn_str_length++;
	}
	return(jgn_str_length);
}
