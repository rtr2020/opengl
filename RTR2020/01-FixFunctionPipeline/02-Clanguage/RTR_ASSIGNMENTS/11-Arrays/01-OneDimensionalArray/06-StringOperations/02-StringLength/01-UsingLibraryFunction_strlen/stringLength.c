#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	char jgn_chArray[MAX_STRING_LENGTH];
	int jgn_strLength = 0;

	//code
	
	printf("\n\n");
	printf("Enter the string:");
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);

	printf("\n\n");
	printf("String entered is: \n\n");
	printf("%s", jgn_chArray);
	
	printf("\n\n");
	jgn_strLength = strlen(jgn_chArray);
	printf("Length of String is = %d characters!!!\n\n", jgn_strLength);
	return(0);
}
