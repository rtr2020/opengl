#include<stdio.h>

#define MAX_STRING_LENGTH 512

int MyStrlen(char[]);

int main(void)
{
	char jgn_chArray[MAX_STRING_LENGTH];
	int jgn_strLength = 0;

	//code
	
	printf("\n\n");
	printf("Enter the string:");
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);

	printf("\n\n");
	printf("String entered is: \n\n");
	printf("%s", jgn_chArray);
	
	printf("\n\n");
	jgn_strLength = MyStrlen(jgn_chArray);
	printf("Length of String is = %d characters!!!\n\n", jgn_strLength);
	return(0);
}

int MyStrlen(char str[])
{
	int jgn_j;
	int jgn_str_length = 0;
	
	for(jgn_j= 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if(str[jgn_j] == '\0')
			break;
		else
			jgn_str_length++;
	}
	return(jgn_str_length);
}
