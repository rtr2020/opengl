#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int Mystrlen(char[]);
	void MyStrcpy(char[], char[]);
	
	char jgn_chArray[MAX_STRING_LENGTH], jgn_chArrayReplaced[MAX_STRING_LENGTH];

	int jgn_iStringLength;

	int jgn_cntA = 0, jgn_cntE = 0, jgn_cntI = 0, jgn_cntO = 0, jgn_cntU = 0;

	int jgn_i;

	printf("\n\n");

	printf("Enter a string:");
	
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);

	printf("\n\n");

	MyStrcpy(jgn_chArrayReplaced, jgn_chArray);

	jgn_iStringLength = Mystrlen(jgn_chArray);

	for(jgn_i = 0; jgn_i < jgn_iStringLength; jgn_i++)
	{
		switch(jgn_chArray[jgn_i])
		{
			case 'a':
			case 'A':
                        case 'e':
                        case 'E':
                        case 'i':
                        case 'I':
                        case 'o':
                        case 'O':
                        case 'u':
                        case 'U':
				jgn_chArrayReplaced[jgn_i] = '*';
				break;
			default:
				break;

		}
	}

	printf("\n\n");
	printf("Original String Entered is : %s\n\n", jgn_chArray);
	printf("String after replacement is : %s\n\n", jgn_chArrayReplaced);

	return(0);
}

void MyStrcpy(char str_destination[], char str_source[])
{
	int MyStrlen(char[]);
	int jgn_StringLength = 0;
	int jgn_j;

	jgn_StringLength = Mystrlen(str_source);
	for (jgn_j = 0; jgn_j < jgn_StringLength; jgn_j++)
		str_destination[jgn_j] = str_source[jgn_j];
	str_destination[jgn_j] = '\0';
}


int Mystrlen(char str[])
{
        int jgn_j;
        int jgn_str_length = 0;

        for(jgn_j= 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
        {
                if(str[jgn_j] == '\0')
                        break;
                else

                        jgn_str_length++;
        }
        return(jgn_str_length);
}
