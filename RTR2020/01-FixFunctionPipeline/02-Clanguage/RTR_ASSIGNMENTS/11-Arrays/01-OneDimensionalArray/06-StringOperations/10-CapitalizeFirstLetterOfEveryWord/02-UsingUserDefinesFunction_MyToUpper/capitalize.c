#include<stdio.h>

#define SPACE ' '

#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

#define MAX_STRING_LENGTH 512

int main(void)
{
	int Mystrlen(char[]);
	char MytoUpper(char);

	char jgn_chArray[MAX_STRING_LENGTH], jgn_chArray_capitalizeFirstLeter[MAX_STRING_LENGTH];

	int jgn_iStringLength;

	int jgn_i,jgn_j;

	printf("\n\n");

	printf("Enter a string:");
	
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);

	printf("\n\n");

	jgn_iStringLength = Mystrlen(jgn_chArray);

	jgn_j = 0;

	for(jgn_i = 0; jgn_i < jgn_iStringLength; jgn_i++)
	{
		if (jgn_i == 0)
			jgn_chArray_capitalizeFirstLeter[jgn_j] = MytoUpper(jgn_chArray[jgn_i]);
		else if(jgn_chArray[jgn_i] == SPACE)
		{
			jgn_chArray_capitalizeFirstLeter[jgn_j] = jgn_chArray[jgn_i];
                        jgn_chArray_capitalizeFirstLeter[jgn_j + 1] = MytoUpper(jgn_chArray[jgn_i + 1]);

			jgn_j++;
			jgn_i++;
		}
		else if((jgn_chArray[jgn_i] == FULLSTOP || jgn_chArray[jgn_i] == COMMA || jgn_chArray[jgn_i] == EXCLAMATION || jgn_chArray[jgn_i] == QUESTION_MARK) && (jgn_chArray[jgn_i] != SPACE))
		{
			jgn_chArray_capitalizeFirstLeter[jgn_j] = jgn_chArray[jgn_i];
			jgn_chArray_capitalizeFirstLeter[jgn_j + 1] = SPACE;
			jgn_chArray_capitalizeFirstLeter[jgn_j + 2] = MytoUpper(jgn_chArray[jgn_i + 1]);
			jgn_j = jgn_j + 2;
			jgn_i++;
		}

		else
			jgn_chArray_capitalizeFirstLeter[jgn_j] = jgn_chArray[jgn_i];

		jgn_j++;
	}

	jgn_chArray_capitalizeFirstLeter[jgn_j] = '\0';
	printf("\n\n");
	printf("String entered by you is: %s\n\n", jgn_chArray);
	printf("String after capitalization: %s\n\n", jgn_chArray_capitalizeFirstLeter);
	return(0);
}

int Mystrlen(char str[])
{
	int jgn_j;
	int jgn_str_length = 0;

	for(jgn_j= 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if(str[jgn_j] == '\0')
			break;
		else

			jgn_str_length++;
	}
	return(jgn_str_length);
}

char MytoUpper(char ch)
{
	int jgn_num;
	int jgn_c;

	jgn_num = 'a' - 'A';

	if((int)ch >= 97 && (int)ch <= 122)
	{
		jgn_c = (int)ch - jgn_num;
		return((char)jgn_c);
	}
	else
		return(ch);
}
