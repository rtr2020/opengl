#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{

	void MyStrcpy(char[], char[]);

	char jgn_chArray[MAX_STRING_LENGTH], jgn_chArrayCopy[MAX_STRING_LENGTH];

	//code
	
	printf("\n\n");
	printf("Enter the string:");
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);

	MyStrcpy(jgn_chArrayCopy, jgn_chArray);

	printf("\n\n");
	printf("Original String entered is: \n\n");
	printf("%s", jgn_chArray);
	
	printf("\n\n");
	printf("Copy String is = %s\n\n", jgn_chArrayCopy);
	return(0);
}


void MyStrcpy(char str_destination[], char str_source[])
{
	int MyStrlen(char[]);
	int jgn_StringLength = 0;
	int jgn_j;

	jgn_StringLength = MyStrlen(str_source);
	for (jgn_j = 0; jgn_j < jgn_StringLength; jgn_j++)
		str_destination[jgn_j] = str_source[jgn_j];
	str_destination[jgn_j] = '\0';
}

int MyStrlen(char str[])
{
	int jgn_j;
	int jgn_str_length = 0;
	
	for(jgn_j= 0; jgn_j < MAX_STRING_LENGTH; jgn_j++)
	{
		if(str[jgn_j] == '\0')
			break;
		else
			jgn_str_length++;
	}
	return(jgn_str_length);
}
