#include<stdio.h>
#include<string.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	char jgn_chArray[MAX_STRING_LENGTH], jgn_chArrayCopy[MAX_STRING_LENGTH];

	//code
	
	printf("\n\n");
	printf("Enter the string:");
	fgets(jgn_chArray, MAX_STRING_LENGTH, stdin);

	strcpy(jgn_chArrayCopy, jgn_chArray);

	printf("\n\n");
	printf("Original String entered is: \n\n");
	printf("%s", jgn_chArray);
	

	printf("\n\n");
	printf("Copied String is = %s\n\n", jgn_chArrayCopy);
	return(0);
}
