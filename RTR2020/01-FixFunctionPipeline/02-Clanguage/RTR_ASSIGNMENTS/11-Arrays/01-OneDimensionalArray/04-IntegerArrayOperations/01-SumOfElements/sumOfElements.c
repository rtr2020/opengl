#include<stdio.h>

#define JGN_NUM_ELEMENTS 10

int main(void)
{
	int jgn_iArray[JGN_NUM_ELEMENTS];
	int jgn_i, jgn_num, jgn_sum = 0;

	//code
	printf("\n\n");

	printf("Enter integer elements for an array:");

	for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
	{
		scanf("%d", &jgn_num);
		jgn_iArray[jgn_i] = jgn_num;
	}

        for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
	{
		jgn_sum =  jgn_sum + jgn_iArray[jgn_i];
	}

	printf("\n\n");

	printf("Sum of all elements of Array = %d", jgn_sum);

	printf("\n\n");

	return(0);
} 
