#include<stdio.h>

#define JGN_NUM_ELEMENTS 10

int main(void)
{
	int jgn_iArray[JGN_NUM_ELEMENTS];
	int jgn_i, jgn_j, jgn_num, jgn_count = 0;

	//code
	printf("\n\n");

	printf("Enter integer elements for an array:");

	for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
	{
		scanf("%d", &jgn_num);
		if (jgn_num < 0)
			jgn_num = -1 * jgn_num;
		jgn_iArray[jgn_i] = jgn_num;
	}

	printf("\n\n");
	printf("Array Elements Are :\n\n");
        for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
	{
		printf("%d\n", jgn_iArray[jgn_i]);
	}

	printf("\n\n");
        printf("Prime numbers amongst the array elements Are :\n\n");
        for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
        {
        	for(jgn_j = 1; jgn_j <= jgn_iArray[jgn_i]; jgn_j++)
        	{
			if((jgn_iArray[jgn_i] % jgn_j) == 0)
			{
				jgn_count++;
			}
        	}

		if(jgn_count == 2)
			printf("%d\n", jgn_iArray[jgn_i]);
		jgn_count = 0;
        }
	
	return(0);
}
