#include<stdio.h>

#define JGN_NUM_ELEMENTS 10

int main(void)
{
	int jgn_iArray[JGN_NUM_ELEMENTS];
	int jgn_i, jgn_num;

	//code
	printf("\n\n");

	printf("Enter integer elements for an array:");

	for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
	{
		scanf("%d", &jgn_num);
		jgn_iArray[jgn_i] = jgn_num;
	}

	printf("\n\n");
	printf("Even numbers amongst the array elements Are :\n\n");
        for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
	{
		if(jgn_iArray[jgn_i] % 2 == 0)
		{
			printf("%d\n", jgn_iArray[jgn_i]);
		}
	}

	printf("\n\n");
        printf("Odd numbers amongst the array elements Are :\n\n");
        for(jgn_i = 0; jgn_i < JGN_NUM_ELEMENTS; jgn_i++)
        {
                if(jgn_iArray[jgn_i] % 2 != 0)
                {
                        printf("%d\n", jgn_iArray[jgn_i]);
                }
        }
	
	return(0);
}
