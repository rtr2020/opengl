#include<stdio.h>

int main(void)
{
	int jgn_iArray[] = {6, 9, 3, 6, 56, 5, 23, 1, 5, 9};
	int jgn_int_size;
	int jgn_iArraySize;
	int jgn_iArrayNumElements;

	float jgn_fArray[] = {1.2, 2.9, 3.8, 6.4, 5.6, 5.7, 2.3, 1.7, 5.1, 9.8};
        int jgn_flt_size;
        int jgn_fArraySize;
        int jgn_fArrayNumElements;

	char jgn_cArray[] = {'J', 'I', 'T', 'H', 'I', 'N'};
	int jgn_charSize;
	int jgn_cArraySize;
	int jgn_cArrayNumElements;

	//code
	
	printf("\n\n");
	printf("Inline initialization and Piece-meal display of elements of iArray\n\n");
	printf("jgn_iArray[0] (1st Element) = %d\n", jgn_iArray[0]);
	printf("jgn_iArray[0] (2nd Element) = %d\n", jgn_iArray[1]);
	printf("jgn_iArray[0] (3rd Element) = %d\n", jgn_iArray[2]);
	printf("jgn_iArray[0] (4th Element) = %d\n", jgn_iArray[3]);
	printf("jgn_iArray[0] (5th Element) = %d\n", jgn_iArray[4]);
	printf("jgn_iArray[0] (6th Element) = %d\n", jgn_iArray[5]);
	printf("jgn_iArray[0] (7th Element) = %d\n", jgn_iArray[6]);
	printf("jgn_iArray[0] (8th Element) = %d\n", jgn_iArray[7]);
	printf("jgn_iArray[0] (9th Element) = %d\n", jgn_iArray[8]);
	printf("jgn_iArray[0] (10th Element) = %d\n", jgn_iArray[9]);
	
	jgn_int_size = sizeof(int);
	jgn_iArraySize = sizeof(jgn_iArray);
	jgn_iArrayNumElements = jgn_iArraySize / jgn_int_size;
	printf("Size of Datatype 'int' = %d bytes \n\n", jgn_int_size);
	printf("Number of elements in jgn_iArray = %d elements\n\n", jgn_iArraySize);
	printf("Size of 'iArray[]' (%d elements * %d bytes) = %d bytes\n\n",jgn_iArrayNumElements, jgn_int_size, jgn_iArraySize);



        printf("\n\n");
        printf("Inline initialization and Piece-meal display of elements of fArray\n\n");
        printf("jgn_fArray[0] (1st Element) = %f\n", jgn_fArray[0]);
        printf("jgn_fArray[0] (2nd Element) = %f\n", jgn_fArray[1]);
        printf("jgn_fArray[0] (3rd Element) = %f\n", jgn_fArray[2]);
        printf("jgn_fArray[0] (4th Element) = %f\n", jgn_fArray[3]);
        printf("jgn_fArray[0] (5th Element) = %f\n", jgn_fArray[4]);
        printf("jgn_fArray[0] (6th Element) = %f\n", jgn_fArray[5]);
        printf("jgn_fArray[0] (7th Element) = %f\n", jgn_fArray[6]);
        printf("jgn_fArray[0] (8th Element) = %f\n", jgn_fArray[7]);
        printf("jgn_fArray[0] (9th Element) = %f\n", jgn_fArray[8]);
        printf("jgn_fArray[0] (10th Element) = %f\n", jgn_fArray[9]);

	jgn_flt_size = sizeof(float);
        jgn_fArraySize = sizeof(jgn_fArray);
        jgn_fArrayNumElements = jgn_fArraySize / jgn_flt_size;
        printf("Size of Datatype 'int' = %d bytes \n\n", jgn_flt_size);
        printf("Number of elements in jgn_iArray = %d elements\n\n", jgn_fArraySize);
        printf("Size of 'iArray[]' (%d elements * %d bytes) = %d bytes\n\n",jgn_fArrayNumElements, jgn_flt_size, jgn_fArraySize);


	printf("\n\n");
        printf("Inline initialization and Piece-meal display of elements of cArray\n\n");
        printf("jgn_cArray[0] (1st Element) = %c\n", jgn_cArray[0]);
        printf("jgn_cArray[0] (2nd Element) = %c\n", jgn_cArray[1]);
        printf("jgn_cArray[0] (3rd Element) = %c\n", jgn_cArray[2]);
        printf("jgn_cArray[0] (4th Element) = %c\n", jgn_cArray[3]);
        printf("jgn_cArray[0] (5th Element) = %c\n", jgn_cArray[4]);
        printf("jgn_cArray[0] (6th Element) = %c\n", jgn_cArray[5]);

        jgn_charSize = sizeof(char);
        jgn_cArraySize = sizeof(jgn_cArray);
        jgn_cArrayNumElements = jgn_cArraySize / jgn_charSize;
        printf("Size of Datatype 'int' = %d bytes \n\n", jgn_charSize);
        printf("Number of elements in jgn_iArray = %d elements\n\n", jgn_cArraySize);
        printf("Size of 'iArray[]' (%d elements * %d bytes) = %d bytes\n\n",jgn_cArrayNumElements, jgn_charSize, jgn_cArraySize);

	return(0);
}

