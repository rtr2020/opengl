#include<stdio.h>

int main(void)
{
	int jgn_iArray[] = {6, 9, 3, 6, 56, 5, 23, 1, 5, 9};
	int jgn_int_size;
	int jgn_iArraySize;
	int jgn_iArrayNumElements;

	float jgn_fArray[] = {1.2, 2.9, 3.8, 6.4, 5.6, 5.7, 2.3, 1.7, 5.1, 9.8};
        int jgn_flt_size;
        int jgn_fArraySize;
        int jgn_fArrayNumElements;

	char jgn_cArray[] = {'J', 'I', 'T', 'H', 'I', 'N'};
	int jgn_charSize;
	int jgn_cArraySize;
	int jgn_cArrayNumElements;

	int jgn_i;

	//code
	
	jgn_int_size = sizeof(int);
	jgn_iArraySize = sizeof(jgn_iArray);
	jgn_iArrayNumElements = jgn_iArraySize / jgn_int_size;

	printf("\n\n");
	for(jgn_i = 0; jgn_i < jgn_iArrayNumElements; jgn_i++)
	{
		printf("jgn_iArray[jgn_i] (1st Element) = %d\n", jgn_iArray[jgn_i]);
	}
	printf("Size of Datatype 'int' = %d bytes \n\n", jgn_int_size);
	printf("Number of elements in jgn_iArray = %d elements\n\n", jgn_iArraySize);
	printf("Size of 'iArray[]' (%d elements * %d bytes) = %d bytes\n\n",jgn_iArrayNumElements, jgn_int_size, jgn_iArraySize);



        printf("\n\n");
        printf("Inline initialization and Piece-meal display of elements of fArray\n\n");
	jgn_flt_size = sizeof(float);
        jgn_fArraySize = sizeof(jgn_fArray);
        jgn_fArrayNumElements = jgn_fArraySize / jgn_flt_size;

	jgn_i = 0;
	while(jgn_i < jgn_fArrayNumElements)
	{
		printf("jgn_fArray[jgn_i] (1st Element) = %f\n", jgn_fArray[jgn_i]);
		jgn_i++;	
	}

        printf("Size of Datatype 'int' = %d bytes \n\n", jgn_flt_size);
        printf("Number of elements in jgn_iArray = %d elements\n\n", jgn_fArraySize);
        printf("Size of 'iArray[]' (%d elements * %d bytes) = %d bytes\n\n",jgn_fArrayNumElements, jgn_flt_size, jgn_fArraySize);


	printf("\n\n");
        printf("Inline initialization and Piece-meal display of elements of cArray\n\n");
        jgn_charSize = sizeof(char);
        jgn_cArraySize = sizeof(jgn_cArray);
        jgn_cArrayNumElements = jgn_cArraySize / jgn_charSize;
	jgn_i = 0; 
	do
	{
		printf("jgn_cArray[jgn_i] (1st Element) = %c\n", jgn_cArray[jgn_i]);
		jgn_i++;
	}while(jgn_i < jgn_cArrayNumElements);
        printf("Size of Datatype 'int' = %d bytes \n\n", jgn_charSize);
        printf("Number of elements in jgn_iArray = %d elements\n\n", jgn_cArraySize);
        printf("Size of 'iArray[]' (%d elements * %d bytes) = %d bytes\n\n",jgn_cArrayNumElements, jgn_charSize, jgn_cArraySize);

	return(0);
}

