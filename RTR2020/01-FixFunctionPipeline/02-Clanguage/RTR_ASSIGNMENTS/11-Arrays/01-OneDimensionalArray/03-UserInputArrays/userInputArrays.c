#include<stdio.h>

#define JGN_INT_ARRAY_NUM_ELEMENTS 5
#define JGN_FLOAT_ARRAY_NUM_ELEMENTS 4
#define JGN_CHAR_ARRAY_NUM_ELEMENTS 12

int main(void)
{
	int jgn_iArray[JGN_INT_ARRAY_NUM_ELEMENTS];
	float jgn_fArray[JGN_FLOAT_ARRAY_NUM_ELEMENTS];
	char jgn_cArray[JGN_CHAR_ARRAY_NUM_ELEMENTS];
	int jgn_i;

	//code
	
	printf("\n\n");
	printf("Enter elements for 'Integer' array : \n");
	for (jgn_i = 0; jgn_i < JGN_INT_ARRAY_NUM_ELEMENTS; jgn_i++)
	{
		scanf("%d", &jgn_iArray[jgn_i]);
	}

	printf("\n\n");
        printf("Enter elements for 'Floating point' array : \n");
        for (jgn_i = 0; jgn_i < JGN_FLOAT_ARRAY_NUM_ELEMENTS; jgn_i++)
        {
                scanf("%f", &jgn_fArray[jgn_i]);
        }


	printf("\n\n");
	printf("Enter elements for 'Character' array : \n");
        for (jgn_i = 0; jgn_i < JGN_CHAR_ARRAY_NUM_ELEMENTS; jgn_i++)
        {
                scanf(" %c", &jgn_cArray[jgn_i]);
        }

        printf("\n\n");
	printf("Elements of 'Integer' array : \n");
        for (jgn_i = 0; jgn_i < JGN_INT_ARRAY_NUM_ELEMENTS; jgn_i++)
        {
                printf("%d\n", jgn_iArray[jgn_i]);
        }

        printf("\n\n");
        printf("Elements of 'Floating point' array : \n");
        for (jgn_i = 0; jgn_i < JGN_FLOAT_ARRAY_NUM_ELEMENTS; jgn_i++)
        {
                printf("%f\n", jgn_fArray[jgn_i]);
        }

        printf("\n\n");
        printf("Elements of 'Character' array : \n");
        for (jgn_i = 0; jgn_i < JGN_CHAR_ARRAY_NUM_ELEMENTS; jgn_i++)
        {
                printf("%c\n", jgn_cArray[jgn_i]);
        }

	printf("\n\n");

	return(0);

}
