#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	int jgn_iArray_2D[NUM_ROWS][NUM_COLUMNS];
	int jgn_iArray_1D[NUM_ROWS * NUM_COLUMNS];
	
	int jgn_i, jgn_j;
	int jgn_num;

	printf("Enter your elements for 2D array:\n");

	for(jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
	{
		printf("For row number %d: \n", jgn_i+1);

		for(jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
		{
			printf("Enter element number %d: \n", jgn_j+1);
			scanf("%d", &jgn_num);
			jgn_iArray_2D[jgn_i][jgn_j] = jgn_num;
		}
		printf("\n\n");
	}

	printf("\n\n");
	printf("Two dimentional array of integers: \n\n");

	for(jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
        {
             	printf("**************Row %d************\n", (jgn_i + 1));

                for(jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
                {
                        printf("jgn_iArray_2D[%d][%d] = %d\n", jgn_i, jgn_j, jgn_iArray_2D[jgn_i][jgn_j]);
                }
                printf("\n\n");
        }


	for(jgn_i = 0; jgn_i < NUM_ROWS; jgn_i++)
        {
                for(jgn_j = 0; jgn_j < NUM_COLUMNS; jgn_j++)
                {
                        jgn_iArray_1D[(jgn_i * NUM_COLUMNS) + jgn_j] = jgn_iArray_2D[jgn_i][jgn_j];
                }
        }


        printf("\n\n");
        printf("2D array after conversion to 1D array: \n\n");

        for(jgn_i = 0; jgn_i < NUM_ROWS * NUM_COLUMNS; jgn_i++)
        {

		printf("jgn_iArray_1D[%d] = %d\n", jgn_i, jgn_iArray_1D[jgn_i]);
               
        }
	printf("\n\n");

	return(0);
}
