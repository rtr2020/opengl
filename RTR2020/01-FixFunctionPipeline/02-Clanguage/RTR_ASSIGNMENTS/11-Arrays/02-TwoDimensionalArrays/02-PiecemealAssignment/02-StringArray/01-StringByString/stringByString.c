#include<stdio.h>

#define JGN_MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrcpy(char[], char[]);

	char jgn_strArray[5][10];
	int jgn_char_size;
	int jgn_strArray_size;
	int jgn_strArray_num_elements, jgn_strArray_num_rows, jgn_strArray_num_columns;
	int jgn_i;

	printf("\n\n");

	jgn_char_size = sizeof(char);

	jgn_strArray_size = sizeof(jgn_strArray);
	printf("Size of 2D array:= %d\n\n", jgn_strArray_size);

	jgn_strArray_num_rows = jgn_strArray_size / sizeof(jgn_strArray[0]);
	printf("Number of rows in 2D array:= %d\n\n", jgn_strArray_num_rows);

	jgn_strArray_num_columns = sizeof(jgn_strArray[0]) / jgn_char_size;
        printf("Number of columns in 2D array:= %d\n\n", jgn_strArray_num_columns);

	jgn_strArray_num_elements = jgn_strArray_num_rows * jgn_strArray_num_columns;
	printf("Maximum number of elements in 2D array is:= %d\n\n", jgn_strArray_num_elements);

	MyStrcpy(jgn_strArray[0], "My");
	MyStrcpy(jgn_strArray[1], "Name");
	MyStrcpy(jgn_strArray[2], "Is");
	MyStrcpy(jgn_strArray[3], "Jithin");
	MyStrcpy(jgn_strArray[4], "Nair");

	printf("\n\n");

	printf("The strings in 2D array are :\n\n");

	for (jgn_i = 0; jgn_i < jgn_strArray_num_rows; jgn_i++)
		printf("%s ", jgn_strArray[jgn_i]);
	printf("\n\n");
	return(0);
}

void MyStrcpy(char jgn_str_destination[], char jgn_str_source[])
{
	int MyStrlen(char[]);
	int jgn_iStringLength = 0;
	int jgn_j;

	jgn_iStringLength = MyStrlen(jgn_str_source);
	for (jgn_j = 0; jgn_j < jgn_iStringLength; jgn_j++)
		jgn_str_destination[jgn_j] = jgn_str_source[jgn_j];
	jgn_str_destination[jgn_j] = '\0';

}

int MyStrlen(char jgn_str[])
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < JGN_MAX_STRING_LENGTH; jgn_j++)
	{
		if (jgn_str[jgn_j] == '\0')
			break;
		else
			jgn_string_length++;

	}
	return(jgn_string_length++);
}
