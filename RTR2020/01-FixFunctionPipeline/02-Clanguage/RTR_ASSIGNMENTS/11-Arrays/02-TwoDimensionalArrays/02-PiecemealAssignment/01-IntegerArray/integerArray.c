#include<stdio.h>

int main(void)
{
	int jgn_iArray[3][5];
	int jgn_int_size;
	int jgn_iArray_size;
	int jgn_iArray_num_elements, jgn_iArray_num_rows, jgn_iArray_num_columns;
	int jgn_i, jgn_j;

	//code
	printf("\n\n");

	jgn_int_size = sizeof(int);

	jgn_iArray_size = sizeof(jgn_iArray);
	printf("Size of 2D array:= %d\n\n", jgn_iArray_size);

	jgn_iArray_num_rows = jgn_iArray_size / sizeof(jgn_iArray[0]);

	printf("Number of rows in 2D array : =%d\n\n", jgn_iArray_num_rows);

	jgn_iArray_num_columns = sizeof(jgn_iArray[0]) / jgn_int_size;
	printf("Number of columns in 2D array : =%d\n\n", jgn_iArray_num_columns);

	jgn_iArray_num_elements = jgn_iArray_num_rows * jgn_iArray_num_columns;

	printf("Number of elements in 2D array:= %d", jgn_iArray_num_elements);

	printf("\n\n");

	printf("Elements in the 2D array : \n\n");

	jgn_iArray[0][0] = 2;
	jgn_iArray[0][1] = 21;
	jgn_iArray[0][2] = 12;
	jgn_iArray[0][3] = 60;
	jgn_iArray[0][4] = 102;

        jgn_iArray[1][0] = 12;
        jgn_iArray[1][1] = 41;
        jgn_iArray[1][2] = 102;
        jgn_iArray[1][3] = 62;
        jgn_iArray[1][4] = 12;

        jgn_iArray[2][0] = 23;
        jgn_iArray[2][1] = 22;
        jgn_iArray[2][2] = 103;
        jgn_iArray[2][3] = 67;
        jgn_iArray[2][4] = 12;


	for (jgn_i = 0; jgn_i < jgn_iArray_num_rows; jgn_i++)
	{
		printf("*** ROW %d ***\n", (jgn_i + 1));
		for (jgn_j = 0; jgn_j < jgn_iArray_num_columns; jgn_j++)
		{
			printf("iArray[%d][%d] = %d\n", jgn_i, jgn_j, jgn_iArray[jgn_i][jgn_j]);
		}
		printf("\n\n");
	}

	return(0);
}
