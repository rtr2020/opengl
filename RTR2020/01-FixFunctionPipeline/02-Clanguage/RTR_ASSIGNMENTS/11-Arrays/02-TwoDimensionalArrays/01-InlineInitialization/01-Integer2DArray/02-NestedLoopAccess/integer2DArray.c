#include<stdio.h>

int main(void)
{
	int jgn_iArray[5][3] = { {1, 2, 3}, {3, 7, 7}, {2, 6, 5}, {3, 7, 14}, {6, 11, 14} };

	int jgn_int_size;
	int jgn_iArray_size;
	int jgn_iArray_num_elements, jgn_iArray_num_rows, jgn_iArray_num_columns;
	int jgn_i, jgn_j;
	//code
	
	printf("\n\n");

	jgn_int_size = sizeof(int);

	jgn_iArray_size = sizeof(jgn_iArray);

	printf("Size of 2-dimentional Array is: = %d", jgn_iArray_size);

	jgn_iArray_num_rows = jgn_iArray_size / sizeof(jgn_iArray[0]);

	printf("Number of rows in two dimensional (2D) integer array is = %d\n\n", jgn_iArray_num_rows);

	jgn_iArray_num_columns = sizeof(jgn_iArray[0]) / jgn_int_size;

	printf("Number of columns in two dimensional (2D) integer array is = %d\n\n", jgn_iArray_num_columns);

	jgn_iArray_num_elements = jgn_iArray_num_rows * jgn_iArray_num_columns;

	printf("Number of elements in two dimensional (2D) integer array Is = %d\n\n", jgn_iArray_num_elements);

	printf("\n\n");

	printf("Elements in The 2D array : \n\n");
	
       	for (jgn_i = 0; jgn_i < jgn_iArray_num_rows; jgn_i++)
	{
		printf("****** ROW %d ******\n", (jgn_i + 1));
		for (jgn_j = 0; jgn_j < jgn_iArray_num_columns; jgn_j++)
		{
			printf("iArray[%d][%d] = %d\n", jgn_i, jgn_j, jgn_iArray[jgn_i][jgn_j]);
		}
		printf("\n\n");
	}

	return(0);
}
