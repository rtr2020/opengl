#include<stdio.h>

int main(void)
{
	int jgn_iArray[5][3] = { {1, 2, 3}, {3, 7, 7}, {2, 6, 5}, {3, 7, 14}, {6, 11, 14} };

	int jgn_int_size;
	int jgn_iArray_size;
	int jgn_iArray_num_elements, jgn_iArray_num_rows, jgn_iArray_num_columns;

	//code
	
	printf("\n\n");

	jgn_int_size = sizeof(int);

	jgn_iArray_size = sizeof(jgn_iArray);

	printf("Size of 2-dimentional Array is: = %d", jgn_iArray_size);

	jgn_iArray_num_rows = jgn_iArray_size / sizeof(jgn_iArray[0]);

	printf("Number of rows in two dimensional (2D) integer array is = %d\n\n", jgn_iArray_num_rows);

	jgn_iArray_num_columns = sizeof(jgn_iArray[0]) / jgn_int_size;

	printf("Number of columns in two dimensional (2D) integer array is = %d\n\n", jgn_iArray_num_columns);

	jgn_iArray_num_elements = jgn_iArray_num_rows * jgn_iArray_num_columns;

	printf("Number of elements in two dimensional (2D) integer array Is = %d\n\n", jgn_iArray_num_elements);

	printf("\n\n");

	printf("Elements in The 2D array : \n\n");

	printf("**** Row 1 ****\n");
	printf("iArray[0][0] = %d\n", jgn_iArray[0][0]);
	printf("iArray[0][1] = %d\n", jgn_iArray[0][1]);
	printf("iArray[0][2] = %d\n", jgn_iArray[0][2]);

	printf("\n\n");

	printf("**** Row 2 ****\n");

        printf("iArray[1][0] = %d\n", jgn_iArray[1][0]);
        printf("iArray[1][1] = %d\n", jgn_iArray[1][1]);
        printf("iArray[1][2] = %d\n", jgn_iArray[1][2]);


        printf("\n\n");

        printf("**** Row 3 ****\n");

        printf("iArray[2][0] = %d\n", jgn_iArray[2][0]);
        printf("iArray[2][1] = %d\n", jgn_iArray[2][1]);
        printf("iArray[2][2] = %d\n", jgn_iArray[2][2]);


	printf("\n\n");

        printf("**** Row 4 ****\n");

        printf("iArray[3][0] = %d\n", jgn_iArray[3][0]);
        printf("iArray[3][1] = %d\n", jgn_iArray[3][1]);
        printf("iArray[3][2] = %d\n", jgn_iArray[3][2]);


        printf("\n\n");

        printf("**** Row 5 ****\n");

        printf("iArray[4][0] = %d\n", jgn_iArray[4][0]);
        printf("iArray[4][1] = %d\n", jgn_iArray[4][1]);
        printf("iArray[4][2] = %d\n", jgn_iArray[4][2]);

	return(0);

}
