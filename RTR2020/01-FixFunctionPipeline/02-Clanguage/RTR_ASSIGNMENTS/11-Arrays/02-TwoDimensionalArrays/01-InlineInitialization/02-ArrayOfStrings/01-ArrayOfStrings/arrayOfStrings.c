#include<stdio.h>

#define JGN_MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrlen(char[]);

	char jgn_strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering", "Batch", "2020", "Of", "Astromedicomp" };

	int jgn_char_size;
	int jgn_strArray_size;
	int jgn_strArray_num_elements, jgn_strArray_num_rows, jgn_strArray_num_columns;
	int jgn_strActual_num_chars = 0;
	int jgn_i;

	printf("\n\n");

	jgn_char_size = sizeof(char);
	jgn_strArray_size = sizeof(jgn_strArray);
	printf("Size of two dimentional character array	is = %d\n\n", jgn_strArray_size);

	jgn_strArray_num_rows = jgn_strArray_size / sizeof(jgn_strArray[0]);
	printf("Number of rows in two dimensional array is = %d\n\n", jgn_strArray_num_rows);

	jgn_strArray_num_columns = sizeof(jgn_strArray[0]) / jgn_char_size;
	printf("Number of columns in two dimentional array is = %d\n\n", jgn_strArray_num_columns);

	jgn_strArray_num_elements = jgn_strArray_num_rows * jgn_strArray_num_columns;

	printf("Maximum number of elements is this 2d array is = %d\n\n", jgn_strArray_num_elements);

	for (jgn_i = 0; jgn_i < jgn_strArray_num_rows; jgn_i++)
	{
		jgn_strActual_num_chars = jgn_strActual_num_chars + MyStrlen(jgn_strArray[jgn_i]);
	}

	printf("Actual number of elements in 2D array is = %d\n\n", jgn_strActual_num_chars);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");

	printf("%s ", jgn_strArray[0]);
	printf("%s ", jgn_strArray[1]);
	printf("%s ", jgn_strArray[2]);
	printf("%s ", jgn_strArray[3]);
	printf("%s ", jgn_strArray[4]);
	printf("%s ", jgn_strArray[5]);
	printf("%s ", jgn_strArray[6]);
	printf("%s ", jgn_strArray[7]);
	printf("%s ", jgn_strArray[8]);
	printf("%s ", jgn_strArray[9]);

	printf("\n\n");
	return(0);
}

int MyStrlen(char str[])
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < JGN_MAX_STRING_LENGTH; jgn_j++)
	{
		if (str[jgn_j] == '\0')
			break;
		else
			jgn_string_length++;
	}
	return(jgn_string_length);
}
