#include<stdio.h>

#define JGN_MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrlen(char[]);

	char jgn_strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering", "Batch", "2020", "Of", "Astromedicomp" };

	int jgn_iStrLengths[10];

	int jgn_strArray_size;
	int jgn_strArray_num_rows;
	int jgn_i, jgn_j;

	printf("\n\n");

	jgn_strArray_size = sizeof(jgn_strArray);

	jgn_strArray_num_rows = jgn_strArray_size / sizeof(jgn_strArray[0]);

	for (jgn_i = 0; jgn_i < jgn_strArray_num_rows; jgn_i++)
	{
		jgn_iStrLengths[jgn_i] = MyStrlen(jgn_strArray[jgn_i]);
	}

	printf("\n\n");
	printf("The entire string array : \n\n");
	for (jgn_i = 0; jgn_i < jgn_strArray_num_rows; jgn_i++)
	{
		printf("%s ", jgn_strArray[jgn_i]);
	}

	printf("\n\n");
	printf("Strings in the 2D array : \n\n");
	for (jgn_i = 0; jgn_i < jgn_strArray_num_rows; jgn_i++)
	{
		printf("String number %d => %s\n\n", (jgn_i + 1), jgn_strArray[jgn_i]);	
		for (jgn_j = 0; jgn_j < jgn_iStrLengths[jgn_i]; jgn_j++)
		{
			printf("Character %d = %c\n", (jgn_j + 1), jgn_strArray[jgn_i][jgn_j]);
		}
		printf("\n\n");
	}

	return(0);
}

int MyStrlen(char str[])
{
	int jgn_j;
	int jgn_string_length = 0;

	for (jgn_j = 0; jgn_j < JGN_MAX_STRING_LENGTH; jgn_j++)
	{
		if (str[jgn_j] == '\0')
			break;
		else
			jgn_string_length++;
	}
	return(jgn_string_length);
}
