#include<stdio.h>

int main()
{
	//code
	int i_jit = 5;
	float f_jit = 3.9f;
	double d_jit = 8.041997;
	char c_jit = 'A';

	printf("\n \n");

	printf("i = %d\n", i_jit);
	printf("f = %f\n", f_jit);
	printf("d = %lf\n", d_jit);
	printf("c = %c\n", c_jit);

	printf("\n\n");

	i_jit = 43;
	f_jit = 6.54f;
	d_jit = 26.1294;
	c_jit = 'p';

	printf("i = %d\n", i_jit);
	printf("f = %f\n", f_jit);
	printf("d = %lf\n", d_jit);
	printf("c = %c\n", c_jit);

	printf("\n\n");

	return(0);

}
