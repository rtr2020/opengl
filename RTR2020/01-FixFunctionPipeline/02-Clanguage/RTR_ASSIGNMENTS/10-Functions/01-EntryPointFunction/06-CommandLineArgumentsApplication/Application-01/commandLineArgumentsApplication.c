#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>

int main(int argc, char *argv[], char *env[])
{
	int jgn_i;
	int jgn_num;
	int jgn_sum = 0;

	//code
	if(argc == 1)
	{
		printf("\n\n");
		printf("No numbers given for addition, Exiting\n\n");
		printf("Usage: commandLineArgumentsApplication <first number> <second number> ...\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Sum of all additions of command line arguments is : \n\n");

	for(jgn_i = 1; jgn_i < argc; jgn_i++)
	{
		jgn_num = atoi(argv[jgn_i]);
		jgn_sum = jgn_sum + jgn_num;
	}

	printf("Sum = %d\n\n", jgn_sum);
	return(0);
}
