#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>

int main(int argc, char *argv[], char *env[])
{
	int jgn_i;

	//code
	if(argc != 4)
	{
		printf("\n\n");
		printf("Invalid, Exiting\n\n");
		printf("Usage: commandLineArgumentsApplication <first name> <middle name> <last name> ...\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Your fullname is : \n\n");

	for(jgn_i = 1; jgn_i < argc; jgn_i++)
	{
		printf("%s ", argv[jgn_i]);
	}

	printf("\n\n");
	return(0);
}
