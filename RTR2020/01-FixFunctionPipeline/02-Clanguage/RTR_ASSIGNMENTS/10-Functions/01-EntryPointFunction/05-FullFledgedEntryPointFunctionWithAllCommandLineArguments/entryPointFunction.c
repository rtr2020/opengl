#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	int jgn_i;

	//code
	printf("\n\n");
	printf("Hello World!!\n\n");
	printf("Number of Command line arumnets passed = %d\n\n", argc);

	printf("Command line arguments passed to this program are: \n\n");
	for(jgn_i = 0; jgn_i < argc; jgn_i++)
	{
		printf("Command Line argument number %d = %s\n", (jgn_i + 1), argv[jgn_i]);
	}
	printf("\n\n");

	printf("First 20 environment variables passed to the functions are: \n\n");

	for(jgn_i = 0; jgn_i < 20; jgn_i++)
	{
		printf("Environment Variable Number %d = %s\n", (jgn_i + 1), envp[jgn_i]);
	}
	printf("\n\n");
	return(0);
}

