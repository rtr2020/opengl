#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	unsigned int jgn_num;
	void recursive(unsigned int);
	printf("\n\n");
	printf("Enter any Number:\n\n");
	scanf("%u", &jgn_num);

	printf("\n\n");

	printf("Output Of recursive function: \n\n");
	recursive(jgn_num);

	printf("\n\n");
	return(0);
}

void recursive(unsigned int jgn_n)
{
	printf("n = %d\n", jgn_n);
	if(jgn_n > 0)
	{
		recursive(jgn_n - 1);
	}
}
