#include<stdio.h>

int main(int argc, char *argv[], char *env[])
{
	int jgn_addition(int, int);

	int jgn_a, jgn_b, jgn_result;
	//code
	printf("\n\n");
	printf("Enter integer value for 'A' : ");
	scanf("%d", &jgn_a);

        printf("Enter integer value for 'B' : ");
        scanf("%d", &jgn_b);

	jgn_result = jgn_addition(jgn_a, jgn_b);
	
	printf("\n\n");
	printf("Sum Of %d and %d = %d\n\n", jgn_a, jgn_b, jgn_result);
	return(0);
}

int jgn_addition(int jgn_a, int jgn_b)
{
	int jgn_sum;

	//code
	jgn_sum = jgn_a + jgn_b;
	return(jgn_sum);
}
