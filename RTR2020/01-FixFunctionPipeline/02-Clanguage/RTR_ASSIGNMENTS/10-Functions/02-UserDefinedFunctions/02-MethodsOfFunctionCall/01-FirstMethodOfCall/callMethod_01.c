#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	void my_addition(void);
	int my_subtraction(void);
	void my_multiplication(int, int);
	int my_division(int, int);

	int jgn_result_sub;
	int jgn_a_multi, jgn_b_multi;
	int jgn_a_div, jgn_b_div, jgn_div;

	//code
	
	my_addition();

	jgn_result_sub = my_subtraction();
	printf("\n\n");
	printf("Subtraction result is = %d\n", jgn_result_sub);

	printf("\n\n");
	printf("Enter integer value of 'A' for multiplication : ");
	scanf("%d", &jgn_a_multi);

        printf("Enter integer value of 'B' for multiplication : ");
        scanf("%d", &jgn_b_multi);

	my_multiplication(jgn_a_multi, jgn_b_multi);

	printf("\n\n");
        printf("Enter integer value of 'A' for Division : ");
        scanf("%d", &jgn_a_div);

        printf("Enter integer value of 'B' for Division : ");
        scanf("%d", &jgn_b_div);

	jgn_div = my_division(jgn_a_div, jgn_b_div);
	printf("\n\n");

	printf("Division gives result = %d", jgn_div);

	printf("\n\n");
}

void my_addition(void)
{
	int jgn_a, jgn_b, jgn_sum;
	//code
	printf("\n\n");
	printf("Enter integer value of 'A': for addition");
        scanf("%d", &jgn_a);

        printf("Enter integer value of 'B': for addition");
        scanf("%d", &jgn_b);

	jgn_sum = jgn_a + jgn_b;
	printf("\n\n");
	printf("Sum of A and B gives = %d\n\n", jgn_sum);
}

int my_subtraction(void)
{
        int jgn_a, jgn_b, jgn_sub;
        //code
        printf("\n\n");
        printf("Enter integer value of 'A': for subtraction");
        scanf("%d", &jgn_a);

        printf("Enter integer value of 'B': for subtraction");
        scanf("%d", &jgn_b);

        jgn_sub = jgn_a - jgn_b;
        return(jgn_sub);
}

void my_multiplication(int jgn_a, int jgn_b)
{
	int jgn_multi;
	jgn_multi = jgn_a * jgn_b;

	printf("\n\n");
	printf("Multiplication of %d and %d = %d\n\n", jgn_a, jgn_b, jgn_multi);
}

int my_division(int jgn_a, int jgn_b)
{
	int jgn_div_quot;
	
	if(jgn_a > jgn_b)
		jgn_div_quot = jgn_a / jgn_b;
	else
                jgn_div_quot = jgn_b / jgn_a;

	return(jgn_div_quot);

}
