#include <stdio.h>

#define JGN_MAX_NUMBER(a, b) ((a > b) ? a : b)

int main(int argc, char *argv[], char *envp[])
{
	int jgn_inum01;
	int jgn_inum02;
	int jgn_iresult;

	float jgn_fnum01;
	float jgn_fnum02;
	float jgn_fresult;

	printf("\n\n");
	printf("Enter an integer number : \n\n");
	scanf("%d", &jgn_inum01);


	printf("\n\n");
        printf("Enter another integer number : \n\n");
        scanf("%d", &jgn_inum02);

	jgn_iresult = JGN_MAX_NUMBER(jgn_inum01, jgn_inum02);

	printf("\n\n");
	printf("Result Of Max integer number is = %d\n", jgn_iresult);

	printf("\n\n");
        printf("Enter a float number : \n\n");
        scanf("%f", &jgn_fnum01);


        printf("\n\n");
        printf("Enter another floating number : \n\n");
        scanf("%f", &jgn_fnum02);

        jgn_fresult = JGN_MAX_NUMBER(jgn_fnum01, jgn_fnum02);

        printf("\n\n");
        printf("Result Of Max float number is = %f\n", jgn_fresult);

	return(0);
}
