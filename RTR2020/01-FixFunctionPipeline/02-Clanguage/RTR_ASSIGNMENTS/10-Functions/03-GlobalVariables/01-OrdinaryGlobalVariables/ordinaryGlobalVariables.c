#include <stdio.h>

int jgn_global_cnt = 0;

int main(void)
{
	void change_count_one(void);
	void change_count_two(void);
	void change_count_three(void);

	printf("\n\n");

	printf("main() : Value of global_count = %d\n", jgn_global_cnt);
	change_count_one();
	change_count_two();
	change_count_three();
	
	printf("\n\n");
	return(0);
}

void change_count_one(void)
{
	jgn_global_cnt = 100;
	printf("change_count_one() : Value of global_count = %d\n", jgn_global_cnt);
}

void change_count_two(void)
{
 	jgn_global_cnt = jgn_global_cnt + 1;
	printf("change_count_two() : Value of global_count = %d\n", jgn_global_cnt);
}

void change_count_three(void)
{
	jgn_global_cnt = jgn_global_cnt + 10;
	printf("change_count_three() : Value of global_count = %d\n", jgn_global_cnt);
}


