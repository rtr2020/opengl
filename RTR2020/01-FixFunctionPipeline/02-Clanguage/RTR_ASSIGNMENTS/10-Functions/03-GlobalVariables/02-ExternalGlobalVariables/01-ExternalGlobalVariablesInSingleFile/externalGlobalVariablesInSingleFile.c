#include <stdio.h>

int main(void)
{
	void change_count(void);

	extern int jgn_global_count;

	printf("\n");

	printf("Value Of jgn_global_count before change_count() = %d\n", jgn_global_count);
	change_count();

	printf("Value Of jgn_global_count after change_count() = %d\n", jgn_global_count);
	printf("\n");
	return(0);
}

int jgn_global_count = 0;

void change_count(void)
{
	jgn_global_count = 9;
	printf("Value Of jgn_global_count in change_count() = %d\n", jgn_global_count);
}
