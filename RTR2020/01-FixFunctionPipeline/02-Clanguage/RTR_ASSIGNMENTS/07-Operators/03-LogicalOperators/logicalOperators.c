#include<stdio.h>

int main(void)
{       
        int jgn_a;
        int jgn_b;
	int jgn_c;
        int jgn_result;

        printf("\n\n");
        printf("\nEnter First Integer:");
        scanf("%d", &jgn_a);

        printf("\nEnter Second Integer:");
        scanf("%d", &jgn_b);

	printf("\nEnter Third Integer:");
        scanf("%d", &jgn_c);

        printf("\n\n");
        printf("If Answer=0, It is 'FLASE'\n");
        printf("If Answer=1, It is 'TRUE'\n");

	jgn_result = (jgn_a <= jgn_b) && (jgn_b != jgn_c);
	printf("Logical AND (&&): Answer is TRUE if both conditions are true and FALSE if any one or both conditions are false\n");
	printf("(%d <= %d) && (%d != %d) : Answer = %d\n\n", jgn_a, jgn_b, jgn_b, jgn_c, jgn_result);

	jgn_result = (jgn_b >= jgn_a) || (jgn_a == jgn_c);
        printf("Logical OR (||): Answer is TRUE if any one or both conditions are true and FALSE if both conditions are false\n");
        printf("(%d >= %d) && (%d == %d) : Answer = %d\n\n", jgn_b, jgn_a, jgn_a, jgn_c, jgn_result);

	jgn_result = !jgn_a;
	printf("!%d (! Logical NOT) Gives Answer = %d\n", jgn_a, jgn_result);

        jgn_result = !jgn_b;
        printf("!%d (! Logical NOT) Gives Answer = %d\n", jgn_b, jgn_result);

        jgn_result = !jgn_c;
        printf("!%d (! Logical NOT) Gives Answer = %d\n\n", jgn_c, jgn_result);

	jgn_result = (!(jgn_a <= jgn_b) && !(jgn_b != jgn_c));
	printf("Using Logical NOT (!) on (%d <= %d) And Also On (%d != %d) And then AND-ing them both gives Answer = %d\n\n", jgn_a, jgn_b, jgn_b, jgn_c, jgn_result);


	jgn_result = !((jgn_b >= jgn_a) || (jgn_a == jgn_c));
        printf("Using Logical NOT (!) on (%d >= %d) And Also On (%d == %d) And then AND-ing them both gives Answer = %d\n\n", jgn_b, jgn_a, jgn_a, jgn_c, jgn_result);

	return(0);
}   

