#include <stdio.h>
int main(void)
{
	int a_jit;
	int b_jit;
	int result_jit;

	printf("\n");
	printf("Enter a number:");
	scanf("%d", &a_jit);

	printf("\n");
	printf("Enter another number:");
	scanf("%d", &b_jit);

	printf("\n");

	result_jit = a_jit + b_jit;
	printf("Addition of number %d and number %d is = %d\n", a_jit, b_jit, result_jit);

	result_jit = a_jit - b_jit;
	printf("Subtraction of number %d and number %d is = %d\n", a_jit, b_jit, result_jit);

	result_jit = a_jit * b_jit;
	printf("Multiplication of number %d and number %d is = %d\n", a_jit, b_jit, result_jit);

	result_jit = a_jit / b_jit;
	printf("Division of number %d and number %d is = %d\n", a_jit, b_jit, result_jit);

	result_jit = a_jit % b_jit;
	printf("Modulus of number %d and number %d is = %d\n", a_jit, b_jit, result_jit);

	printf("\n");

	return(0);
}
