#include <stdio.h>
int main(void)
{
	int a_jit;
	int b_jit;
	int x_jit;
	printf("\n");
	printf("Enter a Number:");
	scanf("%d", &a_jit);
	printf("\n");
	printf("Enter another number:");
	scanf("%d", &b_jit);

	x_jit = a_jit;
	a_jit += b_jit;
	printf("Addition Of %d and %d = %d\n", x_jit, b_jit, a_jit);

	x_jit = a_jit;
	a_jit -= b_jit;
	printf("Subtraction of %d from %d gives %d\n", b_jit, x_jit, a_jit);

	x_jit = a_jit;
	a_jit *= b_jit; 
	printf("Multiplication of %d and %d gives %d\n", x_jit, b_jit, a_jit);

	x_jit = a_jit;
	a_jit /= b_jit;
	printf("Division of %d by %d gives %d\n", x_jit, b_jit, a_jit);

	x_jit = a_jit;
	a_jit %= b_jit;
	printf("Modulo of %d and %d = %d.\n", x_jit, b_jit, a_jit);
	return(0);
}

