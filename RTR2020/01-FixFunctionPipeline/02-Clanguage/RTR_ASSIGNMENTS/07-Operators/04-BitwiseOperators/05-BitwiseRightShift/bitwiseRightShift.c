#include<stdio.h>

int main(void)
{
	void PrintBinaryFormOfNumber(unsigned int);

	unsigned int jgn_a;
	unsigned int jgn_num_bits;
	unsigned int jgn_result;

	printf("\n\n");
	printf("Enter an Integer = ");
	scanf("%u", &jgn_a);

	printf("\n\n");
	printf("By how many bits you want to shift= ");
	scanf("%u", &jgn_num_bits);

	printf("\n\n");

	jgn_result = jgn_a>>jgn_num_bits;
	printf("Bitwise Right-shifting %d By %d bits gives Result = %d\n\n", jgn_a, jgn_num_bits, jgn_result);

	PrintBinaryFormOfNumber(jgn_a);
	PrintBinaryFormOfNumber(jgn_result);

	return(0);
}

void PrintBinaryFormOfNumber(unsigned int jgn_decimal_number)
{
	unsigned int jgn_quotient, jgn_remainder;
	unsigned int jgn_num;
	unsigned int jgn_binary_array[8];
	int jgn_i;

	for(jgn_i = 0; jgn_i < 8; jgn_i++)
		jgn_binary_array[jgn_i] = 0;
	
	printf("The Binary form of the Decimal Integer %d is \t = \t", jgn_decimal_number);
	jgn_num = jgn_decimal_number;
	jgn_i = 7;
	while (jgn_num != 0)
	{
		jgn_quotient = jgn_num / 2;
		jgn_remainder = jgn_num % 2;
		jgn_binary_array[jgn_i] = jgn_remainder;
		jgn_num = jgn_quotient;
		jgn_i--;
	}

	for(jgn_i = 0; jgn_i < 8; jgn_i++)
		printf("%u", jgn_binary_array[jgn_i]);

	printf("\n\n");
}
