#include<stdio.h>

int main(void)
{       
        int jgn_a;
        int jgn_b;
        int jgn_result;

        printf("\n\n");
        printf("\nEnter one Integer:");
        scanf("%d", &jgn_a);

        printf("\nEnter another Integer:");
        scanf("%d", &jgn_b);

        printf("\n\n");
        printf("If Answer=0, It is 'FLASE'\n");
	printf("If Answer=1, It is 'TRUE'\n");

	printf("\n\n");
	jgn_result = (jgn_a < jgn_b);
	printf("(a<b): (\"%d\" is less than \"%d\") Answer = %d\n", jgn_a, jgn_b, jgn_result);

        printf("\n\n");
        jgn_result = (jgn_a > jgn_b);
        printf("(a>b): (\"%d\" is greater than \"%d\") Answer = %d\n", jgn_a, jgn_b, jgn_result);        
	
	
	printf("\n\n");
        jgn_result = (jgn_a <= jgn_b);
        printf("(a <= b): (\"%d\" is less than or equal to \"%d\") Answer = %d\n", jgn_a, jgn_b, jgn_result);


	printf("\n\n");
        jgn_result = (jgn_a >= jgn_b);
        printf("(a >= b): (\"%d\" is greater than or equal to \"%d\") Answer = %d\n", jgn_a, jgn_b, jgn_result);

	printf("\n\n");
        jgn_result = (jgn_a == jgn_b);
        printf("(a == b): (\"%d\" is equal to \"%d\") Answer = %d\n", jgn_a, jgn_b, jgn_result);


	printf("\n\n");
        jgn_result = (jgn_a != jgn_b);
        printf("(a != b): (\"%d\" is not equal to \"%d\") Answer = %d\n", jgn_a, jgn_b, jgn_result);

	return(0);
}   

