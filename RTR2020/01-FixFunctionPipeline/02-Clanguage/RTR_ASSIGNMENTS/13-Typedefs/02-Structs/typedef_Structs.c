#include <stdio.h>

#define MAX_NAME_LENGTH 100

struct Employee
{
	char name[MAX_NAME_LENGTH];
	unsigned int age;
	char gender;
	double salary;
};

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	typedef struct Employee JGN_EMPLOYEE_TYPE;
	typedef struct MyData JGN_DATA_TYPE;

	struct Employee jgn_emp = {"jithin", 27, 'M', 1500.00};
	JGN_EMPLOYEE_TYPE jgn_emp_typedef = {"Ashok", 22, 'M', 16400.00};

	struct MyData jgn_md = {78, 45.87f, 23.576845, 'Y'};

	JGN_DATA_TYPE jgn_md_typedef;
	
	jgn_md_typedef.i = 5;
	jgn_md_typedef.f = 1.6f;
	jgn_md_typedef.d = 8.654791;
	jgn_md_typedef.c = 'J';

	printf("\n\n");
	printf("struct Employee : \n\n");
	printf("jgn_emp.name = %s\n", jgn_emp.name);
	printf("jgn_emp.age = %d\n", jgn_emp.age);
	printf("jgn_emp.gender = %c\n", jgn_emp.gender);
	printf("jgn_emp.salary = %lf\n", jgn_emp.salary);

	printf("\n\n");
	printf("My Employee type : \n\n");
	printf("jgn_emp.name = %s\n", jgn_emp_typedef.name);
        printf("jgn_emp.age = %d\n", jgn_emp_typedef.age);
        printf("jgn_emp.gender = %c\n", jgn_emp_typedef.gender);
        printf("jgn_emp.salary = %lf\n", jgn_emp_typedef.salary);

	printf("\n\n");
	printf("Structure MyData : \n\n");
	printf("jgn_md.i = %d\n", jgn_md.i);
	printf("jgn_md.f = %f\n", jgn_md.f);
	printf("jgn_md.d = %lf\n", jgn_md.d);
	printf("jgn_md.c = %c\n", jgn_md.c);

        printf("\n\n");
        printf("MyDataType : \n\n");
        printf("jgn_md_typedef.i = %d\n", jgn_md_typedef.i);
        printf("jgn_md_typedef.f = %f\n", jgn_md_typedef.f);
        printf("jgn_md_typedef.d = %lf\n", jgn_md_typedef.d);
        printf("jgn_md_typedef.c = %c\n", jgn_md_typedef.c);

	printf("\n\n");

	return(0);	
}
