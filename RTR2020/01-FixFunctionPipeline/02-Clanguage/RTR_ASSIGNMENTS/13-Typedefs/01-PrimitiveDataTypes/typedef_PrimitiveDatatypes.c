#include <stdio.h>

typedef int MY_INT;

int main(void)
{
	MY_INT Add(MY_INT, MY_INT);

	typedef int MY_INT;

	typedef float JGN_FLOAT;

	typedef char JGN_CHAR;

	typedef double JGN_DOUBLE;

	typedef unsigned int UINT;
	typedef UINT HANDLE;
	typedef HANDLE HWND;
	typedef HANDLE HINSTANCE;

	MY_INT jgn_a = 11, jgn_i;

	MY_INT jgn_iArray[] = { 9, 12, 32, 43, 57, 69, 75, 85, 94 };

	JGN_FLOAT jgn_f = 23.6f;
	const JGN_FLOAT jgn_f_pi = 3.14f;

	JGN_CHAR ch = '*';
	JGN_CHAR chArray01[] = "Hello";
	JGN_CHAR chArray02[][10] = {"RTR", "BATCH", "2020"};

	JGN_DOUBLE d = 7.567842;

	UINT uint = 2956;
	HANDLE handle = 347;
	HWND hwnd = 7768;
	HINSTANCE hInstance = 589;

	printf("\n\n");

	printf("Type MY_INT variable jgn_a: %d\n", jgn_a);
	printf("\n\n");

	for (jgn_i = 0; jgn_i < (sizeof(jgn_iArray) / sizeof(int)); jgn_i++)
	{
		printf("Type MY_INT array variable jgn_iArray[%d] = %d\n", jgn_i, jgn_iArray[jgn_i]);
	}

	printf("\n\n");
	printf("Type JGN_FLOAT variable jgn_f = %f\n", jgn_f);
	printf("Type JGN_FLOAT constant jgn_f_pi = %f\n", jgn_f_pi);

	printf("\n\n");
	printf("Type JGN_DOUBLE variable d = %lf\n", d);

	printf("\n\n");
	printf("Type JGN_CHAR variable ch = %c\n", ch);

	printf("\n\n");
	printf("Type JGN_CHAR array variable chArray_01 = %s\n", chArray01);

	printf("\n\n");
	for (jgn_i = 0; jgn_i < (sizeof(chArray02) / sizeof(chArray02[0])); jgn_i++)
	{
		printf("%s\t", chArray02[jgn_i]);
	}

	printf("\n\n");

	printf("Type UINT variable uint = %u\n", uint);
	printf("Type HANDLE variable uint = %u\n", handle);
	printf("Type HWND variable uint = %u\n", hwnd);
	printf("Type HINSTANCE variable uint = %u\n", hInstance);

	printf("\n\n");

	MY_INT jgn_x = 53;
	MY_INT jgn_y = 34;
	MY_INT jgn_ret;

	jgn_ret = Add(jgn_x, jgn_y);

	printf("jgn_ret = %d\n\n", jgn_ret);

	return(0);
}

MY_INT Add(MY_INT jgn_a, MY_INT jgn_b)
{
	MY_INT jgn_c;

	jgn_c = jgn_a + jgn_b;

	return(jgn_c);
}
