#include<stdio.h>

int main()
{
	//code
	printf("\n");
	printf("\nGoing to Next Line..using \\n Escape sequence \n\n");
	printf("Demo of \t Horizontal \t tab using \\t escape sequence\n\n");
	printf("\"A Double Quoted Output\" Done Using \\\" \\\" Escape Sequence\n\n");
	printf("\'Single quote output done using \\\' \\\' escape sequence\n\n");
	printf("Backsapce turned to Backspace\b using \\b escape sequence\n\n");
	printf("\rDemonstrating carriage return using \\r escape sequence\n\n");
	printf("Demonstrating \rcarriage return using \\r escape sequence\n\n");
        printf("Demonstrating carriage return\r using \\r escape sequence\n\n");
	printf("Demonstrating \x41 using \\xhh escape sequence\n\n"); //Gives hexadecimal equivalent of 41 == A
	printf("Demonstrating \102 using \\ooo escape sequence\n\n"); //Gives Octal equivalent of 102 == B
	return(0);
}

