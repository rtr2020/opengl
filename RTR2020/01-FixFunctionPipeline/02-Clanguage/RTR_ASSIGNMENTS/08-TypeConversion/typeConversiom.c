#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j;
	char jgn_ch1, jgn_ch2;

	int jgn_a, jgn_result_int;
	float jgn_f, jgn_result_float;
	
	int jgn_i_explicit;
	float jgn_f_explicit;

	printf("\n\n");

	jgn_i = 60;
	jgn_ch1 = jgn_i;
	printf("\n I = %d", jgn_i);
	printf("\n Ch1 = %c(ch1 = i; //i is integer)", jgn_ch1);

        jgn_ch2 = 'P';
	jgn_j = jgn_ch2;
        printf("\n Ch2 = %c", jgn_ch2);
        printf("\n Ch1 = %d(j = ch2; //ch2 is character)", jgn_j);


	jgn_a = 6;
	jgn_f = 5.6;
	jgn_result_float = jgn_a + jgn_f;
	printf("\n(Implicit) Addition on integer(%d) and float(%f) and stored in float gives = %f", jgn_a, jgn_f, jgn_result_float);

	jgn_result_int = jgn_a + jgn_f;
        printf("\n(Implicit) Addition on integer(%d) and float(%f) and stored in Integer gives = %d", jgn_a, jgn_f, jgn_result_int);


	jgn_f_explicit = 47.877675f;
	jgn_i_explicit = (int)jgn_f_explicit;

	printf("\nFloating Number %f after expilict convertion (TypeCast) to integer gives = %d\n\n",jgn_f_explicit, jgn_i_explicit);
	return(0);
}

