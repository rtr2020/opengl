#include<stdio.h>

int main(void)
{
	int jgn_i;

	//code
	
	printf("\n\n");

	printf("Printing even numbers from 0 to 100: \n\n");

	for(jgn_i = 0; jgn_i <= 100; jgn_i++)
	{
		if(jgn_i % 2 != 0)
		{
			continue;
		}
		else
		{
			printf("\t%d\n", jgn_i);
		}
	}

	printf("\n\n");

	return(0);
}

