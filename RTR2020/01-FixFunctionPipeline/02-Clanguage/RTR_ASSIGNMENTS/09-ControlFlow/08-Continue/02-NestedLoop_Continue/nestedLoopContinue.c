#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j;

	//code
	printf("\n\n");

	printf("Outer loop prints Odd numbers between 1 and 10 \n\n");
	printf("Inner loop prints even numbers between 1 and 10 for every Odd number printed By Outer Loop. \n\n");

	for(jgn_i = 1; jgn_i <= 10; jgn_i++)
	{
		if(jgn_i % 2 != 0)
		{
			printf("i = %d\n", jgn_i);
			printf("--------\n");
			for(jgn_j = 1; jgn_j <= 10; jgn_j++)
			{
				if(jgn_j % 2 == 0)
				{
					printf("\tj = %d\n", jgn_j);
				}
				else
				{
					continue;
				}
			}
			printf("\n\n");
		}
		else
		{
			continue;
		}
	}
}
