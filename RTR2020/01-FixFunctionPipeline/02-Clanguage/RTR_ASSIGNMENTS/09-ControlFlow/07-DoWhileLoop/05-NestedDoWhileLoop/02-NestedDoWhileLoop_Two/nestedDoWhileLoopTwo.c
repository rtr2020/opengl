#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j, jgn_k;

	printf("\n\n");

	jgn_i = 1;	
	do
	{
		printf("i = %d\n", jgn_i);
		printf("------------\n\n");
		jgn_j = 1;
		do
		{
			printf("\tj = %d\n", jgn_j);
			printf("------------\n\n");
			jgn_k = 1;
			do
			{
				printf("\t\tk = %d\n", jgn_k);
				jgn_k++;
			}while(jgn_k <= 3);
			jgn_j++;
			printf("\n\n");
		}while(jgn_j <= 5);
		jgn_i++;
		printf("\n\n");
	}while(jgn_i <= 10);

	return(0);
}
