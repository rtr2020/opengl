#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j, jgn_c;

	printf("\n\n");

	jgn_i = 0;
	do
	{
		jgn_j = 0;
		do
		{
			jgn_c = ((jgn_i & 0x8) == 0) ^ ((jgn_j & 0x8) == 0);

				if(jgn_c == 0)
					printf(" ");

				if(jgn_c == 1)
					printf("* ");
			jgn_j++;
		}while(jgn_j < 64);
		printf("\n\n");
		jgn_i++;
	}while(jgn_i < 64);

	return(0);
}
