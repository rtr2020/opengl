#include<stdio.h>

int main(void)
{
	char jgn_option, jgn_ch = '\0';
	//code
	
	printf("\n\n");
	printf("Press Q or q to Quit from the Infinite Loop\n\n");

	printf("Press 'Y' or 'y' to initiate the user controlled Infinite Loop\n\n");

	scanf("%c", &jgn_option);

	if(jgn_option == 'Y' || jgn_option == 'y')
	{
		do
		{
			printf("In Loop....\n");
			scanf("%c", &jgn_ch);
			if(jgn_ch == 'Q' || jgn_ch == 'q')
				break;
		}while(1);
		printf("\n\n");
		printf("EXITTING USER CONTROLLED INFINITE LOOP...");
                printf("\n\n");
	}
	else
	{
	printf("You must press 'Y' or 'y' again to initialize the loop. Try again\n\n");
	}
	return(0);
}
