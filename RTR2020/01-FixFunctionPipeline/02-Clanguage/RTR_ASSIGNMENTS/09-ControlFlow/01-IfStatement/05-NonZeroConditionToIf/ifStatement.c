#include<stdio.h>

int main(void)
{
	int jgn_a;

	printf("\n\n");
	
	jgn_a = 5;

	if(jgn_a)
	{
		printf("If-block 1 : 'A' exists and has value = %d !\n\n", jgn_a);
	}
	jgn_a = -5;
	if(jgn_a)
        {
                printf("If-block 2 : 'A' exists and has value = %d !\n\n", jgn_a);
        }
	jgn_a = 0;
        if(jgn_a)
        {
                printf("If-block 2 : 'A' exists and has value = %d !\n\n", jgn_a);
        }

	return(0);
}

