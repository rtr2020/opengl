#include<stdio.h>

int main(void)
{
	int jgn_a, jgn_b, jgn_c;

	jgn_a = 6;
	jgn_b = 40;
	jgn_c = 40;

	printf("\n\n");

	if(jgn_a < jgn_b)
	{
		printf("Entering First if block\n");
		printf("A is less than B\n");
	}
	else
	{
		printf("Entering First else block\n");
                printf("A is less not than B\n");
	}
	printf("\nFirst pair of if and else done\n\n");

	if(jgn_b != jgn_c)
	{
		printf("\nEntering Second if block");
		printf("B is not equal to C\n");
	}
	else
        {
                printf("Entering Second else block\n");
                printf("B is equal to C\n");
        }
        printf("\nSecond pair of if and else done\n\n");

	return(0);
}

