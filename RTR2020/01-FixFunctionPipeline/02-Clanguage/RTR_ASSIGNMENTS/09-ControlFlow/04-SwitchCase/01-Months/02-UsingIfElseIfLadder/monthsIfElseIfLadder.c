#include<stdio.h>

int main(void)
{
	int jgn_mnth;

	printf("Enter number of month (1-12) : ");
	scanf("%d", &jgn_mnth);

	printf("\n\n");

	if(jgn_mnth == 1)
		printf("Month Number %d is January!!\n\n", jgn_mnth);
	else if(jgn_mnth == 2)
                printf("Month Number %d is February!!\n\n", jgn_mnth);
        else if(jgn_mnth == 3)
                printf("Month Number %d is March!!\n\n", jgn_mnth);
        else if(jgn_mnth == 4)
                printf("Month Number %d is April!!\n\n", jgn_mnth);
        else if(jgn_mnth == 5)
                printf("Month Number %d is May!!\n\n", jgn_mnth);
        else if(jgn_mnth == 6)
                printf("Month Number %d is June!!\n\n", jgn_mnth);
        else if(jgn_mnth == 7)
                printf("Month Number %d is July!!\n\n", jgn_mnth);
        else if(jgn_mnth == 8)
                printf("Month Number %d is August!!\n\n", jgn_mnth);
        else if(jgn_mnth == 9)
                printf("Month Number %d is September!!\n\n", jgn_mnth);
        else if(jgn_mnth == 10)
                printf("Month Number %d is October!!\n\n", jgn_mnth);
        else if(jgn_mnth == 11)
                printf("Month Number %d is November!!\n\n", jgn_mnth);
        else if(jgn_mnth == 12)
                printf("Month Number %d is December!!\n\n", jgn_mnth);
	else
                printf("Invalid Month Number Entered\n\n");

	return(0);
}

