#include<stdio.h>
//#include<conio.h> //Using Linux Machine for buiding

#define JGN_UPPERCASE_BEGINNING 65
#define JGN_UPPERCASE_ENDING 90

#define JGN_LOWERCASE_BEGINNING 97
#define JGN_LOWERCASE_ENDING 122

#define JGN_DIGIT_BEGINNING 48
#define JGN_DIGIT_ENDING 57

int main(void)
{
	char jgn_ch;
	int jgn_ch_value;

	printf("\n\n");

	printf("Enter character: ");
	scanf("%c", &jgn_ch);

	printf("\n\n");

	switch(jgn_ch)
	{
	case 'a':
	case 'A':

	case 'e':
	case 'E':
	

	case 'i':
	case 'I':

	case 'o':
	case 'O':
	
	case 'u':
	case 'U':
	
		printf("Character %c you entered is Vowel\n\n", jgn_ch);
		break;
	
	default:
		jgn_ch_value = (int)jgn_ch;

		if ((jgn_ch_value >=JGN_UPPERCASE_BEGINNING  && jgn_ch_value <= JGN_UPPERCASE_ENDING) || (jgn_ch_value >= JGN_LOWERCASE_BEGINNING && jgn_ch_value <= JGN_LOWERCASE_ENDING))
		{
			printf("Character %c you entered is consonant\n\n", jgn_ch);
		}
		else if((jgn_ch_value >= JGN_DIGIT_BEGINNING) && (jgn_ch_value <= JGN_DIGIT_ENDING) )
		{
			printf("Character %c you entered is a Digit\n\n", jgn_ch);
		}

		else
		{
			printf("Character %c you entered is a special character\n\n", jgn_ch);
		}
		break;
	}
	return(0);
}

