#include<stdio.h>
//#include<conio.h> //Using Linux Machine for buiding

#define JGN_UPPERCASE_BEGINNING 65
#define JGN_UPPERCASE_ENDING 90

#define JGN_LOWERCASE_BEGINNING 97
#define JGN_LOWERCASE_ENDING 122

#define JGN_DIGIT_BEGINNING 48
#define JGN_DIGIT_ENDING 57

int main(void)
{
	char jgn_ch;
	int jgn_ch_value;

	printf("\n\n");

	printf("Enter character: ");
	scanf("%c", &jgn_ch);

	printf("\n\n");

	if ((jgn_ch == 'A' || jgn_ch == 'a') || (jgn_ch == 'E' || jgn_ch == 'e') || (jgn_ch == 'I' || jgn_ch == 'i') || (jgn_ch == 'O' || jgn_ch == 'o') || (jgn_ch == 'U' || jgn_ch == 'u'))
	{
		printf("Character %c you entered is Vowel\n\n", jgn_ch);
	}

	else
	{
		jgn_ch_value = (int)jgn_ch;

		if ((jgn_ch_value >=JGN_UPPERCASE_BEGINNING  && jgn_ch_value <= JGN_UPPERCASE_ENDING) || (jgn_ch_value >= JGN_LOWERCASE_BEGINNING && jgn_ch_value <= JGN_LOWERCASE_ENDING))
		{
			printf("Character %c you entered is consonant\n\n", jgn_ch);
		}
		else if((jgn_ch_value >= JGN_DIGIT_BEGINNING) && (jgn_ch_value <= JGN_DIGIT_ENDING) )
		{
			printf("Character %c you entered is a Digit\n\n", jgn_ch);
		}

		else
		{
			printf("Character %c you entered is a special character\n\n", jgn_ch);
		}
	}

	return(0);
	
}

