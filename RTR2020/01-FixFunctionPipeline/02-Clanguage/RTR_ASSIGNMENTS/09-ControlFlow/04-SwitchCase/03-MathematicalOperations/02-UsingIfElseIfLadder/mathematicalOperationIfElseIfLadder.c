#include<stdio.h>

int main(void)
{
	int jgn_a, jgn_b;
	int jgn_result;

	char jgn_option, jgn_option_div;

	printf("\n\n");

	printf("Enter Value of A: ");
	scanf("%d", &jgn_a);

	printf("Enter Value of B: ");
	scanf("%d", &jgn_b);

	printf("Enter Option in Character: ");
	printf("'A' or 'a' For addition : \n");
	printf("'S' or 's' For subtraction : \n");
	printf("'M' or 'm' For multiplication : \n");
	printf("'D' or 'd' For division : \n\n");

	printf("Enter Option : ");
	scanf(" %c", &jgn_option);

	printf("\n\n");

	switch(jgn_option)
	{
		case 'A':
		case 'a':
			jgn_result = jgn_a + jgn_b;
			printf("Addition of %d and %d gives = %d\n\n", jgn_a, jgn_b, jgn_result);
			break;

		case 'S':
		case 's':
			if(jgn_a > jgn_b)
			{
				jgn_result = jgn_a - jgn_b;
				printf("Subtraction of B from A is = %d\n\n", jgn_result);
			}
			else
			{
				jgn_result = jgn_b - jgn_a;
				printf("Subtraction of A from B is = %d\n\n", jgn_result);
			}
			break;

		case 'm':
		case 'M':
			jgn_result = jgn_a * jgn_b;
			printf("Multiplication of A and B is = %d", jgn_result);
			break;

		case 'd':
		case 'D':
			printf("Enter option in character: \n");
			printf("'Q' or 'q' or '/' for quotient upon division : \n");
			printf("'R' or 'r' or '%%' For remainder upon division : \n");

			printf("Enter Option: ");
			scanf(" %c", &jgn_option_div);

			printf("\n\n");

			switch(jgn_option_div)
			{
			case 'Q':
			case 'q':
			case '/':
				if(jgn_a >= jgn_b)
				{
					jgn_result = jgn_a / jgn_b;
					printf("Division of A by B gives quotient = %d \n", jgn_result);
				}
				else
				{
					jgn_result = jgn_b / jgn_a;
                                        printf("Division of B by A gives quotient = %d \n", jgn_result);

				}
				break;
			case 'r':
			case 'R':
			case '%':
                               if(jgn_a >= jgn_b)
                                {
                                        jgn_result = jgn_a % jgn_b;
                                        printf("Division of A by B gives remainder = %d \n", jgn_result);
                                }
                                else
                                {
                                        jgn_result = jgn_b % jgn_a;
                                        printf("Division of B by A gives remainder = %d \n", jgn_result);

                                }
                                break;
			default:
				printf("Invalid character entered!\n\n");
				break;
			}
			break;
		default:
			printf("Invalid character entered!\n\n");
	}

}
