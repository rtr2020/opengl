#include<stdio.h>

int main(void)
{
	char jgn_option, jgn_ch = '\0';
	//code
	
	printf("\n\n");
	printf("Press Q or q to Quit from the Infinite Loop\n\n");

	printf("Press 'Y' or 'y' to initiate the user controlled Infinite Loop\n\n");

	scanf("%c", &jgn_option);

	if(jgn_option == 'Y' || jgn_option == 'y')
	{
		for(;;)
		{
			printf("In Loop....\n");
			scanf("%c", &jgn_ch);
			if(jgn_ch == 'Q' || jgn_ch == 'q')
				break;
		}
	}

	printf("\n\n");
	printf("Exiting user controlled infinite loop\n\n");
	
	return(0);
}
