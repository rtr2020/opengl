#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j;

	printf("\n\n");

	printf("Printing numbers from 10 to 1 and 100 to 10\n\n");

	for(jgn_i = 10, jgn_j = 100; jgn_i >= 1, jgn_j >= 10; jgn_i--, jgn_j = jgn_j - 10)
	{
		printf("\t %d \t %d\n", jgn_i, jgn_j);
	}

	printf("\n\n");

	return(0);
}
