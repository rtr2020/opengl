#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j, jgn_k;

	printf("\n\n");

	for(jgn_i = 1; jgn_i <= 10; jgn_i++)
	{
		printf("i = %d\n", jgn_i);
		printf("------------\n\n");
		for(jgn_j = 1; jgn_j <= 5; jgn_j++)
		{
			printf("\tj = %d\n", jgn_j);
			printf("------------\n\n");
			for(jgn_k = 1; jgn_k <= 3; jgn_k++)
			{
				printf("\t\tk = %d\n", jgn_k);
			}
			printf("\n\n");
		}
		printf("\n\n");
	}

	return(0);
}
