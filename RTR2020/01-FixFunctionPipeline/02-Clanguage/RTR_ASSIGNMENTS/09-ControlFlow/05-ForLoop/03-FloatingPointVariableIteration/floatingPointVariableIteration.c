#include<stdio.h>

int main()
{
	float jgn_f;
	float jgn_fnum = 1.9f;

	printf("\n\n");
	printf("Printing numbers %f to %f: \n\n", jgn_fnum, (jgn_fnum * 10.0f));

	for(jgn_f = jgn_fnum; jgn_f <= (jgn_fnum * 10.0f); jgn_f = jgn_f + jgn_fnum)
	{
		printf("\t%f\n", jgn_f);
	}

	printf("\n\n");

	return(0);
}
