#include<stdio.h>

int main()
{
	float jgn_f;
	float jgn_fnum = 1.6f;

	printf("\n\n");
	printf("Printing numbers %f to %f: \n\n", jgn_fnum, (jgn_fnum * 10.0f));

	jgn_f = jgn_fnum;
	while(jgn_f <= (jgn_fnum * 10.0f))
	{
		printf("\t%f\n", jgn_f);
		jgn_f = jgn_f + jgn_fnum;
	}

	printf("\n\n");

	return(0);
}
