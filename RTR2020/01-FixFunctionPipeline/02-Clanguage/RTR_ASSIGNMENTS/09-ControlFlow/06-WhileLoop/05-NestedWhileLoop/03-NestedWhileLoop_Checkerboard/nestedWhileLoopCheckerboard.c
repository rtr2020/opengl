#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j, jgn_c;

	printf("\n\n");

	jgn_i = 0;
	while(jgn_i < 64)
	{
		jgn_j = 0;
		while(jgn_j < 64)
		{
			jgn_c = ((jgn_i & 0x8) == 0) ^ ((jgn_j & 0x8) == 0);

				if(jgn_c == 0)
					printf(" ");

				if(jgn_c == 1)
					printf("* ");
			jgn_j++;
		}
		printf("\n\n");
		jgn_i++;
	}

	return(0);
}
