#include<stdio.h>

int main(void)
{
	int jgn_i, jgn_j, jgn_k;

	printf("\n\n");

	jgn_i = 1;	
	while(jgn_i <= 10)
	{
		printf("i = %d\n", jgn_i);
		printf("------------\n\n");
		jgn_j = 1;
		while(jgn_j <= 5)
		{
			printf("\tj = %d\n", jgn_j);
			printf("------------\n\n");
			jgn_k = 1;
			while(jgn_k <= 3)
			{
				printf("\t\tk = %d\n", jgn_k);
				jgn_k++;
			}
			jgn_j++;
			printf("\n\n");
		}
		jgn_i++;
		printf("\n\n");
	}

	return(0);
}
