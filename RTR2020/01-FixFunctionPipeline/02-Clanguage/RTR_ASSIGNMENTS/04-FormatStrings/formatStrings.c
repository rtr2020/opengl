#include<stdio.h>

int main(void)
{
	//code
	printf("\n\n");

	int a_jit = 13;

	printf("Integer decimal value of a = %d\n", a_jit);
	printf("Integer Octal value if a = %o\n", a_jit);
	printf("Integer Hex value in Lower case of a = %x\n", a_jit);
	printf("Integer Hex value in Upper case of a = %X\n", a_jit);

	char ch_jit = 'A';
	printf("Character ch is = %c\n", ch_jit);

	char str[] = "My Roll number in RTR 2020 BATCH is RTR2020-084 (Jithin)";
	printf("String str = %s\n",str);

	long num_jit = 30121995L;
	printf("Long Integer = %ld\n", num_jit);

	unsigned int b_jit = 7;
	printf("Unsigned integer 'b' = %u\n", b_jit);

	float f_jit = 3012.1995f;
	printf("Floating point number with just %%f 'f_jit' = %f\n", f_jit);
	printf("Floating point number with just %%4.2f 'f_jit' = %4.2f\n", f_jit);
	printf("Floating point number with just %%6.5f 'f_jit' = %6.5f\n", f_jit);

	double pi_jit = 3.14159265358979323846;
	printf("Double precision floating point number with exponential = %g\n", pi_jit);
	printf("Double precision floating point number with exponential Lower Case = %e\n", pi_jit);
	printf("Double precision floating point number with exponential Upper Case = %E\n", pi_jit);
	printf("Double Hexadecimal value of pi_jit in Lower Case = %a\n", pi_jit);
	printf("Double Hexadecimal value of pi_jit in Upper Case = %A\n", pi_jit);
	printf("\n\n");

	return(0);	
}

