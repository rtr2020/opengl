#include<stdio.h>
#include<stdarg.h>

#define NUM_TO_BE_FOUND 3
#define NUM_ELEMENTS 10

int main(void)
{
	void FindNumber(int, int, ...);
	printf("\n\n");

	FindNumber(NUM_TO_BE_FOUND, NUM_ELEMENTS, 3, 9, 2, 4, 5, 7, 9, 5, 4, 8, 0);

	return(0);
}

void FindNumber(int num_to_be_found, int num, ...)
{
	int va_FindNumber(int, int, va_list);

	int jgn_count = 0;
	int jgn_n;
	va_list jgn_number_list;

	va_start(jgn_number_list, num);

	jgn_count = va_FindNumber(num_to_be_found, num, jgn_number_list);
	
	if (jgn_count == 0)
		printf("Number %d could not be found\n\n", num_to_be_found);
	else
		printf("Number %d found %d times\n\n", num_to_be_found, jgn_count);

	va_end(jgn_number_list);
}

int va_FindNumber(int num_to_be_found, int num, va_list list)
{
	int jgn_count_of_num = 0;
	int jgn_n;
	
	while (num)
	{
		jgn_n = va_arg(list, int);
		if (jgn_n == num_to_be_found)
			jgn_count_of_num++;
		num--;
	}

	return(jgn_count_of_num);
}