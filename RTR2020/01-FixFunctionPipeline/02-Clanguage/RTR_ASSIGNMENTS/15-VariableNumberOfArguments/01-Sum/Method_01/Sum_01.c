#include<stdio.h>
#include<stdarg.h>

int main(void)
{
	int CalculateSum(int, ...);

	int jgn_answer;

	printf("\n\n");

	jgn_answer = CalculateSum(5, 10, 20, 30, 40, 50);
	printf("Answer = %d\n\n", jgn_answer);


	jgn_answer = CalculateSum(9, 1, 1, 1, 1, 1, 1, 1, 1, 1);
	printf("Answer = %d\n\n", jgn_answer);

	jgn_answer = CalculateSum(0);
	printf("Answer = %d\n\n", jgn_answer);

	return(0);
}

int CalculateSum(int num, ...)
{
	int jgn_sum_total = 0;
	int jgn_n;

	va_list jgn_numList;

	va_start(jgn_numList, num);

	while (num)
	{
		jgn_n = va_arg(jgn_numList, int);
		jgn_sum_total = jgn_sum_total + jgn_n;
		num--;
	}

	va_end(jgn_numList);
	return(jgn_sum_total);
}