#include<stdio.h>
#include<stdarg.h>

int main(void)
{
	int CalculateSum(int, ...);

	int jgn_answer;

	printf("\n\n");

	jgn_answer = CalculateSum(5, 10, 20, 30, 40, 50);
	printf("Answer = %d\n\n", jgn_answer);


	jgn_answer = CalculateSum(9, 1, 1, 1, 1, 1, 1, 1, 1, 1);
	printf("Answer = %d\n\n", jgn_answer);

	jgn_answer = CalculateSum(0);
	printf("Answer = %d\n\n", jgn_answer);

	return(0);
}

int CalculateSum(int num, ...)
{
	int va_CalculateSum(int, va_list);

	int jgn_sum = 0;
	va_list jgn_numList;

	va_start(jgn_numList, num);

	jgn_sum = va_CalculateSum(num, jgn_numList);

	va_end(jgn_numList);
	return(jgn_sum);
}

int va_CalculateSum(int num, va_list list)
{
	int jgn_n;
	int jgn_sum_total = 0;

	while (num)
	{
		jgn_n = va_arg(list, int);
		jgn_sum_total = jgn_sum_total + jgn_n;
		num--;
	}

	return(jgn_sum_total);
}