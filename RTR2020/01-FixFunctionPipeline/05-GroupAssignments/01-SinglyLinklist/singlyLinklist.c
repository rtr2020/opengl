#include<stdio.h>
#include<malloc.h>

struct node
{
	int val;
	struct node* next;
};

typedef struct node NODE;

NODE *head = NULL, *temp = NULL, *temp2;
NODE *newnode = NULL;

int main(void)
{
	//function declarations
	void createNode(void);
	void insertNodeAtFirst(void);
	void insertNodeAtLast(void);
	void insertNodeAtPos(void);
	int lengthOfNode(void);
	void deleteNodeAtPos(void);
	void deleteNodeAtLast(void);
	void deleteNodeAtFirst(void);
	void display();

	//local variable declaration
	int choice;

	//code

	do
	{
		printf("1: Create Node:\n");
		printf("2: Insert Node at first:\n");
		printf("3: Insert Node at Last:\n");
		printf("4: Insert Node at Position:\n");
		printf("5: Delete Node at Position:\n");
		printf("6: Display Node:\n");

		scanf("%d", &choice);

		switch (choice)
		{
		case 1:
			createNode();
			break;
		case 2:
			insertNodeAtFirst();
			break;
		case 3:
			insertNodeAtLast();
			break;
		case 4:
			insertNodeAtPos();
			break;
		case 5:
			deleteNodeAtPos();
			break;
		case 6:
			printf("*****Result******\n\n");
			display();
			break;
		default:
			printf("Invalid choice\n");
			break;
		}
	} while (choice < 7);
	return(0);
}

void createNode(void)
{
	//code 

	if (head == NULL)
	{
		head = (NODE *)malloc(sizeof(NODE));
		if (head == NULL)
		{
			printf("\nMemory was not allocated");
		}
		printf("Enter value for first node: ");
		scanf("%d", &head->val);
		head->next = NULL;
		printf("Node created\n");
	}
	else
	{
		printf("Node already created\n");
	}
}

void insertNodeAtFirst(void)
{
	//code 

	if (head == NULL)
	{
		printf("No nodes are created\n");
	}
	else
	{
		temp = head;
		newnode = (NODE *)malloc(sizeof(NODE));
		if (newnode == NULL)
		{
			printf("\nMemory was not allocated");
		}
		printf("Enter value for new node: ");
		scanf("%d", &newnode->val);
		printf("\n");
		newnode->next = temp;
		head = newnode;
		//head->next = temp;
	}
}

void insertNodeAtLast(void)
{
	//code 

	if (head == NULL)
	{
		printf("No nodes are created\n");
	}
	else
	{
		temp = head;
		newnode = (NODE*)malloc(sizeof(NODE));
		if (newnode == NULL)
		{
			printf("\nMemory was not allocated");
		}
		else
		{
			temp = head;
			while (temp != NULL)
			{
				if (temp->next == NULL)
				{
					printf("Enter value for new node: ");
					scanf("%d", &newnode->val);
					printf("\n");
					temp->next = newnode;
					newnode->next = NULL;
					temp = newnode;
				}
				temp = temp->next;
			}
		}
	}
}


void insertNodeAtPos(void)
{
	//local variable declaration
	int pos;
	int count = 1;
	int len;
	//code 

	len = lengthOfNode();
	printf("Enter the position to insert node:");
	scanf("%d", &pos);
	printf("\n\n");

	if (head == NULL)
	{
		printf("No nodes are Available\n");
	}
	else
	{
		if (len == pos)
			insertNodeAtLast();
		else if (pos == 1)
			insertNodeAtFirst();
		else if (pos < 0 || pos > len)
			printf("Invalid position given, Please give position between 1 and %d \n\n", len);
		else
		{
			temp = head;
			newnode = (NODE*)malloc(sizeof(NODE));
			if (newnode == NULL)
			{
				printf("\nMemory was not allocated");
			}
			else
			{
				while (temp != NULL)
				{
					if (count == pos - 1)
					{
						temp2 = temp->next;
						printf("Enter value for new node: ");
						scanf("%d", &newnode->val);
						printf("\n");
						temp->next = newnode;
						newnode->next = temp2;
						temp = newnode;
					}
					count++;
					temp = temp->next;
				}
			}
		}
	}
}

void deleteNodeAtPos(void)
{
	//local variable declaration
	int pos;
	int count = 1;
	int len;
	//code 

	len = lengthOfNode();
	printf("Enter the position to delete the node:");
	scanf("%d", &pos);
	printf("\n\n");

	if (head == NULL)
	{
		printf("No nodes are Available\n");
	}
	else if (head->next == NULL)
	{
		printf("Only one node is there to delete hence not doing\n");
	}
	else
	{
		if (len == pos)
			deleteNodeAtLast();
		else if (pos == 1)
			deleteNodeAtFirst();
		else if (pos < 0 || pos > len)
			printf("Invalid position given, Please give position between 1 and %d \n\n", len);
		else
		{
			temp = head;
			while (temp != NULL)
			{
				if (count == pos - 1)
				{
					temp2 = temp->next;
					temp->next = temp2->next;
					free(temp2);
					temp2 = NULL;
				}
				count++;
				temp = temp->next;
			}
		}
	}
}

void deleteNodeAtLast(void)
{
	//local variable declaration
	int count = 1;
	int len = lengthOfNode();
	//code 
	if (head == NULL)
	{
			printf("No nodes are created\n");
	}
	else
	{
		temp = head;
		while (temp != NULL)
		{
			if (count == len-1)
			{
				temp2 = temp->next;
				free(temp2);
				temp2 = NULL;
				temp->next = NULL;
			}
			count++;
			temp = temp->next;
		}
	}
}
void deleteNodeAtFirst(void)
{
	//code 
	if (head == NULL)
	{
		printf("No nodes are created\n");
	}
	else if (head->next == NULL)
		printf("Only single node left to delete\n\n");
	else
	{
		temp = head;
		temp2 = temp->next;
		free(temp);
		temp = NULL;
		head = temp2;
	}
}

int lengthOfNode(void)
{
	int length = 0;
	if (head == NULL)
	{
		length = 0;
	}
	else
	{
		temp = head;
		while (temp != NULL)
		{
			length++;
			temp = temp->next;
		}
	}
	return(length);
}

void display(void)
{
	//code

	if (head == NULL)
	{
		printf("No list created\n");
	}
	else
	{
		temp = head;
		while (temp != NULL)
		{
			printf("%d\n", temp->val);
			temp = temp->next;
		}
	}
}