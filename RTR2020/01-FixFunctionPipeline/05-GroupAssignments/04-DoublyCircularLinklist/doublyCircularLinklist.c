#include<stdio.h>
#include<malloc.h>

struct node
{
	int val;
	struct node* next;
	struct node* prev;
};

typedef struct node NODE;

NODE *head = NULL, *temp = NULL, *temp2 = NULL, *last = NULL;
NODE *newnode = NULL;

int main(void)
{
	//function declarations
	void createNode(void);
	void insertNodeAtFirst(void);
	void insertNodeAtLast(void);
	void insertNodeAtPos(void);
	int lengthOfNode(void);
	void deleteNodeAtPos(void);
	void deleteNodeAtLast(void);
	void deleteNodeAtFirst(void);
	void display();

	//local variable declaration
	int choice;

	//code

	do
	{
		printf("1: Create Node:\n");
		printf("2: Insert Node at first:\n");
		printf("3: Insert Node at Last:\n");
		printf("4: Insert Node at Position:\n");
		printf("5: Delete Node at Position:\n");
		printf("6: Display Node:\n");

		scanf("%d", &choice);

		switch (choice)
		{
		case 1:
			createNode();
			break;
		case 2:
			insertNodeAtFirst();
			break;
		case 3:
			insertNodeAtLast();
			break;
		case 4:
			insertNodeAtPos();
			break;
		case 5:
			deleteNodeAtPos();
			break;
		case 6:
			printf("*****Result******\n\n");
			display();
			break;
		default:
			printf("Invalid choice\n");
			break;
		}
	} while (choice < 7);
	return(0);
}

void createNode(void)
{
	//code 

	if (head == NULL)
	{
		head = (NODE *)malloc(sizeof(NODE));
		if (head == NULL)
		{
			printf("\nMemory was not allocated");
		}
		printf("Enter value for first node: ");
		scanf("%d", &head->val);
		head->next = head;
		head->prev = head;
		last = head;
		printf("Node created\n");
	}
	else
	{
		printf("Node already created\n");
	}
}

void insertNodeAtFirst(void)
{
	//code 

	if (head == NULL)
	{
		printf("No nodes are created\n");
	}
	else
	{
		temp = head;
		newnode = (NODE *)malloc(sizeof(NODE));
		if (newnode == NULL)
		{
			printf("\nMemory was not allocated");
		}
		printf("Enter value for new node: ");
		scanf("%d", &newnode->val);
		printf("\n");
		newnode->next = temp;
		temp->prev = newnode;
		newnode->prev = last;
		head = newnode;
	}
}

void insertNodeAtLast(void)
{
	//code 

	if (head == NULL)
	{
		printf("No nodes are created\n");
	}
	else
	{
		newnode = (NODE*)malloc(sizeof(NODE));
		if (newnode == NULL)
		{
			printf("\nMemory was not allocated");
		}
		else
		{
			printf("Enter value for new node: ");
			scanf("%d", &newnode->val);
			printf("\n");
			last->next = newnode;
			newnode->next = head;
			newnode->prev = last;
			last = newnode;
			head->prev = last;
		}
	}
}


void insertNodeAtPos(void)
{
	//local variable declaration
	int pos;
	int count = 1;
	int len;
	//code 

	len = lengthOfNode();
	printf("Enter the position to insert node:");
	scanf("%d", &pos);
	printf("\n\n");

	if (head == NULL)
	{
		printf("No nodes are Available\n");
	}
	else
	{
		if (len == pos)
			insertNodeAtLast();
		else if (pos == 1)
			insertNodeAtFirst();
		else if (pos < 0 || pos > len)
			printf("Invalid position given, Please give position between 1 and %d \n\n", len);
		else
		{
			temp = head;
			newnode = (NODE*)malloc(sizeof(NODE));
			if (newnode == NULL)
			{
				printf("\nMemory was not allocated");
			}
			else
			{
				while (temp != last)
				{
					if (count == pos - 1)
					{
						temp2 = temp->next;
						printf("Enter value for new node: ");
						scanf("%d", &newnode->val);
						printf("\n");
						temp->next = newnode;
						newnode->next = temp2;
						newnode->prev = temp;
						temp = newnode;
					}
					count++;
					temp = temp->next;
				}
			}
		}
	}
}

void deleteNodeAtPos(void)
{
	//local variable declaration
	int pos;
	int count = 1;
	int len;
	//code 

	len = lengthOfNode();
	printf("Enter the position to delete the node:");
	scanf("%d", &pos);
	printf("\n\n");

	if (head == NULL)
	{
		printf("No nodes are Available\n");
	}
	else if (head == last)
	{
		printf("Only one node is there to delete hence not doing\n");
	}
	else
	{
		if (len == pos-1)
			deleteNodeAtLast();
		else if (pos == 1)
			deleteNodeAtFirst();
		else if (pos < 0 || pos > len)
			printf("Invalid position given, Please give position between 1 and %d \n\n", len);
		else
		{
			temp = head;
			while (temp != last)
			{
				if (count == pos - 1)
				{
					temp2 = temp->next;
					temp->next = temp2->next;
					temp2->prev = temp->prev;
					free(temp2);
					temp2 = NULL;
				}
				count++;
				temp = temp->next;
			}
		}
	}
}

void deleteNodeAtLast(void)
{
	//local variable declaration
	//code 
	if (head == NULL)
	{
			printf("No nodes are created\n");
	}
	else
	{			
		temp = last->prev;
		free(last);
		last = NULL;
		temp->next = head;
		last = temp;
		head->prev = last;
	}
}
void deleteNodeAtFirst(void)
{
	//code 
	if (head == NULL)
	{
		printf("No nodes are created\n");
	}
	else if (head->next == NULL)
		printf("Only single node left to delete\n\n");
	else
	{
		temp = head;
		temp2 = temp->next;
		free(temp);
		temp2->prev = last;
		temp = NULL;
		head = temp2;
		last->next = head;
	}
}

int lengthOfNode(void)
{
	int length = 0;
	if (head == NULL)
	{
		length = 0;
	}
	else
	{
		temp = head;
		while (temp != last)
		{
			length++;
			temp = temp->next;
		}
	}
	return(length);
}

void display(void)
{
	//code

	if (head == NULL)
	{
		printf("No list created\n");
	}
	else
	{
		temp = head;
		while (temp != last)
		{
			printf("%d\n", temp->val);
			temp = temp->next;
		}
		printf("%d\n", temp->val);
	}
}