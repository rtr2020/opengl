#include<stdio.h>
#include<math.h>

#define PI 3.141592654

int main(void)
{
	//local variable declarations
	double radius = 0;
	int hr_no = 12;
	double cord_x = 0, cord_y = 0;
	double radian;

	//code
	printf("Enter radius of the clock: ");
	scanf("%lf", &radius);
	printf("\n\n");

	while(hr_no != 0)
	{
		switch (hr_no)
		{
		case 1:
			radian = (60 * PI) / 180;
			cord_x = radius * (cos(radian));
			cord_y = radius * (sin(radian));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 2:
			radian = (30 * PI) / 180;
			cord_x = radius * (cos(radian));
			cord_y = radius * (sin(radian));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 3:
			cord_x = radius;
			cord_y = 0;
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 4:
			radian = (30 * PI) / 180;
			cord_x = radius * (cos(radian));
			cord_y = -(radius * (sin(radian)));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 5:
			radian = (60 * PI) / 180;
			cord_x = radius * (cos(radian));
			cord_y = -(radius * (sin(radian)));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;

		case 6:
			cord_x = 0;
			cord_y = -(radius);
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 7:
			radian = (60 * PI) / 180;
			cord_x = -(radius * (cos(radian)));
			cord_y = -(radius * (sin(radian)));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 8:
			radian = (30 * PI) / 180;
			cord_x = -(radius * (cos(radian)));
			cord_y = -(radius * (sin(radian)));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;

		case 9:
			cord_x = -(radius);
			cord_y = 0;
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 10:
			radian = (30 * PI) / 180;
			cord_x = -(radius * (cos(radian)));
			cord_y = (radius * (sin(radian)));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		case 11:
			radian = (60 * PI) / 180;
			cord_x = -(radius * (cos(radian)));
			cord_y = (radius * (sin(radian)));
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;

		case 12:
			cord_x = 0;
			cord_y = radius;
			printf("Co-ordinates of hour handle %d are (%lf, %lf)\n\n", hr_no, cord_x, cord_y);
			break;
		default:
			printf("Enter hour handle between 1 and 12\n\n");
			break;
		}
		hr_no--;
	}

	return(0);
}