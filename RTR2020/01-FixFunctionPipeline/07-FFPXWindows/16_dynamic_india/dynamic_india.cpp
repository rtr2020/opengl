#include <iostream>	
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include<math.h>

using namespace std;

bool bFullscreen = false;
bool bDone = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;

GLXContext gGLXContext;

int giWindowWidth = 800;
int giWindowHeight = 600;

const float PI = 3.1415f;

GLfloat i1_anime = -2.0f;
GLfloat n_anime = 2.0f;
GLfloat d_anime = -2.0f;
GLfloat i2_anime = -2.0f;
GLfloat a_anime = 2.0f;

GLfloat plane1_bottom_trans_x = -1.0f;
GLfloat plane1_bottom_trans_y = -0.5f;
GLfloat plane1_bottom_rotate_z = 80;

GLfloat plane2_top_trans_x = -1.0f;
GLfloat plane2_top_trans_y = 0.5f;
GLfloat plane2_top_rotate_z = -80;

GLfloat plane3_middle_trans_x = -1.5f;
GLfloat plane3_middle_trans_y = 0.0f;

int main(void)		
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);
        void update(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;
	char ascii[26];
	CreateWindow();
	initialize();
	
	XEvent event;
	KeySym keysym;		
	
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
						default:
							break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'f':
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						default:
							break;
					}		
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;	
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:			
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
	}
	uninitialize();
	return(0);
}
				
void CreateWindow(void)
{
	void uninitialize(void);
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[] = 
	{
		GLX_DOUBLEBUFFER, True,
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
                GLX_DEPTH_SIZE, 24,
		None
	};
	
	//code:
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("error: unable to open, exiting.\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
		
	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				0,				//x
				0,				//y
				giWindowWidth,			//w
				giWindowHeight,			//h
				0,				//thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("error: failed to create window, exiting.\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First window linux");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

	
void ToggleFullscreen(void)
{

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};
	
	//code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False); 
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
		
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);
	

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGLXContext == NULL)
	{
		printf("error: failed rendering.wxiting.\n");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resize(giWindowWidth, giWindowHeight);
} 

void display(void)
{
	void I1(void);
	void N(void);
	void D(void);
	void I2(void);
	void A(void);

	void iaf_plane(void);
	void plane_animation(void);


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(i1_anime, 0.0f, -2.5f);
	I1();
	if (i1_anime >= 0.2f)
	{
		glTranslatef(1.06f, 0.0f, 0.0f);
		glTranslatef(a_anime, 0.0f, 0.0f);
		A();
	}

	if (a_anime <= 0.2f)
	{
		glTranslatef(-1.06f, 0.0f, 0.0f);
		glTranslatef(0.0f, n_anime, 0.0f);
		N();
	}

	if (n_anime <= 0.0f)
	{
		glTranslatef(0.86f, 0.0f, 0.0f);
		glTranslatef(0.0f, i2_anime, 0.0f);
		I2();
	}

	if (i2_anime >= 0.0f)
	{
		glTranslatef(-0.56f, 0.0f, 0.0f);
		glTranslatef(0.0f, 0.0f, d_anime);
		D();
	}


	if (i1_anime <= 0.2f)
		i1_anime = i1_anime + 0.0028f;

	if (i1_anime >= 0.2f && a_anime >= 0.2f)
		a_anime = a_anime - 0.0028f;

	if (a_anime <= 0.2f && n_anime >= 0.0f)
		n_anime = n_anime - 0.0028f;

	if (n_anime <= 0.0f && i2_anime <= 0.0f)
		i2_anime = i2_anime + 0.0028f;

	if (i2_anime >= 0.0f && d_anime <= 0)
		d_anime = d_anime + 0.0028f;

	if (d_anime >= 0)
	{
		glTranslatef(0.0f, 0.0f, 7.0f);
		plane_animation();
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}



void I1(void)
{
	//I
	glLineWidth(30.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();
}

void N(void)
{
	//N
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.8f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.8f, -0.6f, 0.0f);
	glEnd();
}

void D(void)
{
	//D
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.9f, 0.57f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, -0.57f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.65f, 0.57f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.65f, 0.0f, 0.0f);
	glVertex3f(-0.65f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.65f, -0.57f, 0.0f);
	glEnd();

	glLineWidth(20.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.95f, 0.57f, 0.0f);
	glVertex3f(-0.65f, 0.57f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.95f, -0.57f, 0.0f);
	glVertex3f(-0.65f, -0.57f, 0.0f);
	glEnd();
}

void I2(void)
{
	//I
	glLineWidth(30.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();
}

void A(void)
{
	if (plane3_middle_trans_x >= 0.5f)
	{
		//A
		glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.85f, 0.0f, 0.0f);
		glVertex3f(-0.55f, 0.0f, 0.0f);
		glEnd();
	}
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.7f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.85f, 0.0f, 0.0f);
	glVertex3f(-0.85f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -0.6f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.7f, 0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.55f, 0.0f, 0.0f);
	glVertex3f(-0.55f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();
}

void iaf_plane(void)
{

	//ammunition
	glBegin(GL_QUADS);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f / 4.0f, 0.36f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.42f / 4.0f, 0.0f);
	glVertex3f(-0.3f / 4.0f, 0.42f / 4.0f, 0.0f);
	glVertex3f(-0.3f / 4.0f, 0.36f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.4f / 4.0f, 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.3f / 4.0f, (0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, (0.36f / 4.0f), 0.0f);
	glVertex3f(-0.2f / 4.0f, (0.39f / 4.0f), 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f / 4.0f, -(0.36f / 4.0f), 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, -(0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, -(0.36f / 4.0f), 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.4f / 4.0f), 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.3f / 4.0f, -(0.42f / 4.0f), 0.0f);
	glVertex3f(-0.3f / 4.0f, -(0.36f / 4.0f), 0.0f);
	glVertex3f(-0.2f / 4.0f, -(0.39f / 4.0f), 0.0f);
	glEnd();


	//end of ammunition
	//plane body

	//top side flip
	glBegin(GL_TRIANGLES); 
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f/4.0f, 0.4f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.4f / 4.0f), 0.0f);
	glVertex3f(0.05f / 4.0f, 0.0f, 0.0f);
	glEnd();

	//bottom side flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, 0.4f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.1f / 4.0f, 0.0f);
	glVertex3f(0.05f / 4.0f, 0.01f / 4.0f, 0.0f);
	glEnd();

	//back flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.75f, 0.7125f, 0.7125f);
	glVertex3f(-0.5f / 4.0f, 0.1f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.1f / 4.0f), 0.0f);
	glVertex3f(-0.7f / 4.0f, 0.0f, 0.0f);
	glEnd();

	//back top flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, 0.1f / 4.0f, 0.0f);
	glVertex3f(-0.62f / 4.0f, 0.036f / 4.0f, 0.0f);
	glVertex3f(-0.60f / 4.0f, 0.15f / 4.0f, 0.0f);
	glEnd();

	//back bottom flip
	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, -(0.1f / 4.0f), 0.0f);
	glVertex3f(-0.62f / 4.0f, -(0.036f / 4.0f), 0.0f);
	glVertex3f(-0.60f / 4.0f, -(0.15f / 4.0f), 0.0f);
	glEnd();



	//tail flag

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.6f / 4.0f, 0.009f, 0.0f);
	glVertex3f(-0.8f / 4.0f, 0.009f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f / 4.0f, -0.009f, 0.0f);
	glVertex3f(-0.8f / 4.0f, -0.009f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f / 4.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.5f / 4.0f, -(0.4f / 4.0f), 0.0f);
	glVertex3f(-0.5f / 4.0f, -(0.1f / 4.0f), 0.0f);
	glVertex3f(0.05f / 4.0f, -(0.01f / 4.0f), 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.55f, 0.4785f, 0.4785f);
	glVertex3f(-0.1f / 4.0f, 0.2f / 4.0f, 0.0f);
	glVertex3f(-0.1f / 4.0f, -(0.2f / 4.0f), 0.0f);
	glVertex3f(0.5f / 4.0f, 0.0f, 0.0f);
	glEnd();

	//windscreen
	glTranslatef(0.04f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f / 8.0f, 0.2f / 8.0f, 0.0f);
	glVertex3f(-0.1f / 8.0f, -(0.2f / 8.0f), 0.0f);
	glVertex3f(0.2f / 8.0f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f / 8.0f, 0.0f, 0.0f);
	glVertex3f(0.2f / 8.0f, 0.0f, 0.0f);
	glEnd();
	//End of plane body

	//green logo
	glTranslatef(-0.14f, 0.048f, 0.0f);
	glPointSize(5);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 1.0f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.02 / 4.0f) * cos(angle), (0.02 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//while logo
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.04 / 4.0f) * cos(angle), (0.04 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//orange logo
	glPointSize(3);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.5f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.06 / 4.0f )* cos(angle), (0.06 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();


	glTranslatef(0.0f, -0.096f, 0.0f);
	//green logo
	glPointSize(5);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 1.0f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.02 / 4.0f) * cos(angle), (0.02 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//while logo
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.04 / 4.0f) * cos(angle), (0.04 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();

	//orange logo
	glPointSize(3);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.5f, 0.0f);
	for (float angle = 0.0f; angle < 2.0f * PI; angle = angle + 0.0001f)
	{
		glVertex3f((0.06 / 4.0f) * cos(angle), (0.06 / 4.0f) * sin(angle), 0.0f);
	}
	glEnd();


	glTranslatef(0.1f, 0.045f, 0.0f);
	//I
	glColor3f(1.0f, 1.0f, 1.0f);
	glScalef(0.18f, 0.18f, 0.18f);
	glTranslatef(-0.5f, 0.0f, 0.0f);
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glVertex3f(-0.5f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -0.3f / 4.0f, 0.0f);
	glEnd();

	//A
	glTranslatef(0.03f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.425f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.275f / 4.0f, 0.0f, 0.0f);
	glVertex3f(-0.35f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -0.3f / 4.0f, 0.0f);
	glVertex3f(-0.35f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.2f / 4.0f, -0.3f / 4.0f, 0.0f);
	glEnd();

	//F
	glTranslatef(0.1f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.5f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, -0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.25f / 4.0f, 0.3f / 4.0f, 0.0f);
	glVertex3f(-0.5f / 4.0f, 0.05f / 4.0f, 0.0f);
	glVertex3f(-0.3f / 4.0f, 0.05f / 4.0f, 0.0f);
	glEnd();
}

void plane_animation(void)
{
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glTranslatef(plane1_bottom_trans_x, plane1_bottom_trans_y, -1.5f);
glRotatef(plane1_bottom_rotate_z, 0.0f, 0.0f, 1.0f);
iaf_plane();

glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glTranslatef(plane2_top_trans_x, plane2_top_trans_y, -1.5f);
glRotatef(plane2_top_rotate_z, 0.0f, 0.0f, 1.0f);
iaf_plane();

glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glTranslatef(plane3_middle_trans_x, plane3_middle_trans_y, -1.5f);
iaf_plane();


if (plane1_bottom_trans_x <= -0.7f && plane3_middle_trans_x <= 0.7f)
plane1_bottom_trans_x = plane1_bottom_trans_x + 0.0025f;
if (plane1_bottom_trans_y <= 0.0f && plane3_middle_trans_x <= 0.7f)
plane1_bottom_trans_y = plane1_bottom_trans_y + 0.0042f;
if (plane1_bottom_rotate_z >= 0.0f && plane3_middle_trans_x <= 0.7f)
plane1_bottom_rotate_z = plane1_bottom_rotate_z - 0.69f;

if (plane2_top_trans_x <= -0.7f && plane3_middle_trans_x <= 0.7f)
plane2_top_trans_x = plane2_top_trans_x + 0.0025f;
if (plane2_top_trans_y >= 0.0f && plane3_middle_trans_x <= 0.7f)
plane2_top_trans_y = plane2_top_trans_y - 0.0042f;
if (plane2_top_rotate_z <= 0.0f && plane3_middle_trans_x <= 0.7f)
plane2_top_rotate_z = plane2_top_rotate_z + 0.69f;

if (plane3_middle_trans_x <= -0.7f)
plane3_middle_trans_x = plane3_middle_trans_x + 0.0068f;


if (plane3_middle_trans_x >= -0.7f && plane2_top_trans_x >= -0.7 && plane1_bottom_trans_x >= -0.7 && plane3_middle_trans_x < 0.7f)
{
	plane3_middle_trans_x = plane2_top_trans_x;
	plane1_bottom_trans_x = plane2_top_trans_x;
	plane3_middle_trans_y = plane2_top_trans_y;
	plane1_bottom_trans_y = plane2_top_trans_y;
	glTranslatef(plane3_middle_trans_x, 0.0f, -1.5f);
	glTranslatef(plane2_top_trans_x, 0.0f, -1.5f);
	glTranslatef(plane1_bottom_trans_x, 0.0f, -1.5f);
	plane3_middle_trans_x = plane3_middle_trans_x + 0.0128f;
	plane2_top_trans_x = plane2_top_trans_x + 0.0128f;
	plane1_bottom_trans_x = plane1_bottom_trans_x + 0.0128f;
}

if (plane3_middle_trans_x >= 0.7f)
{
		plane1_bottom_trans_x = plane1_bottom_trans_x + 0.0125f;
		plane1_bottom_trans_y = plane1_bottom_trans_y - 0.0142f;
		plane1_bottom_rotate_z = plane1_bottom_rotate_z - 0.89f;

		plane2_top_trans_x = plane2_top_trans_x + 0.0125f;
		plane2_top_trans_y = plane2_top_trans_y + 0.0142f;
		plane2_top_rotate_z = plane2_top_rotate_z + 0.89f;

		plane3_middle_trans_x = plane3_middle_trans_x + 0.0128f;
}
}

